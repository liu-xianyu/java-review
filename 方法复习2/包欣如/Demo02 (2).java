package 数组;

public class Demo02 {
	public static void main(String[] args) {
		int a = 10;
		int b = 20;
		System.out.println("变换之前：a=" + a + " b=" + b);

		int kk = a;
		a = b;
		b = kk;
		System.out.println("变换之后：a=" + a + " b=" + b);

	}
//已知两个整数变量a = 10，b = 20，使用程序实现这两个变量的数据交换
//​        最终输出a = 20，b = 10;

}
