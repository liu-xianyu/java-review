package md.lesson01.com;

import java.util.Random;

import java.util.Scanner;

public class Demo4 {

	public static void main(String[] args) {
		// 随机生成银行卡后7位数，返回的数据格式如：0586 756
			Scanner scanner = new Scanner(System.in);
			Random random = new Random();
			int [] num1 = new int[4];
			int [] num2 = new int[3];
			System.out.println("银行卡后七位：");
			for (int i = 0; i < num1.length; i++) {
				num1[i] = random.nextInt(10)+1;
				System.out.print(num1[i]);
			}
			System.out.print(" ");
			for (int i = 0; i < num2.length; i++) {
				num2[i] = random.nextInt(10)+1;
				System.out.print(num2[i]);
			}
	}

}
