package com.md;

import java.util.Random;

//用方法实现数组的排序。
//定义一个函数，实现数组从大到小排序，传入一个乱序数组，返回一个从大到小排序的数组。

public class Demo04 {

	public static void main(String[] args) {
		Random input = new Random();
		int []arr = new int[5];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = input.nextInt(100);
			System.out.println(arr[i]+"");
		}
		System.out.println();
		int []brr = Random(arr);
		for (int i = 0; i < arr.length; i++) {
			System.out.println(brr[i]+"");
		}
	}
	public static int[] Random(int[]arr) {
		int temp;
		for (int i = 0; i < arr.length-1; i++) {
			for (int j = i+1; j < arr.length; j++) {
				if (arr[i] < arr[j]) {
					temp = arr[j];
					arr[j] = arr[i];
					arr[i] = temp;
							
							}
			}
		}
		return arr;
	
	}
	public static void printArray(int[] a) {
		System.out.print("[");
		for (int i = 0; i < a.length; i++) {
			if (i == a.length - 1) {
				System.out.print(a[i]);
			} else {
				System.out.print(a[i] + ",");
			}
		}
		System.out.println("]");
}
}
