package com.md;

//已知两个整数变量a = 10，b = 20，使用程序实现
//这两个变量的数据交换最终输出a = 20，b = 10;

public class Demo01 {

	public static void main(String[] args) {
		int a = 10;
		int b = 20;
		int c;
		c = a;
		a = b;
		b = c;
		System.out.println("a="+a +"\t"+ "b=" +b);
	}

}
