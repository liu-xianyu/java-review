package 第三次课作业;
//定义一个方法，用来实现如下功能：
//​ 已知一个数组 arr = {19, 28, 37, 46, 50}; 用程序实现把数组中的元素值交换，
//​ 交换后的数组 arr = {50, 46, 37, 28, 19}; 并在控制台输出交换后的数组元素
public class JiaoHuan2 {
    
	public static void main(String[] args) {
	    int	[] a = new int [] {19,28,37,46,50};
	    
	        System.out.println("交换前:");
	    for (int i = 0; i < a.length; i++) {
			System.out.print(a[i]+",");
		}
        
        int c [] = new int [5];
        c=b(a);        
        System.out.println();
        System.out.println("交换后:");
             
        for (int i = 0; i < c.length; i++) {
			System.out.print(c[i]+",");			
		}     
	}
	
    public static int [] b (int [] a){
    	int num ;
    	for (int i = 0; i < a.length/2; i++) {
			  num= a[i];
			  a[i]=a[a.length-1-i]; 
			  a[a.length-1-i]=num;
		}   	
    	return a;
    }
}

