package 第三次课作业;
//用方法实现数组的排序。
//定义一个函数，实现数组从大到小排序，传入一个乱序数组，返回一个从大到小排序的数组
public class PaiXu {

	public static void main(String[] args) {
       int [] a = new int [] {22,19,80,57,20,66};
       
        System.out.println("排序前:");
       for (int i = 0; i < a.length; i++) {		
    	System.out.print(a[i]+","); 
	}
              
       int [] b = b(a);
        System.out.println();
        System.out.println("排序后:");
       for (int i = 0; i < b.length; i++) {    	
		System.out.print(b[i]+",");
	}
	}	
	
    public static int [] b (int [] a) {
		int num ; 
		for (int i = 0; i < a.length-1; i++) {
			for (int j = 0; j < a.length-1-i; j++) {
			if (a[j]<a[j+1]) {
				num=a[j];
				a[j]=a[j+1];
				a[j+1]=num;
			}
		}					
		}
		return a;
	}
}
