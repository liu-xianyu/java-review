package 第三次课作业;
//已知两个整数变量a = 10，b = 20，使用程序实现这两个变量的数据交换
//​        最终输出a = 20，b = 10;
public class JiaoHuan {

	public static void main(String[] args) {
    int a = 10;
    int b = 20;
    int num ;
    num = a;
    a = b;
    b = num;
    System.out.println("输出前:a=10,b=20");   
    System.out.println("输出后:"+"a="+a+","+"b="+b+";");
	}

}