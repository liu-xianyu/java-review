import java.util.Scanner;

public class Demo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//如果一个数组保存元素是有序的（从大到小），向这个数组中插入一个数，使得插入后的数组元素仍然保持有序。
		int []arr = {90,70,50,30,10};
		int []newarr = new int [arr.length+1];
		
		printarray(arr);
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("请输入一个数：");
		int num = scanner.nextInt();
		
		int index=-1;
		for (int i = 0; i < arr.length; i++) {
			if (num > arr[i]) {
				index = i;
				break;
			}
		}
			if (index!=-1) {
				for (int i = 0; i < newarr.length; i++) {
					if(i<index) {
						newarr[i]=arr[i];
					}
					if(i==index) {
						newarr[i]=num;
					}
					if(i>index) {
						newarr[i]=arr[i-1];
					}
				}
			}else {
				for (int i = 0; i < newarr.length; i++) {
					if(i==newarr.length-1) {
						newarr[i]=num;
					}else {
						newarr[i]=arr[i];
					}
				}
			}
			printarray(newarr);
	}
	private static void printarray(int[] arr) {
		System.out.print("[");
		for (int i = 0 ; i < arr.length ; i++) {
			if(i == arr.length-1){
				System.out.print(arr[i]);
			}else {
				System.out.print(arr[i]+",");
			}
		}
		System.out.print("]");
	}
}
