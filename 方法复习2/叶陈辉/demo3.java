package demo;

public class demo3 {

	public static void main(String[] args) {
		//数组值转换
		int[] arr = {19, 28, 37, 46, 50};
		int sum;
		int a=0;
		int b=arr.length-1;
		System.out.println("打印前");
		System.out.print("[");
		for (int i = 0; i < arr.length; i++) {
			if(i==arr.length) {
				System.out.print(arr[i]);
			}
			System.out.print(arr[i]+",");
		}
		System.out.println("]");
		System.out.println("转换后");
		for (;a<b;a++,b--) {
			sum=arr[a];
			arr[a]=arr[b];
			arr[b]=sum;
		}
		System.out.print("[");
		for (int i = 0; i < arr.length; i++) {
			if(i==arr.length) {
				System.out.println(arr[i]);
			}else
			System.out.print(arr[i]+",");
		}
		System.out.print("]");
	}

}
