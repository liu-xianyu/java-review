package demo;

import java.util.Scanner;

public class demo2 {

	public static void main(String[] args) {
		// //数组排序
		int[] arr = {50,40,30,10};
		int[] array = new int[arr.length+1];
		Scanner scan = new Scanner(System.in);
		
		System.out.println("请输入一个数：");
		int num = scan.nextInt();
		
		int index = -1;
		for (int i = 0; i < arr.length; i++) {
			if (num>arr[i]) {
				index = i;
				break;
			}
		}
		
		if (index!=-1) {//这种情况说明，在数组中一定有一个数比num小
			for (int i = 0; i <array.length; i++) {
				if (i<index) {
					array[i]=arr[i];
				}
				if (i==index) {
					array[i]=num;
				}
				if (i>index) {
					array[i]=arr[i-1];
				}
			}
		}else {//这种情况，在数组中，每一个数都比num大。
			for (int i = 0; i < array.length; i++) {
				if (i==array.length-1) {
					array[i]=num;
				}else {
					array[i]=arr[i];
				}
			}
		}
	}
	
	
	//打印数组的方法
	public static void printArray(int [] arr) {
		System.out.print("[");
		for (int i = 0; i < arr.length; i++) {
			if (i==arr.length-1) {
				System.out.print(arr[i]);
			}else {
				System.out.print(arr[i]+",");
			}
		}
		System.out.print("]");
	}

}
