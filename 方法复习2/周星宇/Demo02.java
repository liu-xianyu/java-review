package Demo;

import java.util.Iterator;

class Demo02 {
    public static void main(String[] agrs) {
    	int [] arr = {20,48,87,43,22};
    	int temp=-1;
    	for (int i = 0; i < arr.length; i++) {
    		for (int j = 0; j < arr.length-1-i; j++) {
				if (arr[j]<arr[j+1]) {
					temp=arr[j+1];
					arr[j+1]=arr[j];
					arr[j]=temp;
				}
		}
    }
    	print(arr);
}
    public static void print(int[] a) {
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + ",");
		}
	}
}