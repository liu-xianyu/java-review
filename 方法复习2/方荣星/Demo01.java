package fang;

import java.util.Scanner;

public class Demo01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = { 90, 70, 65, 20, 10 };
		int[] newarr = new int[arr.length + 1];
		int index = -1;
		print(arr);
		System.out.println();
		Scanner sc = new Scanner(System.in);
		int num;
		System.out.println("请输入一个数字");
		num = sc.nextInt();
		for (int i = 0; i < arr.length; i++) {
			if (num > arr[i]) {
				index = i;
				break;
			}
		} // 找到索引
		if (index != -1) {
			for (int i = 0; i < newarr.length; i++) {
				if (index > i) {
					newarr[i] = arr[i];
				}
				if (index == i) {
					newarr[i] = num;
				}
				if (index < i) {
					newarr[i] = arr[i - 1];
				}
			}
		} else {// 等于-1 每个数字都比他大
			for (int i = 0; i < newarr.length; i++) {
				if (i == newarr.length - 1) {
					newarr[i] = num;
				} else {
					newarr[i] = arr[i];
				}
			}
		}
		print(newarr);

	}

	public static void print(int[] a) {
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + ",");
		}
	}

}
