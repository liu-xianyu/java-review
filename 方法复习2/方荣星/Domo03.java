package fang;

public class Domo03 {

	public static void main(String[] args) {
//		定义一个方法，用来实现如下功能：
//		​	已知一个数组 arr = {19, 28, 37, 46, 50}; 用程序实现把数组中的元素值交换，

//		​	交换后的数组 arr = {50, 46, 37, 28, 19}; 并在控制台输出交换后的数组元素
		int [] arr={19, 28, 37, 46, 50};
		int temp=0;
		temp=arr[0];
		arr[0]=arr[4];
		arr[4]=temp;
		temp=arr[3];	
		arr[3]=arr[1];
		arr[1]=temp;
		print(arr);
}
	public static void print(int[] a) {
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + ",");
		}
	}
}