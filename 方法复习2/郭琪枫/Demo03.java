import java.util.Random;

public class Demo03 {
	//用方法实现数组的排序。
	//定义一个函数，实现数组从大到小排序，传入一个乱序数组，返回一个从大到小排序的数组。
	public static void main(String[] args) {
		Random rd=new Random();
		int[] arr=new int[8];
		for (int i = 0; i < arr.length; i++) {
			arr[i]=rd.nextInt(100)+1;
		}
		System.out.println("排序之后：");
		sortArray(arr);
	}

	public static void sortArray(int[] arr) {
		int num ;
		for (int i = 0; i < arr.length-1; i++) {
			for (int j = 0; j < arr.length-1-i; j++) {
				if(arr[j]<arr[j+1]) {
					num = arr[j+1];
					arr[j+1]=arr[j];
					arr[j] = num;
				}
			}
		}
		System.out.print("[");
		for (int i = 0; i < arr.length; i++) {
			if (i == arr.length - 1) {
				System.out.print(arr[i]);
			} else {
				System.out.print(arr[i] + ",");
			}
		}
		System.out.println("]");
	}
		
	}

