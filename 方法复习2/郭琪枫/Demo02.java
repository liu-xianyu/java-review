import java.util.Arrays;


public class Demo02 {
//	定义一个方法，用来实现如下功能：
//	​	已知一个数组 arr = {19, 28, 37, 46, 50}; 用程序实现把数组中的元素值交换，
//	​	交换后的数组 arr = {50, 46, 37, 28, 19}; 并在控制台输出交换后的数组元素
	public static void main(String[] args) {
		int[]  arr = {19, 28, 37, 46, 50};
		System.out.println("交换前："+Arrays.toString(arr));
		
		int [] resultArray = reverseArray(arr);
		System.out.println("交换后："+Arrays.toString(resultArray));
	}
	
	public static int[] reverseArray(int[] arr) {
		int start = 0;
		int end = arr.length-1;
		int temp;
		
		
		for (; start < end; start++,end--) {
			temp = arr[end];
			arr[end] = arr[start];
			arr[start] = temp;
		}
		return arr;
	}
}
