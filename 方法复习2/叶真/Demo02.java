package JavaReview;

import java.util.Iterator;
import java.util.Scanner;

public class Demo02 {

	public static void main(String[] args) {
		// 用方法实现数组的排序。
//		定义一个函数，实现数组从大到小排序，
//		传入一个乱序数组，返回一个从大到小排序的数组。
		
		Scanner scan = new Scanner(System.in) ;
		
		int[] arr = new int[5] ;
		
		for (int i = 0; i < arr.length; i++) {
			System.out.println("请输入一个数字：");
			arr[i] = scan.nextInt() ;
		}
		
		System.out.println("排序前：");
		printArr(arr);
		
		System.out.println("请输入一个数字：");
		int num = scan.nextInt();
		
		int[] b = strcArr(arr, num);
		
		System.out.println("转换后：");
		printArr(b);

	}
	
	public static void printArr(int[] arr) {
		System.out.print("[");
		
		for (int i = 0; i < arr.length; i++) {
			if (i == arr.length - 1) {
				System.out.print(arr[i]);
			}else {
				System.out.print(arr[i]);
				System.out.print(",");
			}
		}
		
		System.out.println("]");
	}
	
	public static int[] strcArr(int[] arr ,int num) {
		
		int[] newarr = new int[arr.length+1];
		
		for (int i = 0; i < arr.length -1; i++) {
			for (int j = 0; j < arr.length -1; j++) {
				if (arr[j] < arr[j + 1]) {
					int temp = arr[j] ;
					arr[j] = arr[j + 1] ;
					arr[j+1] = temp ; 
				}
			}
		}
		
		int index = -1 ;
		
		for (int i = 0; i < arr.length; i++) {
			if (num < arr[i]) {
				index = i ;
				break;
			}
		}
		
		for (int i = 0; i < newarr.length; i++) {
			if (index > i) {
				newarr[i] = arr[i];
			}else if (index == i) {
				newarr[i] = num ;
			}else if (index < i) {
				newarr[i] = arr[i - 1];
			}
		}
		
		return newarr;
	}

}
