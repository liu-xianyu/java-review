import java.util.Arrays;


public class Demo06{

	public static void main(String[] args) {
		int[]  arr = {19, 28, 37, 46, 50};
		System.out.println(Arrays.toString(arr));
		
		int [] resultArray = reverseArray(arr);
		
		System.out.println(Arrays.toString(resultArray));
	}
	
	public static int[] reverseArray(int[] arr) {
		int start = 0;
		int end = arr.length-1;
		int temp;
		
		//˼·1
		for (; start < end; start++,end--) {
			temp = arr[end];
			arr[end] = arr[start];
			arr[start] = temp;
		}

	
		
		return arr;
	}
}
