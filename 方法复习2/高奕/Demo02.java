import java.util.Scanner;

public class Demo02 {

	public static void main(String[] args) {
		// 用方法实现数组的排序。
		//定义一个函数，实现数组从大到小排序，传入一个乱序数组，返回一个从大到小排序的数组。

		Scanner sc =new Scanner(System.in);
		int [] num= {40,50,30,70,90};
		
		
		arr(num);
		for (int i = 0; i < num.length; i++) {
			System.out.print(num[i]+" ");
		}
		
	}
	public static int [] arr(int [] num) {
		int temp;
		for (int i = 0; i < num.length-1; i++) {
			for (int j = 0; j < num.length-1-i; j++) {
				if (num[j]<num[j+1]) {
					temp=num[j+1];
					num[j+1]=num[j];
					num[j]=temp;
					
				}
			}
			
		}
		
		return num;
	}

}
