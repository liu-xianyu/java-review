
public class Demo04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//用方法实现数组的排序。
		//定义一个函数，实现数组从大到小排序，传入一个乱序数组，返回一个从大到小排序的数组。
		int [] array = {50,80,64,55,6};
		int temp=-1;
		for (int i = 0; i < array.length-1; i++) {
			for (int j = 0; j < array.length-1-i; j++) {
				if (array[j]<array[j+1]) {
					temp=array[j+1];
					array[j+1]=array[j];
					array[j]=temp;
				}
			}
		}
		print(array);
	}
	
	public static void print(int[] a) {
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + ",");
		}
	}
}