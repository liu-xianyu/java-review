
public class Demo03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//定义一个方法，用来实现如下功能：已知一个数组 arr = {19, 28, 37, 46, 50}; 用程序实现把数组中的元素值交换，交换后的数组 arr = {50, 46, 37, 28, 19}; 并在控制台输出交换后的数组元素
		int []array = {19, 28, 37, 46, 50};
		System.out.println("排序之前：");
		printarray(array);
		
		System.out.println("排序之后：");
		printarray(sortarray(array));
	}
	
	public static int[]  sortarray(int[] arr) {
		int num;
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr.length-1-i; j++) {
				if (arr[j]<arr[j+1]) {
					num = arr[j+1];
					arr[j+1]=arr[j];
					arr[j]=num;
				}
			}
		}
		return arr;
	}
	private static void printarray(int[] arr) {
		System.out.print("[");
		for (int i = 0 ; i < arr.length ; i++) {
			if(i == arr.length-1){
				System.out.print(arr[i]);
			}else {
				System.out.print(arr[i]+",");
			}
		}
		System.out.print("]");
	}
}
