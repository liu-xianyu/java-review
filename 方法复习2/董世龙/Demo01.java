package JavaReview;

public class Demo01 {

	public static void main(String[] args) {
		// 已知两个整数变量a = 10，b = 20，使用程序实现这两个变量的数据交换
//		​        最终输出a = 20，b = 10;
		
		int a = 10 ;
		int b = 20 ;
		int temp ;
		
		temp = a ;
		a = b ;
		b = temp ;

	}

}
