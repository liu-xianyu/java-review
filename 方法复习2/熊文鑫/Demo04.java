import java.util.Random;

public class Demo04 {

	public static void main(String[] args) {
		// 定义一个方法，实现的功能是：随机生成银行卡后7位数，返回的数据格式如：0586 756
		//方法：Random   数组
		//字符串截取
		Random ran = new Random();
		
		int num1 = ran.nextInt(9999) ;
		int num2 = ran.nextInt(999);
		
		System.out.println(num1 + " " + num2);

	}
}

