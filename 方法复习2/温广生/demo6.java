package less_1;

public class demo6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		已知两个整数变量a = 10，b = 20，使用程序实现这两个变量的数据交换
//				​        最终输出a = 20，b = 10;
		int a=10;
		int b=20;
		int temp;
		System.out.println("交换前"+a+","+b);
		temp=a;
		a=b;
		b=temp;
		System.out.println("交换后"+a+","+b);
	}

}
