
public class arrayJH2 {

	public static void main(String[] args) {
//		定义一个方法，用来实现如下功能：
//		​	已知一个数组 arr = {19, 28, 37, 46, 50}; 用程序实现把数组中的元素值交换，
//		​	交换后的数组 arr = {50, 46, 37, 28, 19}; 并在控制台输出交换后的数组元素
		int[] arr= {19, 28, 37, 46, 50};
		printArray(arr);
		System.out.println();
		System.out.println("交换后");
		printArray(newarr(arr));
	}
	public static int[] newarr(int[] arr) {
		int end=arr.length-1;
		for (int i = 0; i < end; i++,end--) {
			int temp = arr[i];
			arr[i]=arr[end];
			arr[end]=temp;
		}
		return arr;
	}
	public static void printArray(int [] a) {
		System.out.print("[");
		for (int i = 0; i < a.length; i++) {
			if (i==a.length-1) {
				System.out.print(a[i]);
			}else {
				System.out.print(a[i]+",");
			}
		}
		System.out.print("]");
	}
}
