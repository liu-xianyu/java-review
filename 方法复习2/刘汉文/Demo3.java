import java.util.Scanner;

public class Demo3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = {90,70,50,30,10};//
		int[] newArr = new int[arr.length+1];
		
		printArray(arr);
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("请输入一个数：");
		int num = sc.nextInt();
		
		int index = -1;
		for (int i = 0; i < arr.length; i++) {
			if (num>arr[i]) {
				index = i;
				break;
			}
		}
		
		if (index!=-1) {//这种情况说明，在数组中一定有一个数比num小
			for (int i = 0; i < newArr.length; i++) {
				if (i<index) {
					newArr[i]=arr[i];
				}
				if (i==index) {
					newArr[i]=num;
				}
				if (i>index) {
					newArr[i]=arr[i-1];
				}
			}
		}else {//这种情况，在数组中，每一个数都比num大。
			for (int i = 0; i < newArr.length; i++) {
				if (i==newArr.length-1) {
					newArr[i]=num;
				}else {
					newArr[i]=arr[i];
				}
			}
		}
		
		printArray(newArr);
	}
	
	
	
	//打印数组的方法
	public static void printArray(int [] arr) {
		System.out.print("[");
		for (int i = 0; i < arr.length; i++) {
			if (i==arr.length-1) {
				System.out.print(arr[i]);
			}else {
				System.out.print(arr[i]+",");
			}
		}
		System.out.print("]");
	}

}
