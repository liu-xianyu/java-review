package JavaReview;

public class Demo04 {

	public static void main(String[] args) {
		// 定义一个方法，用来实现如下功能：
//		​	已知一个数组 arr = {19, 28, 37, 46, 50};
//		用程序实现把数组中的元素值交换，
//		​	交换后的数组 arr = {50, 46, 37, 28, 19}; 
//		并在控制台输出交换后的数组元素
		
		int[] arr = {19,28,37,46,50} ;
		
		System.out.println("转换前：");
		printArr(arr);
		
		exchangeArr(arr);
		System.out.println("转换后：");
		printArr(arr);

	}
	public static int[] exchangeArr(int[] arr) {
		
		for (int start = 0,end = arr.length - 1; start < arr.length - 1; start++,end--) {
			if (start == end) {
				break;
			}else {
				if (start < end) {
					int temp = arr[start] ;
					arr[start] = arr[end] ;
					arr[end] = temp ;
				}
			}
		}
		
		return arr;
	}
	
	public static void printArr(int[] arr) {
		System.out.print("[");
		
		for (int i = 0; i < arr.length; i++) {
			if (i == arr.length - 1) {
				System.out.print(arr[i]);
			}else {
				System.out.print(arr[i]);
				System.out.print(",");
			}
		}
		
		System.out.println("]");
	}

}
