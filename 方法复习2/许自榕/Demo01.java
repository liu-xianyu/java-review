public class Demo01 {
	public static void main(String[] args) {
		int[] array = {19, 28, 37, 46, 50};
		
		System.out.println("??");
		printArray(array);

		System.out.println("?");
		printArray(sortArray(array));
	}

	public static int[] sortArray(int[] arr) {
		int num ;
		for (int i = 0; i < arr.length-1; i++) {
			for (int j = 0; j < arr.length-1-i; j++) {
				if(arr[j]<arr[j+1]) {
					num = arr[j+1];
					arr[j+1]=arr[j];
					arr[j] = num;
				}
			}
		}
		return arr;
	}

	public static void printArray(int[] arr) {
		System.out.print("[");
		for (int i = 0; i < arr.length; i++) {
			if (i == arr.length - 1) {
				System.out.print(arr[i]);
			} else {
				System.out.print(arr[i] + ",");
			}
		}
		System.out.println("]");
	}
}