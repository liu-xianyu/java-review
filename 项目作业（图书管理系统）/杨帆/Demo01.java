package a;

import java.util.Scanner;

public class Demo01 {

	static Scanner abc = new Scanner(System.in);
	static String[][] users = new String[20][4];
	static String[][] books = new String[20][5];
	static String[][] press = new String[20][3];

	public static void main(String[] args) {
		init();
		homepage();
	}

	private static void init() { // 初始化
		users[0][0] = "学习部";
		users[0][1] = "admin";
		users[0][2] = "123";
		users[0][3] = "管理员";

		users[1][0] = "学习部";
		users[1][1] = "张三";
		users[1][2] = "123";
		users[1][3] = "普通用户";

		users[2][0] = "体育部";
		users[2][1] = "李四";
		users[2][2] = "123";
		users[2][3] = "普通用户";

		users[3][0] = "宣传部";
		users[3][1] = "王五";
		users[3][2] = "123";
		users[3][3] = "普通用户";

		books[0][0] = "9781234567";
		books[0][1] = "高数";
		books[0][2] = "50";
		books[0][3] = "北京出版社";
		books[0][4] = "张三";

		books[1][0] = "9789261347";
		books[1][1] = "体育与健康";
		books[1][2] = "30";
		books[1][3] = "南京出版社";
		books[1][4] = "李四";

		books[2][0] = "9789754312";
		books[2][1] = "宣传手册";
		books[2][2] = "10";
		books[2][3] = "东京出版社";
		books[2][4] = "王五";

		books[3][0] = "9781231358";
		books[3][1] = "大学英语";
		books[3][2] = "50";
		books[3][3] = "南京出版社";
		books[3][4] = "张三";

		press[0][0] = "北京出版社";
		press[0][1] = "北京";
		press[0][2] = "张三";

		press[1][0] = "南京出版社";
		press[1][1] = "南京";
		press[1][2] = "李四";

		press[2][0] = "东京出版社";
		press[2][1] = "东京";
		press[2][2] = "王五";

		press[3][0] = "天津出版社";
		press[3][1] = "天津";
		press[3][2] = "admin";
	}

	private static void homepage() { // 欢迎
		while (true) {
			System.out.println("欢迎来到闽大图书管理系统！");
			System.out.println("请输入数字进行选择：1.登录 2.注册");

			int key1 = abc.nextInt();

			if (key1 == 1) {
				login();
			} else if (key1 == 2) {
				register();
			} else {
				System.out.println("输入错误！请重新输入");
			}
		}
	}

	private static void login() { // 登录
		System.out.println("请输入用户名：");
		String usersname = abc.next();

		System.out.println("请输入密码：");
		String password = abc.next();

		boolean flag1 = false;

		for (int i = 0; i < users.length; i++) {
			if (usersname.equals(users[i][1]) && password.equals(users[i][2])) {
				flag1 = true;
				break;
			}
		}
		if (flag1 == true) {
			System.out.println("登录成功！");
			system();
		} else {
			System.out.println("该用户不存在或者密码错误！请重新登录！");
			homepage();
		}

	}

	private static void register() { // 注册
		int index1 = -1;

		for (int i = 0; i < users.length; i++) {
			if (users[i][0] == null) {
				index1 = i;
				break;
			}
		}

		System.out.println("请输入所属部门：");
		users[index1][0] = abc.next();

		System.out.println("请输入用户名：");
		users[index1][1] = abc.next();

		System.out.println("请设置登录密码：");
		users[index1][2] = abc.next();

		users[index1][3] = "普通用户";

		System.out.println("注册成功！");

	}

	private static void system() { // 欢迎您使用闽大书籍管理系统
		System.out.println("欢迎您使用闽大书籍管理系统！！！");
		System.out.println("请输入数字进行选择：1 图书管理  2 出版社管理  3 退出登录  4 退出系统");

		int key2 = abc.nextInt();

		switch (key2) {
		case 1:
			Library();
			break;
		case 2:
			press1();
			break;
		case 3:
			System.out.println("已退出登录！");
			homepage();
			break;
		case 4:
			System.exit(0);
			break;
		}

	}

	private static void Library() { // 图书管理系统
		while (true) {
			System.out.println("请输入：1.增加 2.删除 3.更新 4.查询 5.返回上一级菜单");

			int b1 = abc.nextInt();

			switch (b1) {
			case 1: // 增加
				zengjia1();
				break;
			case 2: // 删除
				shanchu1();
				break;
			case 3: // 更新
				gengxin1();
				break;
			case 4: // 查询
				chaxun1();
				break;

			case 5: // 返回上一级
				system();
				break;

			}
		}

	}

	private static void zengjia1() { // 增加图书
		int index2 = -1;
		for (int i = 0; i < books.length; i++) {
			if (users[i][0] == null) {
				index2 = i;
				break;
			}
		}
		System.out.println("请输入图书ISBN:");
		String num = abc.next();

		boolean flag2 = false;
		for (int i = 0; i < books.length; i++) {
			if (num.equals(books[i][0])) {
				flag2 = true;
				break;
			}
			if (flag2 == true) {
				System.out.println("增加失败！");
				System.out.println("ISBN重复，请重新输入ISBN");

			} else {
				books[index2][0] = num;
				System.out.println("请输入书名:");
				books[index2][1] = abc.next();

				System.out.println("请输入价格:");
				books[index2][2] = abc.next();

				System.out.println("请输入出版社:");
				books[index2][3] = abc.next();

				System.out.println("请输入作者:");
				books[index2][4] = abc.next();

				System.out.println("增加成功！");

				break;
			}

		}
	}

	private static void shanchu1() { // 删除图书
		System.out.println("请输入要删除的书本名：");
		String index3 = abc.next();
		int ocxigus = -1;
		for (int i = 0; i < books.length; i++) {
			if (books[i][1] != null) {
				if (index3.equals(books[i][1])) {
					ocxigus = i;
				}
			}
		}
				if (ocxigus != -1) {									
					books[ocxigus][0] = null;
					books[ocxigus][1] = null;
					books[ocxigus][2] = null;
					books[ocxigus][3] = null;
					books[ocxigus][4] = null;
					System.out.println("删除成功！");					
				} else {
					System.out.println("没有此图书！请重新输入！");
				}
	}

	private static void chaxun1() { // 查询图书
		System.out.println("请输入查询种类：1.ISBN 2.书名（模糊） 3.出版社 4. 作者 5. 价格范围 6.查询所有 7.返回上一级");
		int key3 = abc.nextInt();
		switch (key3) {
		case 1:// ISBN
			System.out.println("请输入要查找的ISBN：");
			String asdf = abc.next();

			int zxgy = -1;
			boolean zlxgkj = false;
			for (int i = 0; i < books.length; i++) {
				if (asdf.equals(books[i][0])) {
					zlxgkj = true;
					zxgy = i;
				}
			}
			if (zlxgkj == true) {
				System.out.println("ISBN\t\t书名\t价格\t出版社\t作者");
				for (int i = 0; i < books.length; i++) {
					for (int j = 0; j < books[i].length; j++) {
						System.out.print(books[zxgy][j] + "\t");
					}
					break;
				}
			}
			System.out.println();
			break;
		case 2:// 书名（模糊）
			System.out.println("请输入要查找的关键字：");
			String prf = abc.next();

			System.out.println("ISBN\t\t书名\t价格\t出版社\t作者");
			for (int i = 0; i < books.length; i++) {
				if (books[i][1] != null) {
					if (prf.indexOf(books[i][1]) != -1) {
						for (int j = 0; j < books[i].length; j++) {
							System.out.print(books[i][j] + "\t");
						}
					} else {
						System.out.println();
						System.out.println("没有与此关键字有关的图书！请重新输入！");
						break;
					}
				}
			}
			System.out.println();
			break;
		case 3:// 出版社
			System.out.println("请输入要查找的出版社：");
			String qwe = abc.next();

			System.out.println("ISBN\t\t书名\t价格\t出版社\t作者");
			for (int i = 0; i < books.length; i++) {
				if (qwe.equals(books[i][3])) {
					for (int j = 0; j < books[i].length; j++) {
						System.out.print(books[i][j] + "\t");
					}
				}
			}
			System.out.println();
			break;
		case 4:// 作者
			System.out.println("请输入要查找的作者：");
			String rty = abc.next();

			System.out.println("ISBN\t\t书名\t价格\t出版社\t\t作者");
			for (int i = 0; i < books.length; i++) {
				if (books[i][0] != null && rty.equals(books[i][4])) {
					System.out.print(books[i][0] + "\t" + books[i][1] + "\t" + books[i][2] + "\t" + books[i][3] + "\t"+ books[i][4]);
					System.out.println();
				}
			}

			break;
		case 5:// 价格范围
			System.out.println("请输入要查找的价格范围：");
			System.out.println("请输入最低价格：");
			int min = abc.nextInt();

			System.out.println("请输入最高价格：");
			int max = abc.nextInt();

			System.out.println("ISBN\t\t书名\t价格\t出版社\t作者");
			for (int i = 0; i < books.length; i++) {
				if (min <= Integer.parseInt(books[i][2]) && Integer.parseInt(books[i][2]) <= max) {
					for (int j = 0; j < books[i].length; j++) {
						System.out.print(books[i][j] + "\t");
					}
					System.out.println();
				}
				break;
			}
			break;
		case 6:// 查询所有
			System.out.println("ISBN\t\t书名\t价格\t出版社\t作者");
			for (int i = 0; i < books.length; i++) {
				if (books[i][0] != null) {
					for (int j = 0; j < books[i].length; j++) {
						if (books[i][0] != null) {
							System.out.print(books[i][j] + "\t");
						}
					}
					System.out.println();
				}
			}
			System.out.println();
			break;
		case 7:// 返回上一级
			Library();
			break;
		}
	}

	private static void gengxin1() { // 更新图书
		System.out.println("请输入ISBN号：");
		String zxc = abc.next();	
		int xclibhy = -1;
		System.out.println("ISBN\t\t书名\t价格\t出版社\t\t作者");
		for (int i = 0; i < books.length; i++) {
			if (books[i][0] != null && zxc.equals(books[i][0])) {
				System.out.print(books[i][0] + "\t" + books[i][1] + "\t" + books[i][2] + "\t" + books[i][3] + "\t"+ books[i][4]);
				System.out.println();
				xclibhy = i;
			}
		}
		
		if (xclibhy != -1) {			
		System.out.println("请输入新的书名：");
		books[xclibhy][1] = abc.next();

		System.out.println("请输入新的价格：");
		books[xclibhy][2] = abc.next();

		System.out.println("请输入新的出版社：");
		books[xclibhy][3] = abc.next();

		System.out.println("请输入新的作者：");
		books[xclibhy][4] = abc.next();

		System.out.println("更新成功！！！");		
		} else {
			System.out.println("没有此图书！请重新输入！");						
		}
	}

	private static void press1() { // 出版社
		while (true) {

			System.out.println("请输入：1.增加 2.删除 3.更新 4.根据出版社名称查询 5.查询所有出版社 6.返回上一级菜单");
			int key4 = abc.nextInt();
			switch (key4) {
			case 1: // 增加
				zengjia2();
				break;
			case 2: // 删除
				shanchu2();
				break;
			case 3: // 更新
				gengxin2();
				break;
			case 4: // 根据出版社名称查询
				System.out.println("请输入要查找的出版社名称：");
				String bnm = abc.next();

				System.out.println("出版社名称\t地址\t联系人");
				for (int i = 0; i < press.length; i++) {
					if (bnm.equals(press[i][0])) {
						for (int j = 0; j < press[i].length; j++) {
							if (press[i][0] != null) {
								System.out.print(press[i][j] + "\t");
							}
						}
						System.out.println();
					}
				}
				System.out.println();
				break;
			case 5: // 查询所有出版社
				System.out.println("出版社名称\t地址\t联系人");
				for (int i = 0; i < press.length; i++) {
					if (press[i][0] != null) {
						for (int j = 0; j < press[i].length; j++) {
							if (press[i][0] != null) {
								System.out.print(press[i][j] + "\t");
							}
						}
						System.out.println();
					}
				}
				System.out.println();
				break;
			case 6: // 返回上一级菜单
				system();
				break;

			}
		}
	}

	private static void zengjia2() { // 增加出版社
		int index2 = -1;
		
		boolean dfhfgh = false;		
		
		System.out.println("请输入出版社名称：");
		String pressname = abc.next();
		for (int i = 0; i < press.length; i++) {
			if (pressname == press[i][0] && press[i][0] == null) {
				dfhfgh = true;
				index2 = i;
				break;
			}			
		}								
		if (dfhfgh == true) {
			press[index2][0] = pressname;
			
			System.out.println("请输入出版社地址：");
			press[index2][1] = abc.next();
			
			System.out.println("请输入出版社联系人：");
			press[index2][2] = abc.next();
			
			System.out.println("出版社添加成功！");			
		}else {
			System.out.println("已有此出版社！请重新输入！");
		}
	}

	private static void shanchu2() { // 删除出版社
		System.out.println("请输入要删除的出版社名称：");
		String index4 = abc.next();
		int xikvhsdog = -1;
		for (int i = 0; i < press.length; i++) {
			if (index4.equals(press[i][0])) {
				if (press[i][0].equals(books[i][3])) {
					System.out.println("该出版社有相关图书信息存在！不可以删除！如果要删除请先删除所有相关图书信息！！！");
					System.out.println("删除失败！");
				} else {
					xikvhsdog = i;
				}
			}
		}
				if (xikvhsdog != -1) {
					press[xikvhsdog][0] = null;
					press[xikvhsdog][1] = null;
					press[xikvhsdog][2] = null;
					System.out.println("删除成功！");					
				} else {
					System.out.println("没有此出版社！请重新输入！");	
				}

	}

	private static void gengxin2() { // 更新出版社
		System.out.println("请输入要更新的出版社名称：");
		String cag = abc.next();
		for (int i = 0; i < press.length; i++) {
			if (cag.equals(press[i][0])) {
				System.out.println("出版社名称\t地址\t联系人");
				if (press[i][0] != null) {
					for (int j = 0; j < press[i].length; j++) {
						System.out.print(press[i][j] + "\t");
					}
					System.out.println();
				}
				System.out.println();

				System.out.println("请输入要更新的地址：");
				press[i][1] = abc.next();

				System.out.println("请输入新的联系人：");
				press[i][2] = abc.next();

				System.out.println("更新成功！！！");
			} else {
				System.out.println("没有此出版社！请重新输入！");
				break;
			}
		}
	}

}