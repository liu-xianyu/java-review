import java.util.Scanner;

public class Demo02 {

	public static void main(String[] args) {
		// 1.	采用二维数组存放用户信息（部门、用户名、密码、用户角色）、
//		书籍信息（编码（ISBN）、书籍名称、价格、出版社、作者）、
//		出版社信息（出版社名称、地址、联系人）
		Scanner scan = new Scanner(System.in);
		
		String[][] users = new String[100][4];
		String[][] books = new String[100][5];
		String[][] presses = new String[100][3];
		
//		2.	初始化用户信息、出版社信息、书籍信息；（设置基础信息，赋值；）
		
		begin(users, books, presses);
		
//		3.	用户登录功能：请用户输入账号密码，对比用户信息是否正确：
//		1）	不正确提示“该用户不存在或者密码错误！请重新登录！”
//		2）	正确，进入系统菜单，“1 图书管理  2 出版社管理  3 退出系统” 
		
		start(users, books);
		
	}
	
	//开始页面
	public static void start(String[][] users,String[][] books) {
		Scanner scan = new Scanner(System.in);
		System.out.println("欢迎来到闽大图书管理系统！");
		while (true) {
			System.out.println("1.登录	2.注册");
			int key = scan.nextInt();
			
			if (key == 1) {//登录
				
				login(users, books);
				
			}else if (key == 2) {//注册
				
				register(users);
				
			}else {
				System.out.println("输入错误！请重新输入！");
			}
		}
	}
	
	//初始化
	public static void begin(String[][] users,String[][] books,String[][] presses) {
		users[0][0] = "社畜部";
		users[0][1] = "admin";
		users[0][2] = "123456";
		users[0][3] = "管理员";
		
		users[1][0] = "财务部";
		users[1][1] = "张三";
		users[1][2] = "123456";
		users[1][3] = "管理员";
		
		books[0][0] = "10616501";
		books[0][1] = "三体全集";
		books[0][2] = "139RMB";
		books[0][3] = "重庆出版社";
		books[0][4] = "刘慈欣";
		
		presses[0][0] = "重庆出版社";
		presses[0][1] = "重庆市";
		presses[0][2] = "陈兴芜";
	}
	
	//登录
	public static String[][] login(String[][] users,String[][] books) {
		Scanner scan = new Scanner(System.in);
		
		while (true) {
			System.out.println("请输入用户名：");
			String username = scan.next();
			
			System.out.println("请输入密码：");
			String password = scan.next();
			
			boolean flag = false;
			
			for (int i = 0; i < users.length; i++) {
				if (username.equals(users[i][1]) && password.equals(users[i][2])) {
					flag = true;
					break;
				}
			}
			
			if (flag == true) {
				System.out.println("登录成功！");
				homepage(username, books, users);
			}else {
				System.out.println("该用户不存在或者密码错误！请重新登录！");
			}
		}
	}
	
	//注册
	public static void register(String[][] users) {
		Scanner scan = new Scanner(System.in);
		int index = -1;
		
		for (int i = 0; i < users.length; i++) {
			if (users[i][0] == null) {
				index = i;
				break;
			}
		}
		
		System.out.println("请输入所属部门：");
		users[index][0] = scan.next();
		
		System.out.println("请输入用户名：");
		users[index][1] = scan.next();
		
		System.out.println("请输入密码：");
		users[index][2] = scan.next();
		
		users[index][3] = "普通用户";
		System.out.println("您的账号为普通用户。");
		
		System.out.println("注册成功！");
		
	}
	
	//主页
	public static void homepage(String username,String[][] books,String[][] users) {
		Scanner scan = new Scanner(System.in);
		System.out.println(username + "， 欢迎您使用闽大书籍管理系统！！！");
		System.out.println("请输入数字进行选择：1 图书管理  2 出版社管理  3 退出登录  4 退出系统");
		
		int key = scan.nextInt();
		
		switch (key) {
		case 1://图书管理
			librarian(books);
			break;
		case 2://出版社管理
			
			break;
		case 3://退出登录
			start(users, books);
			break;
		case 4://退出系统
			System.exit(0);
			break;

		default:
			System.out.println("输入错误，请重新输入！");
			break;
		}
	}
	
	//图书管理
//	1)	增加：图书编码唯一，如果新增的图书信息编码已存在，提示“图书已存在”；
//		不存在才进行新增书籍信息；
//	2)	删除：根据ISBN编码删除图书；
//	3)	更新：ISBN唯一不能更改，根据ISBN更新相应的书籍信息；
//	4)	查询菜单：
//	a)	根据ISBN查询
//	b)	根据书名查询（模糊）
//	c)	根据出版社查询
//	d)	根据作者查询
//	e)	根据价格范围查询
//	f)	查询所有书籍信息
//	g)	返回上一级菜单
	public static void librarian(String[][] books){
		Scanner scan = new Scanner(System.in);
		
		System.out.println("1、增加\t2、删除\t3、更新\t4、查询菜单");
		int key = scan.nextInt();
		
		switch (key) {
		case 1://增加
			add(books);
			break;
		case 2://删除
				
			break;
		case 3://更新
			
			break;
		case 4://查询菜单
			
			break;

		default:
			break;
		}
	}
	
	//图书管理——增加
	//图书编码唯一，如果新增的图书信息编码已存在，提示“图书已存在”；
//	不存在才进行新增书籍信息；
	public static void add(String[][] books) {
		Scanner scan = new Scanner(System.in);
		int index1 = -1;
		
		while (true) {
		System.out.println("请输入增加书目的图书编码：");
		String index = scan.next();
		
		for (int i = 0; i < books.length; i++) {
			if (index == books[i][0]) {
				System.out.println("图书已存在！");
				break;
			}else {
				for (int j = 0; j < books.length; j++) {
					if (books[j][0] == null) {
						index1 = j;
						break;
					}
				}
				books[index1][0] = index;
				
				System.out.println("请输入书名：");
				books[index1][0] = scan.next();
			}
		}
		}
		
	}

}
