package lesson;

import java.util.Scanner;


public class Demo02 {
	static String[][] users = new String[100][4];
	static String[][] books = new String[100][5];
	static String[][] press = new String[100][3];
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		init();

		homepage();

	}

	private static void homepage() {
		while (true) {
			System.out.println("欢迎来到闽大图书管理系统！");
			System.out.println("1.登录    2.注册");
			int num = sc.nextInt();
			if (num == 1) {
				login();
			} else if (num == 2) {
				register();
			} else {
				System.out.println("选项错误，请重新选择");
			}
		}
	}

	private static void login() { // 登录
		System.out.println("请输入用户名");
		String name = sc.next();
		System.out.println("请输入密码");
		String password = sc.next();
		boolean flag = false;
		for (int i = 0; i < users.length; i++) {
			if (name.equals(users[i][1]) && password.equals(users[i][2])) {
				flag = true;
				break;
			}
		}
		if (flag == true) {
			homepage1(name);

		} else if (flag == false) {
			System.out.println("该用户不存在或者密码错误！请重新登录！");
		}

	}

	private static void homepage1(String name) {
		System.out.println(name + "， 欢迎您使用闽大书籍管理系统！！！");
		System.out.println("请输入数字进行选择：1 图书管理  2 出版社管理  3 退出登录  4 退出系统");

		int num2 = sc.nextInt();
		switch (num2) {
		
		case 1:
			bookpage(name);
			break;
		case 2:
			while (true) {
				pressamin(name);
			}
			
		case 3:
			System.out.println("已退出登录");
			homepage();
			break;
		case 4:
			System.exit(0);
			break;

		default:
			break;
		}
	}

	private static void pressamin(String name) {
		System.out.println("请输入：1.增加 2.删除 3.更新 4.根据出版社名称查询 5.查询所有出版社 6.返回上一级菜单");
		int numkey =sc.nextInt();
		switch(numkey) {
		case 1:
		addpressname();	
		
		break;
		case 2:
		if (numkey==2) {
			
			boolean flag3=false;
			
			System.out.println("请输入要删除的出版社名称");
			String name2=sc.next();
			for (int i = 0; i < books.length; i++) {
				if (books[i][0]!=null) {
					if (name2.equals(books[i][3])) {
					flag3=true;
				}
				}
			}
			if (flag3==true) {
				System.out.println("删除失败");
			}else {
				System.out.println("删除成功");
			}
		}	
			
			break;
		case 3:
			if (numkey==3) {
				int index=-1;
				boolean nameflag=false;
				System.out.println("请输入要更新的出版社名称");
				String name1=sc.next();
				for (int i = 0; i < books.length; i++) {
					if (name1.equals(press[i][0])) {
						nameflag=true;
						index=i;
						break;
					}
				}
				if (nameflag==true) {
					System.out.println("请输入新的出版社名称");
					press[index][0]=sc.next();
					System.out.println("请输入新的出版社地址");
					press[index][1]=sc.next();
					System.out.println("请输入新的出版社联系人");
					press[index][2]=sc.next();
				}else {
					System.out.println("出版社不存在");
				}
			}
			break;
		case 4:
			int index1=-1;
			boolean flag2=false;
			
			System.out.println("请输入要查询的出版社名称");
			String name1=sc.next();
			for (int i = 0; i < press.length; i++) {
				if (press[i][0]!=null) {
					if (name1.indexOf(press[i][0]) != -1) {
					flag2=true;
					index1=i;
				}
				}
				
			}
			if (flag2==true) {
				System.out.println("出版社名称\t地址\t联系人");
				for (int i = 0; i < 3; i++) {
					System.out.print(press[index1][i]+"\t");
				}System.out.println();
			}else {
				System.out.println("查询不到相关出版社");
			}
			
			break;
		case 5:
			System.out.println("出版社名称\t地址\t联系人");
			for (int i = 0; i < press.length; i++) {
				if (press[i][0]!=null) {
				for (int j = 0; j < press[i].length; j++) {
						System.out.print(press[i][j]+"\t");
					}					
				System.out.println();
				}
			}
			
			break;
		case 6:
			homepage1(name);
			break;
		
		
		}
		
		
	}

	private static void addpressname() {
		boolean pressflag =false; 
		int index = -1; 
		for (int i = 0; i < press.length; i++) {
			if (press[i][0]==null) {
				index = i;
			}
		}
		
		System.out.println("请输入要添加的出版社名称");
		String pressname=sc.next();
		
		for (int i = 0; i < press.length; i++) {
			if (pressname.equals(press[i][0])) {
				pressflag = true;				
				break;
			}
		}
		if (pressflag == true) {
			System.out.println("出版社已存在");
			
		}else {
			press[index][0] = pressname;
			
			System.out.println("请输入出版社地址");
			press[index][1]=sc.next();
			
			System.out.println("请输入出版社联系人");
			press[index][2]=sc.next();
			
			System.out.println("添加成功");
		}
	}

	private static void bookpage(String name) {
		while (true) {
			System.out.println("请输入：1.增加 2.删除 3.更新 4.查询 5.返回上一级菜单");
			int num3 = sc.nextInt();
			if (num3 == 1) {
				increase();
			} else if (num3 == 2) {
				delete();
			} else if (num3 == 3) {
				update();
			} else if (num3 == 4) {
				bookmanage(name);
				
			} else if (num3 == 5) {
				homepage1(name);
			}
		}
	}

	private static void bookmanage(String name) {
		while (true) {
			System.out.println("请输入查询种类：1.isbn 2.书名（模糊） 3.出版社 4. 作者 5. 价格范围 6.查询所有 7.返回上一级");
			int numkey = sc.nextInt();
			switch (numkey) {
			case 1:
				bookisbn();
				break;
			case 2:
				bookname();
				break;
			case 3:
				bookpress();
				break;
			case 4:
				bookwriter();
				break;
			case 5:
				bookpirce();
				break;
			case 6:
				bookall();
				
				break;
			case 7:
				bookpage(name);
				break;
			default:
				break;
			}
		}
		
	}

	private static void bookall() {		//打印所有
		for (int i = 0; i < books.length; i++) {
			if (books[i][0] != null) {
				for (int j = 0; j < books[i].length; j++) {
					System.out.print(books[i][j] + "\t");
				}
				System.out.println();
			}
		}

		
	}

	private static void bookpirce() {
		System.out.println("请输入价格范围查询");
		System.out.println("请输入最低价格");
		int min = sc.nextInt();
		System.out.println("请输入一个最高价格");
		int max = sc.nextInt();
		int index1 = -1;
		boolean priceflag = false;
		for (int i = 0; i < books.length; i++) {
			if (books[i][2] != null) {
				if (Integer.parseInt(books[i][2]) > min && Integer.parseInt(books[i][2]) < max) {
					priceflag = true;
					System.out.println(books[i][2]);
				}
			}
		}
		System.out.println();
		if (priceflag == false) {
			System.out.println("没有查询到价格范围内的书籍");
		}
	}

	private static void bookwriter() { // 查询相关作者
		int index = -1;
		System.out.println("请输入要查询的相关作者");
		String bookwriter = sc.next();
		for (int i = 0; i < books.length; i++) {
			if (books[i][4] != null) {
				if (books[i][4].indexOf(bookwriter) != -1) {
					index = i;
					break;
				}

			}
		}
		System.out.println("ISBN\t书名\t价格\t出版社\t作者");
		System.out.print(books[index][0] + "\t");
		System.out.print(books[index][1] + "\t");
		System.out.print(books[index][2] + "\t");
		System.out.print(books[index][3] + "\t");
		System.out.print(books[index][4] + "\t");
		System.out.println();
		if (index == -1) {
			System.out.println("查询不到相关书籍");
		}

	}

	private static void bookpress() { // 查询出版社
		System.out.println("请输入出版社");
		String press = sc.next();
		int index1 = -1;
		boolean pressflag = false;
		for (int i = 0; i < books.length; i++) {
			if (press.equals(books[i][3])) {
				index1 = i;
				pressflag = true;
				break;
			}
		}
		System.out.println("ISBN\t书名\t价格\t出版社\t作者");
		for (int i = 0; i < 5; i++) {
			if (pressflag == true) {
				System.out.print(books[index1][i] + "\t");

			}
		}
		System.out.println();
		if (pressflag == false) {
			System.out.println("查询不到相关信息");
		}
	}

	private static void bookname() { // 查询书名
		int index = -1;
		System.out.println("请输入书名");
		String bookname = sc.next();
		for (int i = 0; i < books.length; i++) {
			if (books[i][1] != null) {
				if (books[i][1].indexOf(bookname) != -1) {
					index = i;
					break;
				}

			}
		}
		System.out.println("ISBN\t书名\t价格\t出版社\t作者");
		System.out.print(books[index][0] + "\t");
		System.out.print(books[index][1] + "\t");
		System.out.print(books[index][2] + "\t");
		System.out.print(books[index][3] + "\t");
		System.out.print(books[index][4] + "\t");
		System.out.println();
		if (index == -1) {
			System.out.println("查询不到相关书籍");
		}
	}

	private static void bookisbn() {
		int index = -1;
		System.out.println("请输入ISBN号：");
		String ISBN = sc.next();
		for (int i = 0; i < books.length; i++) {
			if (ISBN.equals(books[i][0])) {
				index = i;
				break;
			}
		}
		if (index == -1) {
			System.out.println("查询不到相关书籍");
		} else {
			System.out.println("ISBN\t书名\t价格\t出版社\t作者");
			for (int i = 0; i < 5; i++) {
				System.out.print(books[index][i] + "\t");
			}
			System.out.println();
		}

	}

	private static void update() { // 圖書 更新
		boolean flag3 = false;
		int index = -1;
		System.out.println("请输入ISBN号");
		String booksindex = sc.next();
		for (int i = 0; i < books.length; i++) {
			if (booksindex.equals(books[i][0])) {
				flag3 = true;
				index = i;
				break;
			}
		}
		if (flag3 == true) {
			System.out.println("请输入新的ISBN号");
			books[index][0] = sc.next();
			System.out.println("请输入新的书名");
			books[index][1] = sc.next();
			System.out.println("请输入新的价格");
			books[index][2] = sc.next();
			System.out.println("请输入新的版社");
			books[index][3] = sc.next();
			System.out.println("请输入新的作者");
			books[index][4] = sc.next();
			System.out.println("更新成功!!!");
		} else {
			System.out.println("该ISBN号不存在！！！");
		}
	}

	private static void delete() { // 图书 删除
		System.out.println("请输入要删除的书本名称：");
		String bookname = sc.next();
		boolean flag = false;
		int index = -1;
		for (int i = 0; i < books.length; i++) {
			if (bookname.equals(books[i][1])) {
				flag = true;
				index = i;
			}
		}

		if (flag == true) {
			books[index][0] = null;
			books[index][1] = null;
			books[index][2] = null;
			books[index][3] = null;
			books[index][4] = null;
			System.out.println("删除成功！");
		} else {
			System.out.println("没有此图书！请重新输入！");
		}
	}

	private static void increase() { // 图书 增加
		int index = -1;
		for (int i = 0; i < books.length; i++) {
			if (books[i][0] == null) {
				index = i;
			}
		}
		boolean flag = false;

		System.out.println("请输入图书ISBN");
		String bookindex = sc.next();
		for (int j = 0; j < books.length; j++) {
			if (bookindex.equals(books[j][0])) {
				flag = true;
				break;
			}
		}
		if (flag == true) {
			System.out.println("添加失败！！！该书已经存在！！！");
		} else {
			books[index][0] = bookindex;
			System.out.println("请输入书名");
			books[index][1] = sc.next();
			System.out.println("请输入价格");
			books[index][2] = sc.next();
			System.out.println("请输入出版社");
			books[index][3] = sc.next();
			System.out.println("请输入作者");
			books[index][4] = sc.next();
			System.out.println("添加成功");
		}
	}

	private static void register() { // 注册
		int index = -1;
		for (int i = 0; i < users.length; i++) {
			if (users[i][0] == null) {
				index = i;
			}
		}
		System.out.println("请输入所属部门：");
		users[index][0] = sc.next();
		System.out.println("请输入用户名：");
		users[index][1] = sc.next();
		System.out.println("请输入密码：");
		users[index][2] = sc.next();
		users[index][3] = "普通";

		System.out.println("注册成功");
	}

	private static void init() { // 初始化

		users[0][0] = "管理部";
		users[0][1] = "admin";
		users[0][2] = "123";
		users[0][3] = "管理员";

		users[1][0] = "行政部";
		users[1][1] = "秘书";
		users[1][2] = "123";
		users[1][3] = "助理";

		books[0][0] = "95631";
		books[0][1] = "水浒传";
		books[0][2] = "30";
		books[0][3] = "厦门出版社";
		books[0][4] = "施耐庵";

		books[1][0] = "95632";
		books[1][1] = "西游记";
		books[1][2] = "30";
		books[1][3] = "龙岩出版社";
		books[1][4] = "吴承恩";

		press[2][0] = "北京出版社";
		press[2][1] = "北京市";
		press[2][2] = "张北";

	}

}
