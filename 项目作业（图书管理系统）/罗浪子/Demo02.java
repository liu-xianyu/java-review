import java.util.Scanner;

public class Demo02 {
	//1.采用二维数组存放用户信息（部门、用户名、密码、用户角色）、
	//书籍信息（编码（ISBN）、书籍名称、价格、出版社、作者）、
	//出版社信息（出版社名称、地址、联系人）
	//2.初始化用户信息、出版社信息、书籍信息；（设置基础信息，赋值；）
	//3.用户登录功能：请用户输入账号密码，对比用户信息是否正确：
	//1）不正确提示“该用户不存在或者密码错误！请重新登录！”
	//2）正确，进入系统菜单，“1 图书管理  2 出版社管理  3 退出系统” 

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String [][] users = new String[100][4];
		String [][] books = new String[100][5];
		String [][] press = new String[100][3];
		
		users[0][0] = "行政部";
		users[0][1] = "蔡东生";
		users[0][2] = "wbd";
		users[0][3] = "普通用户";
		
		users[1][0] = "行政部";
		users[1][1] = "杨帆";
		users[1][2] = "123";
		users[1][3] = "工具人";
		
		books[0][0] = "123456";
		books[0][1] = "从你的全世界路过";
		books[0][2] = "50";
		books[0][3] = "北京出版社";
		books[0][4] = "张嘉佳";
		
		press[0][0] = "北京出版社";
		press[0][1] = "北京";
		press[0][2] = "张三";

		while (true) {
			System.out.println("********欢迎来到闽大图书管理系统！********");
			System.out.println("1.登录2.注册");
			int key = scan.nextInt();
			if (key == 1) {
				//登录
				System.out.println("请输入用户名");
				String name = scan.next();
				System.out.println("请输入密码");
				String password = scan.next();
				boolean flag = false;
				for (int i = 0; i < users.length; i++) {
					if (name.equals(users[i][1]) && password.equals(users[i][2])) {
						flag = true;
						break;
					}
				}
				if (flag) {
					System.out.println("登录成功");
					System.out.println(" 欢迎您使用闽大书籍管理系统！！！");
					System.out.println("请输入数字进行选择：1 图书管理  2 出版社管理  3 退出登录  4 退出系统");
				}else {
					System.out.println("登陆失败");
				}
			}
			else if (key==2) {
				//注册
				int index =-1;
				for (int i = 0; i < users.length; i++) {
					if (users[i][0]==null) {
						index=i;
						System.out.println("请输入部门");
						users[index][0] = scan.next();
						System.out.println("请输入用户名");
						users[index][1] = scan.next();
						System.out.println("请输入密码");
						users[index][2] = scan.next();
						System.out.println("请输入用户角色");
						users[index][3] = scan.next();
						System.out.println("恭喜你！注册成功");
						break;
					}
				}
		}
	
		
		}
	}

}
