package yang;

import java.util.Scanner;

public class Wqe {
	static String a[][] = new String[300][4];
	static String b[][] = new String[300][5];
	static String c[][] = new String[300][3];

	static Scanner liang = new Scanner(System.in);

	static int h = -1;
	static boolean falg = false;
	static boolean falg1 = false;

	public static void main(String[] args) {
		/*
		 * 1. 采用二维数组存放用户信息（部门、用户名、密码、用户角色）、 书籍信息（编码（ISBN）、书籍名称、价格、出版社、作者）、
		 * 出版社信息（出版社名称、地址、联系人） 2. 初始化用户信息、出版社信息、书籍信息；（设置基础信息，赋值；） 3.
		 * 用户登录功能：请用户输入账号密码，对比用户信息是否正确： 1） 不正确提示“该用户不存在或者密码错误！请重新登录！” 2） 正确，进入系统菜单，“1
		 * 图书管理 2 出版社管理 3 退出系统”
		 */

		a[0][0] = "游戏部";
		a[0][1] = "aaa";
		a[0][2] = "123456";
		a[0][3] = "赤兔";

		b[0][0] = "ISBN1001";
		b[0][1] = "太平要术";
		b[0][2] = "50";
		b[0][3] = "西凉出版社";
		b[0][4] = "匿";
		
		b[1][0] = "ISBN1002";
		b[1][1] = "仕女图";
		b[1][2] = "100";
		b[1][3] = "黄巾出版社";
		b[1][4] = "匿";

		c[0][0] = "西凉出版社";
		c[0][1] = "黄花村";
		c[0][2] = "马某";

		zong();

	}

	/* 初始界面 */private static void zong() {
		
		while (true) {
			System.out.println("欢迎来到闽大图书管理系统！");
			System.out.println("请选择您的操作:");
			System.out.println("1.登录 \t2.注册");
			int q = liang.nextInt();
			//登录
			if (q == 1) {
				denglu();
				break;

			}//注册
			else if (q == 2) {
				zhuce();
				System.out.println("注册成功，将自动跳转登录页面");
				denglu();
				break;
			} else {
				System.out.println("输入错误请重新输入！！！");
			}
			

		}
		//系统界面,判断登录输入是否正确(判断在dnglu();完成)
		if (falg == true) {
			
			shouye();
		} else {
			System.out.println("输入错误请重新输入！！！");
		}

	}

	/* 注册界面 */private static void zhuce() {
		
		//创建账户
		
		while (true) {
			System.out.println("请创建账号");
			String userName = liang.next();
				
			boolean flag = isExistUserName(userName);
			//判断用户
			if (!flag) {
				System.out.println("该用户名可用，已注册成功");
				int index = getFirstNullUserIndex();
				a[index][1] = userName;
				break;
			}else {
				System.out.println("该用户已被使用，请重新创建");
			}
			
		}
		//创建密码
		for (int i = 0; i < a.length; i++) {
			if (a[i][2] == null) {
				System.out.println("请创建密码");
				a[i][2] = liang.next();
				break;
			}
		}

	}

	/* 注册判断用户名是否重复 */public static boolean isExistUserName(String userName){
		
		boolean flag = false;
		for (int i = 0; i < a.length; i++) {
			if (userName.equals(a[i][1])) {
				flag = true;
				break;
			}
		}
		return flag;
	}

	/* 找出用户表第一个null值 */public static int getFirstNullUserIndex(){
		
		int index = -1;
		for (int i = 0; i < a.length; i++) {
			if (a[i][1]==null) {
				index = i;
				break;
			}
		}
		return index;
	}
	
	/* 登录界面 */private static void denglu() {
		
		while (true) {
			System.out.println("请输入账户：");
			String zhanghu = liang.next();
			System.out.println();
			System.out.println("请输入密码：");
			String mima = liang.next();
			
		//查出用户是否存在
			for (int i = 0; i < a.length; i++) {
				if (zhanghu.equals(a[i][1]) && mima.equals(a[i][2])) {
					falg = true;
					break;
				}
			}
			if (falg == true) {
				break;
			} else {
				System.out.println("输入错误，请重新输入！！");
			}
		}
	}

	/* 登录成功，进入主系统 */private static void shouye() {
		
		System.out.println("***************系统菜单***************\r\n" + "\t1. 图书管理\r\n" + "\t2. 出版社管理\r\n"
				+ "\t3. 退出登录\r\n" + "\t4. 退出系统\r\n");
		System.out.println("请选择操作");
		int num = liang.nextInt();
		switch (num) {
		case 1:
			//图书管理系统
			bookSystem();
			break;
		case 2:
			//出版社管理系统
			pressSystem();
			break;
		case 3:
			//退出登录循环
			zong();
			break;
		case 4:
			//退出系统
			System.out.println("退出成功");
			System.exit(0);
			break;
		default:
			System.out.println("输入错误请重新输入！！！");
			break;
		}

	}
	
	/* 跳回图书管理系统界面 */private static void skip(){
		
		System.out.println("即将跳转回管理系统");
		System.out.println();
		System.out.println();
		bookSystem();
	}
	
	/* 图书管理界面 */private static void bookSystem() {
		
		while(true) {
		System.out.println("****图书管理系统****");
		System.out.println("1.增加\t2.删除\t3.更新\t4.查询\t5.返回上一级");
		System.out.println("请选择操作：");
		int key = liang.nextInt();
		switch (key) {
		case 1:
			int index = isFirstNULL();
			for (int i = 0; i < b.length; i++) {
				isrepetition(index);
				System.out.println("请输入新增加的图书名称");
				b[index][1]=liang.next();
				System.out.println("请输入新增加的图书价格");
				b[index][2]=liang.next();
				System.out.println("请输入新增加的图书所属出版社");
				b[index][3]=liang.next();
				System.out.println("请输入新增加的图书的作者");
				b[index][4]=liang.next();
				break;
			}
			System.out.println();
			System.out.println("添加完毕");
			skip();
			break;
		case 2:
			System.out.println("请输入要删除的图书编码;");
			String coding01=liang.next();
			for (int i = 0; i < b.length; i++) {
				if (coding01.equals(b[i][0])) {
					b[i][0]=null;
					break;
				}
			}
			System.out.println("删除成功！");
			skip();
			break;
		case 3:
			System.out.println("请输入要更改的图书编码;");
			String coding02=liang.next();
			int index01=-1;
			for (int i = 0; i < b.length; i++) {
				if (coding02.equals(b[i][0])) {
					index01=i;
					break;
				}
			}
			for (int i = 0; i < b.length; i++) {
				if (i==1) {
					System.out.println("请输入更改后图书的图书名称");
					String change=liang.next();
					b[index01][i]=change;
				}
				if (i==2) {
					System.out.println("请输入更改后图书的图书价格");
					String change=liang.next();
					b[index01][i]=change;
				}
				if (i==3) {
					System.out.println("请输入更改后图书的图书所属出版社");
					String change=liang.next();
					b[index01][i]=change;
				}
				if (i==4) {
					System.out.println("请输入更改后图书的图书的作者");
					String change=liang.next();
					b[index01][i]=change;
				}
				
			}
			System.out.println("修改完毕");
			skip();
			break;
		case 4:
			inquire();
			break;
		case 5:
			shouye();
			break;
		default:
			break;
		}
	}
}
	
	/* 判断图书编码是否已存在 */private static void isrepetition(int index) {
		
		boolean flag=true;
		while (flag) {
			System.out.println("请输入新增加的图书编码");
			b[index][0]=liang.next();
			for (int i = 0; i < b.length; i++) {
				if (!b[index][0].equals(b[i][0])) {
					flag=false;
					break;
				}else{
					System.out.println("已存在，请重新输入");
					break;
				}
			}
			
		}
		
	}

	/* b 找出图书表第一个null值 */private static int isFirstNULL() {
		
		int index = -1;
		for (int i = 0; i < b.length; i++) {
			if (b[i][0] == null) {
				 index = i;
				break;
			}
		}
		return index;
	}

	/* 图书查询系统 */private static void inquire() {
		
		System.out.println("****图书查询系统****");
		System.out.println("1.根据ISBN查询\t2.根据书名查询\t3.根据出版社查询\t"
				+ "4.根据作者查询\t5.根据价格范围查询\t6.查询所有书籍信息\t7.返回上一级菜单");
		System.out.println("请选择操作：");
		int choice=liang.nextInt();
		
		
		switch (choice) {
		case 1:
			System.out.println("请输入编码进行查询：ISBN");
			String coding=liang.next();
			for (int i = 0; i < isFirstNULL(); i++) {
				if (coding.equals(b[i][0])) {
					int index=i;
					for (int j = 0; j < isFirstNULL(); j++) {
						for (int j2 = 0; j2 < b[j].length; j2++) {
							System.out.println(b[index][j2]);
						}
						
						
					}
					break;
				}
			}
			System.out.println("");
			System.out.println("查询完毕，自动跳回上一级");
			inquire();
			break;
		case 2:
			System.out.println("请输入查询的书名：");
			String title = liang.next();
			for (int i = 0; i < isFirstNULL(); i++) {
				if (title.equals(b[i][1])) {
					System.out.println(b[i][1]);
				}
			}
			System.out.println("");
			System.out.println("查询完毕，自动跳回上一级");
			inquire();
			break;
		case 3:
			System.out.println("请输入查询的出版社：");
			String press = liang.next();
			for (int i = 0; i < isFirstNULL(); i++) {
				if (press.equals(b[i][3])) {
					System.out.println(b[i][3]);
				}
			}
			System.out.println("");
			System.out.println("查询完毕，自动跳回上一级");
			inquire();
			break;
		case 4:
			System.out.println("请输入查询的作者：");
			String author = liang.next();
			for (int i = 0; i < isFirstNULL(); i++) {
				
				if(b[i][5].indexOf(author)!=-1){
					System.out.println(b[i][5]);
				}
			}
			System.out.println("");
			System.out.println("查询完毕，自动跳回上一级");
			inquire();
			break;
		case 5:
			System.out.println("请输入最低价格");
			int min = liang.nextInt();
			System.out.println("请输入最高价格");
			int max = liang.nextInt();
			
			for (int i = 0; i < b.length; i++) {
				if (Integer.valueOf(b[i][2])>min && Integer.valueOf(b[i][2])<max) {
					System.out.println("查询到以下书籍");
					System.out.println(b[i][1]);
					break;//数据未填满，会报错
				}else {
					System.out.println("未查询到相关书籍");
					break;//数据未填满，会报错
				}
			}
			System.out.println("");
			System.out.println("按t返回");
			String index05=liang.next();
			if (index05.equals("t")) {
				inquire();
			}
			break;
		case 6:
			for (int i = 0; i < b.length; i++) {
				if (b[i][0]!=null) {
					System.out.println();
					for (int j = 0; j < b[i].length; j++) {
						if (b[i][j]!=null) {
							System.out.print(b[i][j]+"\t");
						}
					}
				}
				
			}
			System.out.println("");
			System.out.println("按t返回");
			String index06=liang.next();
			if (index06.equals("t")) {
				inquire();
			}
			break;
		case 7:
			bookSystem();
			break;
			

		default:
			break;
			}
		}
	
	/* 出版社系统 */ 	private static void pressSystem() {
		
		System.out.println("****出版社查询系统****");
		System.out.println("1.增加出版社\t2.删除出版社\t3.更新出版社\t"
				+ "4.根据出版社名查询\t5.查询所有出版社\t6.返回上一级菜单");
		System.out.println("请选择操作：");
		int key=liang.nextInt();
		
		int index = CfirstNullIndex();
		
		switch (key) {
		case 1:
			for (int i = 0; i < c.length; i++) {
				if (i==0) {
					System.out.println("请输入添加的出版社名称：");
					c[index][0]=liang.next();
				}
				if (i==1) {
					System.out.println("请输入添加的地址：");
					c[index][1]=liang.next();
				}
				if (i==2) {
					System.out.println("请输入添加的联系人：");
					c[index][2]=liang.next();
				}
				
			}
			System.out.println();
			System.out.println("添加完毕，自动返回上一级");
			pressSystem();
			
			break;
		case 2:
			delect();
			System.out.println();
			System.out.println("删除完毕，自动返回上一级");
			pressSystem();
			break;
		case 3:
			int index01=while01(index);
			
			for (int i1 = 0; i1 < b.length; i1++) {
				if (i1==0) {
					System.out.println("请输入更改后图书的图书名称：");
					String change=liang.next();
					c[index01][i1]=change;
				}
				if (i1==1) {
					System.out.println("请输入更改后图书的图书价格：");
					String change1=liang.next();
					c[index01][i1]=change1;
				}
				if (i1==2) {
					System.out.println("请输入更改后图书的图书所属出版社：");
					String change2=liang.next();
					b[index01][i1]=change2;
				}

			}
			System.out.println();
			System.out.println("修改完毕，自动返回上一级");
			pressSystem();
			break;
		case 4:
			System.out.println("请输入查询的出版社名：");
			String title = liang.next();
			int index09=-1;
			for (int i = 0; i < c.length; i++) {
				if (title.equals(c[i][0])) {
					 index09=i;
					break;
				}
			}
			for (int i = 0; i < c.length; i++) {
				System.out.println(c[index09][i]);
			}
			System.out.println();
			System.out.println("查询完毕，自动返回上一级");
			pressSystem();
			break;
		case 5:
			for (int i = 0; i < c.length; i++) {
				if (c[i][0]!=null) {
					System.out.println(c[i][0]);
				}
				
			}
			System.out.println();
			System.out.println("查询完毕，自动返回上一级");
			pressSystem();
			break;
		case 6:
			shouye();
			break;

		default:
			break;
		}
	}
	
	
	/*出版社名循环输入*/private static int while01(int index) {
		System.out.println("请输入要更新的出版社名：");
		String renew = liang.next();
		int index01=-1;
		boolean flag=ifYesORNo(renew,index);
		for (int i = 0; i < index+1; i++) {
			if (flag) {
				index01=i;
				System.out.println(index01);
				break;
			}else if(c[index][0]==null){
				System.out.println("输入的出版社不存在，请重新输入");
				while01(index);
			}
		}
		return index01;
	}

	/* 判断输入出版社是否可删除 */private static void delect() {
		
		System.out.println("请输入要删除的出版社");
		String delect=liang.next();
		
		for (int i = 0; i < c.length; i++) {
			if ( c[i][0].indexOf(delect)!=1 && !c[i][0].equals(b[i][1])) {
				c[i][0]=null;
				c[i][1]=null;
				c[i][2]=null;
				System.out.println("删除完毕");
				break;
			}else if( c[i][0].indexOf(delect)!=1 && c[i][0].equals(b[i][1])){
				System.out.println("该出版社存有关联的图书无法删除！！！");
				delect();
			}
			else {
				System.out.println("未查到相关信息，请重新输入");
				delect();
			}
		}
	}
	
	/*判断输入出版社是否存在*/private static boolean ifYesORNo(String renew,int index) {
		boolean flag= true;
		for (int i = 0; i < index; i++) {
			if (renew.equals(c[i][0])) {
				flag=true;
			}else {
				flag=false;
			}
		}
		return flag;
		
	}
	
	/* c 找出出版社表第一个null值 */private static int CfirstNullIndex() {
		int index = -1;
		for (int i = 0; i < c.length; i++) {
			if (c[i][0]==null) {
				index=i;
				break;
			}
				
			
		}
		return index;
	}
}

