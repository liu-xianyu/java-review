import java.util.Scanner;

public class d {
	static Scanner sc = new Scanner(System.in);
	static String[][] users = new String[100][4];
	static String[][] books = new String[100][5];
	static String[][] chuban = new String[100][3];

	public static void main(String[] args) {

		ChuShiZhi(users, books, chuban);
		start(users, books);
	}

	public static void start(String[][] users, String[][] books) {// 开始页面
		Scanner sc = new Scanner(System.in);
		System.out.println("欢迎来到闽西职业技术学院书籍管理系统！");
		while (true) {
			System.out.println("1.登录\t2.注册");
			int key = sc.nextInt();

			if (key == 1) {
				DengLu(users, books);
			} else if (key == 2) {
				ZhuCe(users);
			} else {
				System.out.println("您的输入有误,请重新输入！");
			}
		}
	}

	public static void ChuShiZhi(String[][] users, String[][] books, String[][] chuban) {// 初始值
		users[0][0] = "财务部";
		users[0][1] = "admin";
		users[0][2] = "123456";
		users[0][3] = "总裁";

		books[1][0] = "ISBN";
		books[1][1] = "简爱";
		books[1][2] = "35RMB";
		books[1][3] = "外国文学名著丛书编辑委员会";
		books[1][4] = "夏洛蒂·勃朗特";

		chuban[2][0] = "外国文学名著丛书编辑委员会";
		chuban[2][1] = "北京市朝阳区1号";
		chuban[2][2] = "无名氏";
	}

	public static String[][] DengLu(String[][] users, String[][] books) {// 登录
		Scanner sc = new Scanner(System.in);

		while (true) {
			System.out.println("请输入用户名：");
			String username = sc.next();

			System.out.println("请输入密码：");
			String mima = sc.next();

			boolean flag = false;

			for (int i = 0; i < users.length; i++) {
				if (username.equals(users[i][1]) && mima.equals(users[i][2])) {
					flag = true;
					break;
				}
			}
			if (flag == true) {
				System.out.println("登录成功！");
				ZhuYe(username, books, users);
			} else {
				System.out.println("该用户不存在或者密码错误！请重新登录！");
			}
		}
	}

	public static void ZhuCe(String[][] users) {// 注册
		Scanner sc = new Scanner(System.in);
		int index = -1;
		for (int i = 0; i < users.length; i++) {
			if (users[i][0] == null) {
				index = i;
				break;
			}
		}
		System.out.println("请输入您所在的部门：");
		users[index][0] = sc.next();

		System.out.println("请输入您的用户名：");
		users[index][1] = sc.next();

		System.out.println("请输入您的密码：");
		users[index][2] = sc.next();

		System.out.println("注册成功！");
	}

	public static void ZhuYe(String username, String[][] books, String[][] users) {// 主页
		Scanner sc = new Scanner(System.in);
		System.out.println("欢迎您使用闽西职业技术学院书籍管理系统！");
		System.out.println("请选择您要执行的操作：1.图书管理  2.出版社管理  3.退出登录  4.退出系统");

		int xuanze1 = sc.nextInt();

		switch (xuanze1) {
		case 1:// 图书管理
			TuShuGuanLi(username, users, books);
			break;
		case 2:// 出版社管理
			chubanshi(username, books, presses, users);
			break;
		case 3:// 退出登录
			username = "";
			System.out.println("已退出登录");
			start(users, books);
			break;
		case 4:// 退出系统
			System.exit(0);
			break;
		default:
			System.out.println("您的输入错误,请重新输入！");
			break;
		}
	}

	public static void TuShuGuanLi(String username, String[][] books, String[][] users) {// 图书管理
		Scanner sc = new Scanner(System.in);

		System.out.println("1.增加   2.删除  3.更新  4.查询菜单 ");
		int xuanze2 = sc.nextInt();

		switch (xuanze2) {
		case 1:// 增加
			ZengJia(username, books, users);
			break;
		case 2:// 删除
			break;
		case 3:// 更新

			break;
		case 4:// 查询菜单

			break;

		default:
			break;
		}
	}

	public static void ZengJia(String username, String[][] books, String[][] users) {// 图书管理-----增加
		Scanner sc = new Scanner(System.in);

		System.out.println("1.进入增加页面   2.返回上一级");
		int xuanze3 = sc.nextInt();

		switch (xuanze3) {
		case 1:
			while (true) {
				System.out.println("请输入图书的ISBN编码：");
				String index = sc.next();

				for (int i = 0; i < books.length; i++) {
					if (index == books[i][0]) {
						System.out.println("该图书已存在！");
						ZengJia(username, books, users);
					}
				}
				int index1 = -1;
				for (int j = 0; j < books.length; j++) {
					if (books[j][0] == null) {
						index1 = j;
						break;
					}
				}
				books[index1][0] = index;

				System.out.println("请输入所增加图书的书名：");
				books[index1][1] = sc.next();

				System.out.println("请输入该图书的价格：");
				books[index1][2] = sc.next();

				System.out.println("请输入该图书的出版社：");
				books[index1][3] = sc.next();

				System.out.println("请输入该图书的作者：");
				books[index1][4] = sc.next();

				System.out.println("该图书已添加成功！");
			}
		case 2:
			TuShuGuanLi(username, books, users);
			break;
		}
	}

	public static void shanchu(String username, String[][] books, String[][] users) {// 图书管理——删除
		Scanner scan = new Scanner(System.in);
		while (true) {
			System.out.println("1.进入删除页面   2.返回上一级");
			int key = scan.nextInt();
			switch (key) {
			case 1:// 1.进入删除页面
				System.out.println("请输入您想删除图书的ISBN编码：");
				String ISBN = scan.next();
				int index = -1;

				for (int i = 0; i < books.length; i++) {
					if (ISBN.equals(books[i][0])) {
						index = i;
						shanchushuju(index);
					} else if (index == -1) {
						System.out.println("输入有误,未查询到该图书！");
						break;
					}
				}
				break;
			case 2:// 返回上一级
				TuShuGuanLi(username, books, users);
				break;
			}
		}
	}

	private static void shanchushuju(int index) {// 删除数据

		books[index][0] = null;
		books[index][1] = null;
		books[index][2] = null;
		books[index][3] = null;
		books[index][4] = null;
	}

	private static void gengxin(int index1, String username) {// 图书管理——更新

		while (true) {
			System.out.println("1、进入更新\t2、返回上一级");
			int key = sc.nextInt();

			switch (key) {
			case 1:// 进入更新
				System.out.println("请输入ISBN编码：");
				String ISBN = sc.next();
				int index = -1;

				for (int i = 0; i < books.length; i++) {
					if (ISBN.equals(books[i][0])) {
						index = i;
						zhixinggengxin(index);
					} else {
						System.out.println("该书本不存在！");
						break;
					}
				}
				break;
			case 2:// 返回上一级
				TuShuGuanLi(username, books, users);
				break;
			}
		}
	}

	private static void zhixinggengxin(int index) {// 执行更新
		System.out.println("输入新的书名：");
		books[index][1] = sc.next();

		System.out.println("输入新的价格：");
		books[index][2] = sc.next();

		System.out.println("输入新的出版社：");
		books[index][3] = sc.next();

		System.out.println("输入新的作者：");
		books[index][4] = sc.next();

		System.out.println("更新成功！");
		gengxin(index, null);
	}

	private static void chubanshi(String username, String[][] books, String[][] users, String[][] presses) {
		System.out.println("1、增加  2.删除  3.更新  4.根据出版社名称查询  5.查询所有出版社  6.返回上一级");
		int key = sc.nextInt();

		switch (key) {
		case 1:// 增加
			ZengJia(username, books, users);
			break;
		case 2:// 删除
			shanchu(username, books, users);
			break;
		case 3:// 更新
			gengxin(key, username);
			break;
		case 4:// 根据出版社名称查询
			mingcheng(users, books, presses);
			break;
		case 5:// 查询所有出版社
			suoyou(books, presses);
			break;
		case 6:// 返回上一级
			ZhuYe(username, books, users);
			break;
		}
	}

	private static void suoyou(String[][] books, String[][] presses) {
		System.out.println(books[0][3]);
		System.out.println(presses[0][0]);
	}

    private static void mingcheng(String[][] users,String[][] books,String[][] presses) {
     	Scanner scan = new Scanner(System.in);
	    System.out.println("请输入你要查找的名称");
	    String key=scan.next();
	    String str=key;
	  for (int i = 0; i < presses.length; i++) {
		  if (str.equals(presses[i][0])) {
			  System.out.println(presses[i][0]);
		}
	}
}
}