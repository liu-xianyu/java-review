import java.util.Scanner;

public class ShuJiGuanLi {
// 实现简易的图书管理系统；功能要求如下：
// 1. 采用二维数组存放用户信息（部门、用户名、密码、用户角色）、书籍信息（编码（ISBN）、书籍名称、价格、出版社、作者）、出版社信息（出版社名称、地址、联系人）
// 2. 初始化用户信息、出版社信息、书籍信息；（设置基础信息，赋值；）
// 3. 用户登录功能：请用户输入账号密码，对比用户信息是否正确：
// 1） 不正确提示“该用户不存在或者密码错误！请重新登录！”
// 2） 正确，进入系统菜单，“1 图书管理  2 出版社管理  3 退出系统” 
	public static void main(String[] args) {
		String[][] users = new String[50][4];
		String[][] books = new String[50][5];
		String[][] chuban = new String[50][3];

		users[0][0] = "财务部";
		users[0][1] = "admin";
		users[0][2] = "123456";
		users[0][3] = "总裁";

		books[1][0] = "ISBN";
		books[1][1] = "简爱";
		books[1][2] = "35RMB";
		books[1][3] = "外国文学名著丛书编辑委员会";
		books[1][4] = "夏洛蒂·勃朗特";

		chuban[2][0] = "外国文学名著丛书编辑委员会";
		chuban[2][1] = "北京市朝阳区1号";
		chuban[2][2] = "无名氏";
		
		while (true) {
			
			System.out.println("欢迎来到闽西职业技术学院书籍管理系统");
			System.out.print("1.登录\t2.注册");

			Scanner sc = new Scanner(System.in);
			int jieshou = sc.nextInt();

			if (jieshou == 1) {
				System.out.println("请输入您的用户名：");
				String yonghuming = sc.next();
				System.out.println("请输入您的密码：");
				String mima = sc.next();

				boolean flag = false;
				for (int i = 0; i < users.length; i++) {
					if (yonghuming.equals(users[i][1]) && mima.equals(users[i][2])) {
						flag = true;
						break;
					}
				}
				if (flag) {
					System.out.println("登录成功！");
					System.out.println("请输入数字进行选择：1.图书管理  2.出版社管理  3.退出登录  4.退出系统");
					int xuanze = sc.nextInt();
					switch(xuanze) {
					case 1:
						System.out.println("1.增加   2.删除  3.更新  4.查询菜单  5.返回上一级菜单");
						break;
					case 2:
						System.out.println("1.增加   2.删除  3.更新  4.根据出版社名称查询  5.查询所有出版社  6.返回上一级菜单");
						break;
					case 3:
						
						break;
					case 4:
						System.exit(0);
						break;	
					}
					
				} else {
					System.out.println("该用户不存在或者密码错误！请重新登录！");
				}
			}
			else if (jieshou == 2) {
				int index = -1;
				for (int i = 0; i < users.length; i++) {
					if (users[i][0] == null) {
						index = i;
						break;
					}
				}
				System.out.println("请输入您所在的部门：");
				users[index][0] = sc.next();
				System.out.println("请输入您的用户名：");
				users[index][1] = sc.next();
				System.out.println("请输入您的密码：");
				users[index][2] = sc.next();

				System.out.println("注册成功！");
			}
		}
	}
}