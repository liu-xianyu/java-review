
import java.util.Scanner;

//实现简易的图书管理系统；功能要求如下：
//1.采用二维数组存放用户信息（部门、用户名、密码、用户角色）、书籍信息（编码
//（ISBN）、书籍名称、价格、出版社、作者）、出版社信息（出版社名称、地址、联系人）
//2.初始化用户信息、出版社信息、书籍信息；（设置基础信息，赋值；）
//3.用户登录功能：请用户输入账号密码，对比用户信息是否正确：
//1）不正确提示“该用户不存在或者密码错误！请重新登录！”
//2）正确，进入系统菜单，“1 图书管理  2 出版社管理  3 退出系统” 

public class BMS {

	 static Scanner input = new Scanner(System.in);
	 static	String US[][] = new String[100][4];
	 static String BS[][] = new String[100][5];
	 static String PS[][] = new String[100][3];
	private static int i;
	 
	public static void main(String[] args) {
		init();
		indexPage();   //索引页
	}
	
	private static void indexPage() {
		while(true) {
			System.out.println("欢迎来到闽大图书管理系统！！！");
			System.out.println("1.登录     2.注册");
			
			int num = input.nextInt();
			switch(num) {
			case 1:
				Login();
				break;
			case 2:
				Register();
				break;
				default:
					System.out.println("系统退出");
					System.exit(0);
				break;
			}}}
		
	private static void Register() {
		int index = -1;
		for (int i = 0; i < US.length; i++) {
			if (US[i][0] == null) {
				index = i;
				break;
				}}
		System.out.println("请输入所属部门：");
		US[index][0] = input.next();
		System.out.println("请输入用户名：");
		US[index][1] = input.next();
		System.out.println("请输入密码：");
		US[index][2] = input.next();
		System.out.println("注册成功！");
		indexPage();
	}

	private static void Login() {
		System.out.println("请输入用户名：");
		String UN = input.next();
		System.out.println("请输入密码：");
		String PW = input.next();
		
		boolean result = false;
		for (int i = 0; i < US.length; i++) {
			if (UN.equals(US[i][1]) && PW.equals(US[i][2])) {
				result = true;
				break;
			}
		}if(result){
				System.out.println("登陆成功！");
				System.out.println(UN+","+"欢迎您使用闽大书籍管理系统！！！");
				HomePage();	
		}else {
			System.out.println("该用户不存在或者密码错误！请重新登录！");
		}
		Login();
	}

	private static void HomePage() {
		System.out.println("请输入数字进行选择：1 图书管理  2 出版社管理  3 退出登录  4 退出系统");
		
		int num = input.nextInt();
		switch(num) {
		case 1:
			tushuguanli();
			break;
		case 2:
			chubanshe();
			break;
		case 3:
			ExitLogin();
			break;
		case 4:
			System.exit(0);
				}tushuguanli();
	}

	private static void ExitLogin() {
		System.out.println("已退出登录！");
		indexPage();
	}

	private static void chubanshe() {
		System.out.println("请输入：1.增加 2.删除 3.更新 4.根据出版社名称查询 5.查询所有出版社 6.返回上一级菜单");
		int num = input.nextInt();
		
		switch (num) {
		case 1:
			CBSAdd();
			break;
		case 2:
			CBSDelete();
			break;
		case 3:
			CBSUpdata();
			break;
		case 4:
			CBSSelect();
			break;
		case 5:
			CBSAll();
			break;
		case 6:
			default:
			System.out.println(US[i][1]+","+"欢迎您使用闽大书籍管理系统！！！");
			HomePage();	
			break;
		}}

	private static void CBSAll() {

		System.out.println("出版社名称t\t地址\t\t联系人");
		for (int i = 0; i < PS.length-98; i++) {
			for (int j = 0; j < PS[i].length; j++) {
				System.out.println(PS[i][0]+"\t"+PS[i][1]+"\t"+PS[i][2]);
				break;

			}}HomePage();}
	


	private static void CBSSelect() {
		System.out.println("请输入出版社名称：");
		String cbsName = input.next();
		
		for (int i = 0; i < PS.length; i++) {
			if (cbsName.equals(PS[i][0])) {
					System.out.println("出版社名称：" + PS[i][0] );
					System.out.println("出版社地址：" + PS[i][1]);
					System.out.println("出版社联系人：" + PS[i][2]);
					break;	
		}else{
				System.out.println("该出版社不存在！");
		}}HomePage();}
			

	private static void CBSUpdata() {
		 System.out.println("请输入要更新出版社名称：");
			String cbsname = input.next();
			
			for (int i = 0; i < PS.length; i++) {
				if (cbsname.equals(PS[i][0])) {
					for (int j = 0; j < PS[i].length; j++) {
						System.out.println("出版社名称\t\t地址\t\t\t联系人");
						System.out.println(PS[i][0]+"\t"+PS[i][1]+"\t"+PS[i][2]);
					break;}
					for (int j = 0; j < PS.length; j++) {
						System.out.println("请输入要更新的地址：");
						String address = input.next();
						System.out.println("请输入要更新的联系人姓名：");
						String lxrname = input.next();
						System.out.println("更新成功");
		if (cbsname.equals(PS[i][0])){
							System.out.println("出版社名称\tt地址\t联系人");		
							System.out.println(cbsname+"\t"+address+"\t"+lxrname);
				break;
		}}HomePage();
					}}}
	private static void CBSDelete() {
		System.out.println("请输入要删除的出版社名称：");
		String CbsName = input.next();
		
		boolean result = false;
		for (int i = 0; i < BS.length; i++) {
			if (CbsName .equals(PS[i][0]) &&  PS[i][0] == null) {
				result = true;
				System.out.println("删除成功");
				break;
			}
		if(CbsName .equals(PS[i][0]) ) 
				System.out.println("该出版社有相关图书信息存在！不可以删除！如果要删除请先删除所有相关图书信息！！！");
		else 
				System.out.println("删除失败");
		HomePage();
	}
	}
	private static void CBSAdd() {
		
		System.out.println("请出版社名称:");
		String cbsname = input.next();
		
		boolean result = false;
		
	 	for (int i = 0; i < BS.length; i++) {
	 			if (cbsname.equals(PS[i][0])){
	 				result = true;
	 				break;
	 				}}
	 	if (result == false) {
	 		System.out.println("请输入出版社地址：");
			PS[i][1] = input.next();
			System.out.println("请输入出版社联系人：");
			PS[i][2] = input.next();
			System.out.println("出版社添加成功!");
	 	}else {
		 		System.out.println("出版社添加失败！");
	 	}
		HomePage();
	}

	private static void tushuguanli() {
		System.out.println("1.增加 2.删除 3.更新 4.查询 5.返回上一级菜单");
		int num = input.nextInt();
		
		switch(num) {
		case 1:
			BookAdd();
			break;
		case 2:
			BookDelete();
			break;
		case 3:
			BookUpdata();
			break;
		case 4:
			BookSelect();
			break;
		case 5:
			default:
				System.out.println(US[i][1]+","+"欢迎您使用闽大书籍管理系统！！！");
				HomePage();	
				break;
		}}

	private static void BookSelect() {
		System.out.println("请输入查询种类：1.isbn 2.书名（模糊） 3.出版社 4. 作者 5. 价格范围 6.查询所有 7.返回上一级");
		
		int num = input.nextInt();
		switch(num) {
		case 1:
			isbn();
		case 2:
			Bookname();
		case 3:
			pub();
		case 4:
			writer();
		case 5:
			price();
		case 6:
			all();
		case 7:
			default:
				tushuguanli();
				break;
		}	}

	private static void Bookname() {

		System.out.println("请输入书名：");
		String name = input.next();
			
		 System.out.println("ISBN\t\t\t书名\t价格\t\t出版社\t作者");
			for (int i = 0; i < BS.length; i++) {
				if (name.equals(BS[i][1])) {
					for (int j = 0; j < BS[i].length; j++) {
					System.out.println(BS[i][0]+"\t"+BS[i][1]+"\t"+BS[i][2]+"\t"+BS[i][3]+"\t"+BS[i][4]);
					break;		
	}
		}}BookSelect() ;	
				}
				
	private static void all() {
		
		System.out.println("ISBN\t\t书名\t\t价格\t\t出版社\t作者");
		for (int i = 0; i < BS.length-98; i++) {
			for (int j = 0; j < BS[i].length; j++) {
				System.out.println(BS[i][0]+"\t"+BS[i][1]+"\t"+BS[i][2]+"\t"+BS[i][3]+"\t"+BS[i][4]);
				break;
			}
		}
		BookSelect();
		}
	
	private static void price() {
		System.out.println("请输入最低价格：");
		int min = input.nextInt();
		System.out.println("请输入最高价格：");
		int max = input.nextInt();
		
		for (int i = 0; i < BS.length; i++) {
			if (min < Double.parseDouble(BS[i][2]) &&  max > Double.parseDouble(BS[i][2]) ) {
					System.out.println(BS[i][0]+"\t"+BS[i][1]+"\t"+BS[i][2]+"\t"+BS[i][3]+"\t"+BS[i][4]);
					break;}
			}BookSelect() ;
	}

	private static void writer() {
		
		System.out.println("请输入作者姓名： ");
		String name = input.next();

		System.out.println("ISBN\t\t\t书名\t价格\t\t出版社\t作者");
		for (int i = 0; i < BS.length; i++) {
			if (name.equals(BS[i][4])) {
						System.out.println(BS[i][0]+"\t"+BS[i][1]+"\t"+BS[i][2]+"\t"+BS[i][3]+"\t"+BS[i][4]);
					break;
			}
		}
		BookSelect() ;
	}
	
	private static void pub() {
		System.out.println("请输入出版社前的数字进行选择：1.重庆出版社2.北京联合出版社 ");
		int num = input.nextInt();
		switch (num) {
		case 1:
			ChongQing();
			break;
		case 2:
			BeiJing();
			break;
		default:
			System.out.println("请选择正确的数字！！！");
			break;
		}
			BookSelect();
			}
		
	private static void ChongQing() {
		System.out.println("ISBN\t\t\t书名\t价格\t\t出版社\t作者");
		System.out.println(BS[0][0]+"\t"+BS[0][1]+"\t"+BS[0][2]+"\t"+BS[0][3]+"\t"+BS[0][4]);
	}
	private static void BeiJing() {
		System.out.println("ISBN\t\t\t书名\t价格\t\t出版社\t作者");
		System.out.println(BS[1][0]+"\t"+BS[1][1]+"\t"+BS[1][2]+"\t"+BS[1][3]+"\t"+BS[1][4]);
	}

	private static void isbn() {
		System.out.println("请输入ISBN号：");
		String isbn = input.next();
		
		 System.out.println("ISBN\t\t\t书名\t价格\t\t出版社\t作者");
			for (int i = 0; i < BS.length; i++) {
				if (isbn.equals(BS[i][0])) {
					for (int j = 0; j < BS[i].length; j++) {
					System.out.println(BS[i][0]+"\t"+BS[i][1]+"\t"+BS[i][2]+"\t"+BS[i][3]+"\t"+BS[i][4]);
					break;					
	}}	}
			BookSelect() ;
			}

	private static void BookUpdata() {
		System.out.println("请输入ISBN号：");
		String isbn = input.next();
		
		boolean result = false;
		
	 	for (int i = 0; i < BS.length; i++) {
	 			if (isbn.equals(BS[i][0])){
	 				result = true;
	 				break;
	 				}}
	 	if (result == false) {
	 		System.out.println("该ISBN号不存在");
	 	}else {
	 		System.out.println("请输入书名:");
			BS[i][1] = input.next();		
			System.out.println("请输入价格:");
			BS[i][2] = input.next();	
			System.out.println("请输入出版社:");
			BS[i][3]= input.next();	
			System.out.println("请输入作者:");
			BS[i][4] = input.next();	
			System.out.println("更新成功！");		
	 	}
		tushuguanli();
}

	private static void BookDelete() {
		System.out.println("请输入要删除的书本名称：");
		String name = input.next();

		boolean result = false;
		
	 	for (int i = 0; i < BS.length; i++) {
	 			if (name.equals(BS[i][1])){
	 				result = true;
	 				break;
	 				}}
	 	if (result == false) {
	 		System.out.println("没有找到该书，删除失败！");	
				 	}else {
		 	System.out.println("删除成功！");			 	
		 	}
	 	tushuguanli();
	}
		
	private static void BookAdd() {
		 boolean result = false;
			
			System.out.println("请输入图书ISBN:");
			String isbn = input.next();
			
		 	for (int i = 0; i < BS.length; i++) {
		 			if (isbn.equals(BS[i][0])){
		 				result = true;
		 				break;
		 				}}
		 	if (result  == false) {
		 		System.out.println("请输入书名:");
				BS[i][1] = input.next();		
				System.out.println("请输入价格:");
				BS[i][2] = input.next();	
				System.out.println("请输入出版社:");
				BS[i][3] = input.next();	
				System.out.println("请输入作者:");
				BS[i][4] = input.next();	
				System.out.println("添加成功！");	
		 	}else {
		 		System.out.println("添加失败！！！该书已经存在！！！");
		 	}
			tushuguanli();
	}
	
	private static void init() {

		 US[0][0] = "行政部";
		 US[0][1] = "李";
		 US[0][2] = "123";
		 US[0][3] = "管理员";
		 
		 US[1][0] = "开发部";
		 US[1][1] = "刘";
		 US[1][2] = "123";
		 US[1][3] = "普通员工";
	
		 BS[0][0] = "97872290";
		 BS[0][1] = "三体全集 ";
		 BS[0][2] = "168.0";
		 BS[0][3] = "重庆出版社";
		 BS[0][4] = "刘慈欣";
					
		 BS[1][0] = "97875442";
		 BS[1][1] = "房思琪的初恋乐园";
		 BS[1][2] = "49.9";
		 BS[1][3] = "北京联合出版社";
		 BS[1][4] = "林奕含";
		 
		 PS[0][0] = "北京联合出版公司";
		 PS[0][1] = "北京";
		 PS[0][2] = "王五";
		 
		 PS[1][0] = "作家出版社公司";
		 PS[1][1] = "香港九龙荷里活商业中心8楼";
		 PS[1][2] = "李四";
	}
		}
