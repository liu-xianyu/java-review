import java.util.Iterator;
import java.util.Scanner;

public class Demo {
	// 1.	采用二维数组存放用户信息（部门、用户名、密码、用户角色）、
//		书籍信息（编码（ISBN）、书籍名称、价格、出版社、作者）、
//		出版社信息（出版社名称、地址、联系人）
	static Scanner scan = new Scanner(System.in);
	static String[][] users = new String[100][4];
	static String[][] books = new String[100][5];
	static String[][] presses = new String[100][3];
	static String username = "";

	public static void main(String[] args) {
		
//		2.	初始化用户信息、出版社信息、书籍信息；（设置基础信息，赋值；）
		
		begin(users, books, presses);
		
//		3.	用户登录功能：请用户输入账号密码，对比用户信息是否正确：
//		1）	不正确提示“该用户不存在或者密码错误！请重新登录！”
//		2）	正确，进入系统菜单，“1 图书管理  2 出版社管理  3 退出系统” 
		
		start(users, books);
		
	}
	
	//开始页面
	public static void start(String[][] users,String[][] books) {
		System.out.println("欢迎来到闽大图书管理系统！");
		while (true) {
			System.out.println("1.登录	2.注册");
			int key = scan.nextInt();
			
			if (key == 1) {//登录
				
				login(users, books);
				
			}else if (key == 2) {//注册
				
				register(users);
				
			}else {
				System.out.println("输入错误！请重新输入！");
			}
		}
	}
	
	//初始化
	public static void begin(String[][] users,String[][] books,String[][] presses) {
		users[0][0] = "社畜部";
		users[0][1] = "admin";
		users[0][2] = "123456";
		users[0][3] = "管理员";
		
		users[1][0] = "财务部";
		users[1][1] = "张三";
		users[1][2] = "123456";
		users[1][3] = "管理员";
		
		books[0][0] = "10616501";
		books[0][1] = "三体全集";
		books[0][2] = "139";
		books[0][3] = "重庆出版社";
		books[0][4] = "刘慈欣";
		
		presses[0][0] = "重庆出版社";
		presses[0][1] = "重庆市";
		presses[0][2] = "陈兴芜";
	}
	
	//登录
	public static String[][] login(String[][] users,String[][] books) {
		
		while (true) {
			System.out.println("请输入用户名：");
			String username = scan.next();
			
			System.out.println("请输入密码：");
			String password = scan.next();
			
			boolean flag = false;
			
			for (int i = 0; i < users.length; i++) {
				if (username.equals(users[i][1]) && password.equals(users[i][2])) {
					flag = true;
					break;
				}
			}
			
			if (flag == true) {
				System.out.println("登录成功！");
				homepage(username, books, users);
			}else {
				System.out.println("该用户不存在或者密码错误！请重新登录！");
			}
		}
	}
	
	//注册
	public static void register(String[][] users) {
		int index = -1;
		
		for (int i = 0; i < users.length; i++) {
			if (users[i][0] == null) {
				index = i;
				break;
			}
		}
		
		System.out.println("请输入所属部门：");
		users[index][0] = scan.next();
		
		System.out.println("请输入用户名：");
		users[index][1] = scan.next();
		
		System.out.println("请输入密码：");
		users[index][2] = scan.next();
		
		users[index][3] = "普通用户";
		System.out.println("您的账号为普通用户。");
		
		System.out.println("注册成功！");
		
	}
	
	//主页
	public static void homepage(String username,String[][] books,String[][] users) {
		System.out.println(username + "， 欢迎您使用闽大书籍管理系统！！！");
		System.out.println("请输入数字进行选择：1 图书管理  2 出版社管理  3 退出登录  4 退出系统");
		
		int key = scan.nextInt();
		
		switch (key) {
		case 1://图书管理
			librarian(username, books, users);
			break;
		case 2://出版社管理
			Press();
			break;
		case 3://退出登录
			username = "";
			System.out.println("已退出登录");
			start(users, books);
			break;
		case 4://退出系统
			System.exit(0);
			break;

		default:
			System.out.println("输入错误，请重新输入！");
			break;
		}
	}
	
	//出版社管理
	/*
	 * 1)	增加
	2)	删除：出版社有关联的图书不能删除；
	3)	更新 
	4)	根据出版社名称查询
	5)	查询所有出版社
	6)	返回上一级菜单
	 */
	private static void Press() {
		System.out.println("请输入：1.增加 2.删除 3.更新 4.根据出版社名称查询 5.查询所有出版社 6.返回上一级菜单");
		int key = scan.nextInt();
		
		switch (key) {
		case 1://增加
			PressAdd();
			break;
		case 2://删除
			PressDel();
			break;
		case 3://更新
			PressUpDate();
			break;
		case 4://根据出版社名称查询
			PressQuery();
			break;
		case 5://查询所有出版社
			PressQueryAll();
			break;
		case 6://返回上一级
			homepage(username, books, users);
			break;

		}
	}
	
	//出版社管理——查询所以出版社
	private static void PressQueryAll() {
		
		System.out.println("出版社名称\t出版社地址\t出版社联系人");
		for (int i = 0; i < presses.length; i++) {
			if (presses[i][0] != null) {
				System.out.println(presses[i][0]
						+"\t"+presses[i][1]
								+"\t"+presses[i][2]);
			}
		}
		
		Press();
		
	}

	//出版社管理——出版社名称查询
	private static void PressQuery() {
		System.out.println("1、进入出版社查询\t2、返回上一级");
		int key = scan.nextInt();
		
		switch (key) {
		case 1:
			System.out.println("请输入出版社名称：");
			String PressName = scan.next();
			
			for (int i = 0; i < presses.length; i++) {
				if (presses[i][0].indexOf(PressName) != -1) {
					System.out.println("出版社名称\t出版社地址\t出版社联系人");
					System.out.println(presses[i][0]
											+"\t"+presses[i][1]
													+"\t"+presses[i][2]);
					
					PressQuery();
				}else {
					System.out.println("该出版社不存在！");
					PressQuery();
				}
			}
			break;

		case 2:
			Press();
			break;
		}
	}

	//出版社管理——更新
	private static void PressUpDate() {
		System.out.println("1、进入更新\t2、返回上一级");
		int key = scan.nextInt();
		
		switch (key) {
		case 1:
			System.out.println("请输入新的出版社的名称：");
			String PressName = scan.next();
			
			for (int i = 0; i < presses.length; i++) {
				if (presses[i][0].equals(PressName)) {
					presses[i][0] = PressName;
					
					System.out.println("请输入新的出版社地址：");
					presses[i][1] = scan.next();
					
					System.out.println("请输入新的出版社联系人：");
					presses[i][2] = scan.next();
					
					PressUpDate();
				}else {
					System.out.println("该出版社不存在！");
				}
			}
			break;

		case 2:
			Press();
			break;
		}
		
	}

	//出版社管理——删除
	private static void PressDel() {
		System.out.println("1、进入删除出版社\t2、返回上一级");
		int key = scan.nextInt();
		
		switch (key) {
		case 1:
			System.out.println("请输入要删除的出版社的名称：");
			String PressName = scan.next();
			
			for (int i = 0; i < presses.length; i++) {
				if (presses[i][0].equals(PressName)) {
					for (int j = 0; j < books.length; j++) {
						if (books[j][3].equals(PressName)) {
							System.out.println("该出版社有相关图书信息存在！不可以删除！如果要删除请先删除所有相关图书信息！！！");
						}else {
							presses[i][0] = null;
							presses[i][1] = null;
							presses[i][2] = null;
							
							PressDel();
						}
					}
				}
			}
			break;

		case 2:
			Press();
			break;
		}
	}

	//出版社管理——增加
	private static void PressAdd() {
		System.out.println("1、进入增加出版社\t2、返回上一级");
		int key = scan.nextInt();
		
		switch (key) {
		case 1:
			System.out.println("请输入增加的出版社名称：");
			String pressname = scan.next();
			
			for (int i = 0; i < presses.length; i++) {
				if (presses[0][0].equals(pressname)) {
					System.out.println("该出版社已存在！");
					PressAdd();
				}else {
					int index = -1;
					
					for (int j = 0; j < presses.length; j++) {
						if (presses[i][0] == null) {
							index = i;
							break;
						}
					}
					presses[index][0] = pressname;
					
					System.out.println("请输入出版社的地址：");
					presses[index][1] = scan.next();
					
					System.out.println("请输入出版社的联系人：");
					presses[index][2] = scan.next();
					
					PressAdd();
				}
			}
			break;

		case 2:
			Press();
			break;
		}
	}

	//图书管理
//	1)	增加：图书编码唯一，如果新增的图书信息编码已存在，提示“图书已存在”；
//		不存在才进行新增书籍信息；
//	2)	删除：根据ISBN编码删除图书；
//	3)	更新：ISBN唯一不能更改，根据ISBN更新相应的书籍信息；
//	4)	查询菜单：
//	a)	根据ISBN查询
//	b)	根据书名查询（模糊）
//	c)	根据出版社查询
//	d)	根据作者查询
//	e)	根据价格范围查询
//	f)	查询所有书籍信息
//	g)	返回上一级菜单
	public static void librarian(String username,String[][] books,String[][] users){
		
		System.out.println("1、增加\t2、删除\t3、更新\t4、查询菜单\t5、返回上一级");
		int key = scan.nextInt();
		
		switch (key) {
		case 1://增加
			add(username, books, users);
			break;
		case 2://删除
			delete(username, books, users);
			break;
		case 3://更新
			deteup();
			break;
		case 4://查询菜单
			inquirebooks();
			break;
		case 5://返回上一级
			homepage(username, books, users);
			break;
		}
	}
	
	//图书管理——查询
	/*
	 *  a)	根据ISBN查询
		b)	根据书名查询（模糊）
		c)	根据出版社查询
		d)	根据作者查询
		e)	根据价格范围查询
		f)	查询所有书籍信息
		g)	返回上一级菜单
	 */
	private static void inquirebooks() {
		System.out.println("请输入查询种类：1.isbn 2.书名（模糊） 3.出版社 4. 作者 5. 价格范围 6.查询所有 7.返回上一级");
		int key = scan.nextInt();
		
		switch (key) {
		case 1://isbn
			inquireBookISBN();
			break;
		case 2://书名（模糊）
			inquireBookName();
			break;
		case 3://出版社
			inquireBookPress();
			break;
		case 4://作者
			inquireBookMaker();
			break;
		case 5://价格范围
			inquireBookMoney();
			break;
		case 6://查询所有
			inquireBookAll();
			break;
		case 7://返回上一级
			librarian(username, books, users);
			break;

		}
		
		
	}
	
	//图书管理——查询——查询所有
	private static void inquireBookAll() {
		
		System.out.println("ISBN\t书名\t价格\t出版社\t作者");
		for (int i = 0; i < books.length; i++) {
			System.out.println(books[i][0]
					+"\t"+books[i][1]
							+"\t"+books[i][2]
									+"\t"+books[i][3]
											+"\t"+books[i][4]);
			
		}
		librarian(username, books, users);
		
	}

	//图书管理——查询——价格范围
	private static void inquireBookMoney() {
		System.out.println("1、进入价格范围查询\t2、返回上一级");
		int key = scan.nextInt();
		int index = -1;
		
		switch (key) {
		case 1://进入查询
			System.out.println("请输入最低价格：");
			int min = scan.nextInt();
			
			System.out.println("请输入最高价格：");
			int max = scan.nextInt();
			
			for (int i = 0; i < books.length; i++) {
				if (Integer.parseInt(books[i][2]) >= min && Integer.parseInt(books[i][2]) <= max) {
					index = i;
					
					System.out.println("ISBN\t书名\t价格\t出版社\t作者");
					System.out.println(books[index][0]
							+"\t"+books[index][1]
									+"\t"+books[index][2]
											+"\t"+books[index][3]
													+"\t"+books[index][4]);
				}
			}
			break;

		case 2:
			librarian(username, books, users);
			break;
		}
		
	}

	//图书管理——查询——作者
	private static void inquireBookMaker() {
		System.out.println("1、进入作者查询\t2、返回上一级");
		int key = scan.nextInt();
		int index = -1;
		
		switch (key) {
		case 1://进入查询
			System.out.println("请输入作者名字：");
			String bookmaker = scan.next();
			for (int i = 0; i < books.length; i++) {
				if (bookmaker == books[i][4]) {
					index = i;
					
					System.out.println("ISBN\t书名\t价格\t出版社\t作者");
					System.out.println(books[index][0]
							+"\t"+books[index][1]
									+"\t"+books[index][2]
											+"\t"+books[index][3]
													+"\t"+books[index][4]);
					break;
				}else {
					librarian(bookmaker, books, users);
				}
			}
			
			librarian(bookmaker, books, users);
			break;

		case 2:
			librarian(username, books, users);
			break;
		}
	}

	//图书管理——查询——出版社
	private static void inquireBookPress() {
		System.out.println("1、进入出版社查询\t2、返回上一级");
		int key = scan.nextInt();
		int index = -1;
		
		switch (key) {
		case 1://进入查询
			System.out.println("请输入出版社名字：");
			String press = scan.next();
			
			for (int i = 0; i < books.length; i++) {
				if (press == books[i][3]) {
					index = i;
					
					System.out.println("ISBN\t书名\t价格\t出版社\t作者");
					System.out.println(books[index][0]
							+"\t"+books[index][1]
									+"\t"+books[index][2]
											+"\t"+books[index][3]
													+"\t"+books[index][4]);
					break;
				}else {
					librarian(press, books, users);
				}
			}
			
			librarian(press, books, users);
			break;

		case 2:
			librarian(username, books, users);
			break;
		}
		
	}

	//图书管理——查询——书名（模糊）
	private static void inquireBookName() {
		System.out.println("1、进入书名查询\t2、返回上一级");
		int key = scan.nextInt();
		int index = -1;
		
		switch (key) {
		case 1://进入查询
			System.out.println("请输入书名：");
			String bookname = scan.next();
			
			for (int i = 0; i < books.length; i++) {
				if (books[i][1] != null && books[i][1].indexOf(bookname) != -1) {
					index = i;
					
					System.out.println("ISBN\t书名\t价格\t出版社\t作者");
					System.out.println(books[index][0]
							+"\t"+books[index][1]
									+"\t"+books[index][2]
											+"\t"+books[index][3]
													+"\t"+books[index][4]);
					break;
				}else {
					librarian(bookname, books, users);
				}
			}
			
			librarian(bookname, books, users);
			break;

		case 2://返回上一级
			librarian(username, books, users);
			break;
		}
	}

	//图书管理——查询——ISBN：按ISBN码进行查询
	private static void inquireBookISBN() {
		System.out.println("1、进入ISBN码查询\t2、返回上一级");
		int key = scan.nextInt();
		int index = -1;
		
		switch (key) {
		case 1:
			System.out.println("请输入ISBN码：");
			String ISBN = scan.next();
			
			for (int i = 0; i < books.length; i++) {
				if (ISBN == books[i][0]) {
					index = i;
					
					System.out.println("ISBN\t书名\t价格\t出版社\t作者");
					System.out.println(books[index][0]
							+"\t"+books[index][1]
									+"\t"+books[index][2]
											+"\t"+books[index][3]
													+"\t"+books[index][4]);
					break;
				}else {
					System.out.println("输入错误！");
					librarian(ISBN, books, users);
				}
			}
			
			librarian(ISBN, books, users);
			break;

		case 2://返回上一级
			inquirebooks();
			break;
		}
	}

	//图书管理——增加
	//图书编码唯一，如果新增的图书信息编码已存在，提示“图书已存在”；
//	不存在才进行新增书籍信息；
	public static void add(String username,String[][] books,String[][] users) {
		
		System.out.println("1、进入增加\t2、返回上一级");
		int key = scan.nextInt();
		
		switch (key) {
		case 1:
			while (true) {
				System.out.println("请输入增加书目的图书编码：");
				String index = scan.next();
				
				for (int i = 0; i < books.length; i++) {
					if (index.equals(books[i][0])) {
						System.out.println("图书已存在！");
						add(username, books, users);
					}
				}
				
				int index1 = -1;
				for (int j = 0; j < books.length; j++) {
					if (books[j][0] == null) {
						index1 = j;
						break;
					}
				}
				books[index1][0] = index;
						
				System.out.println("请输入书名：");
				books[index1][1] = scan.next();
						
				System.out.println("请输入价格：");
				books[index1][2] = scan.next();
						
				System.out.println("请输入出版社：");
				books[index1][3] = scan.next();
						
				System.out.println("请输入作者名：");
				books[index1][4] = scan.next();
						
				System.out.println("添加书本成功！");
				
				}
		case 2:
			librarian(username, books, users);
			break;
		}
	
	}
	
	//图书管理——删除
	//根据ISBN编码删除图书；
	public static void delete(String username,String[][] books,String[][] users) {
		while(true) {
			System.out.println("1、进入删除\t2、返回上一级");
			int key = scan.nextInt();
			switch (key) {
			case 1://删除
					System.out.println("请输入您想删除书的ISBN编码：");
					String ISBN = scan.next();
					int index = -1;
					
					for (int i = 0; i < books.length; i++) {
						if (ISBN.equals(books[i][0])) {//进入删除
							index = i;
							delbook(index);
						}else if(index == -1){
							System.out.println("输入错误，没有这本书！");
							break;
						}
					}
				break;
			case 2://返回上一级
				librarian(username, books, users);
				break;
			}
		}
	}
	//执行删除数据
	private static void delbook(int index) {
		books[index][0] = null;
		books[index][1] = null;
		books[index][2] = null;
		books[index][3] = null;
		books[index][4] = null;
	}
	
	//图书管理——更新
	//更新：ISBN唯一不能更改，根据ISBN更新相应的书籍信息；
	private static void deteup() {
		
		while(true){
			System.out.println("1、进入更新\t2、返回上一级");
		int key = scan.nextInt();
		
		switch (key) {
		case 1://进入更新
		
		System.out.println("请输入ISBN编码：");
		String ISBN = scan.next();
		int index = -1;
		
		for (int i = 0; i < books.length; i++) {
			if (ISBN.equals(books[i][0])) {
				index = i;
				bookdateup(index);
			}else {
				System.out.println("该书本不存在！");
				break;
				}
			}
			break;
		case 2://返回上一级
			librarian(username, books, users);
			break;
		}
		
		
		}
	}
	
//执行更新
	private static void bookdateup(int index) {
		System.out.println("输入新的书名：");
		books[index][1] = scan.next();
		
		System.out.println("输入新的价格：");
		books[index][2] = scan.next();
		
		System.out.println("输入新的出版社：");
		books[index][3] = scan.next();
		
		System.out.println("输入新的作者：");
		books[index][4] = scan.next();
		
		System.out.println("更新成功");
		deteup();
	}

}
