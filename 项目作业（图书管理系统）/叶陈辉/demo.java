import java.util.Scanner;

public class demo {

	public static void main(String[] args) {
		// 图书管理
		Scanner scan = new Scanner(System.in);
		String[][] users = new String[100][4];
		String[][] books = new String[100][5];
		String[][] presses = new String[100][3];
		begin(users, books, presses);
		start(users, books);
	}
	public static void start(String[][] users,String[][] books) {
		Scanner scan = new Scanner(System.in);
		System.out.println("欢迎来到图书管理系统！");
		while (true) {
			System.out.println("1.登录 2.注册");
			int key = scan.nextInt();
			
			if (key == 1) {
				
				login(users, books);
				
			}else if (key == 2) {
				
				register(users);
				
			}else {
				System.out.println("输入错误！请重新输入！");
			}
		}
	}
	public static void begin(String[][] users,String[][] books,String[][] presses) {
		users[0][0] = "学习部";
		users[0][1] = "张三";
		users[0][2] = "123";
		users[0][3] = "管理员";
		
		users[1][0] = "李四";
		users[1][2] = "123";
		users[1][3] = "管理员";
		
		books[0][0] = "10616501";
		books[0][1] = "华南";
		books[0][2] = "10";
		books[0][3] = "重庆出版社";
		books[0][4] = "刘思";
		
		presses[0][0] = "华北";
		presses[0][1] = "北京市";
		presses[0][2] = "刘华";
	}
	public static String[][] login(String[][] users,String[][] books) {
		Scanner scan = new Scanner(System.in);
		while (true) {
			System.out.println("请输入用户名：");
			String username = scan.next();
			
			System.out.println("请输入密码：");
			String password = scan.next();
			boolean flag = false;
			for (int i = 0; i < users.length; i++) {
				if (username.equals(users[i][1]) && password.equals(users[i][2])) {
					flag = true;
					break;
				}
			}
			if (flag == true) {
				System.out.println("登录成功！");
				homepage(username, books, users);
			}else {
				System.out.println("该用户不存在或者密码错误！请重新登录！");
			}
		}
	}

	public static void register(String[][] users) {
		Scanner scan = new Scanner(System.in);
		int index = -1;
		for (int i = 0; i < users.length; i++) {
			if (users[i][0] == null) {
				index = i;
				break;
			}
		}
		System.out.println("请输入所属部门：");
		users[index][0] = scan.next();
		
		System.out.println("请输入用户名：");
		users[index][1] = scan.next();
		
		System.out.println("请输入密码：");
		users[index][2] = scan.next();
		
		users[index][3] = "普通用户";
		System.out.println("您的账号为普通用户。");
		
		System.out.println("注册成功！");
	}
	public static void homepage(String username,String[][] books,String[][] users) {
		Scanner scan = new Scanner(System.in);
		System.out.println(username + "， 欢迎您使用书籍管理系统！！！");
		System.out.println("请输入数字进行选择：1 图书管理  2 出版社管理  3 退出登录  4 退出系统");
		int key = scan.nextInt();
		switch (key) {
		case 1:
			librarian(books);
			break;
		case 2:
			
			break;
		case 3:
			start(users, books);
			break;
		case 4:
			System.exit(0);
			break;

		default:
			System.out.println("输入错误，请重新输入！");
			break;
		}
	}
	public static void librarian(String[][] books){
		Scanner scan = new Scanner(System.in);
		
		System.out.println("1、增加\t2、删除\t3、更新\t4、查询菜单");
		int key = scan.nextInt();
		
		switch (key) {
		case 1:
			add(books);
			break;
		case 2:
				
			break;
		case 3:
			
			break;
		case 4:
			
			break;

		default:
			break;
		}
	}
	public static void add(String[][] books) {
		Scanner scan = new Scanner(System.in);
		int index1 = -1;
		while (true) {
		System.out.println("请输入增加书目的图书编码：");
		String index = scan.next();
		for (int i = 0; i < books.length; i++) {
			if (index == books[i][0]) {
				System.out.println("图书已存在！");
				break;
			}else {
				for (int j = 0; j < books.length; j++) {
					if (books[j][0] == null) {
						index1 = j;
						break;
					}
				}
				books[index1][0] = index;
				
				System.out.println("请输入书名：");
				books[index1][0] = scan.next();
			}
		}
		}
		
	}

}
