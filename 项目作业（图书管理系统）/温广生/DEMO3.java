package less;

import java.util.Scanner;

public class DEMO3 {
	static Scanner scan = new Scanner(System.in);
	public static void main(String[] args) {
		String[][] a = new String[100][4];
		String[][] b = new String[100][5];
		String[][] c = new String[100][3];
		begin(a, b, c);
		start(a, b);
	}
	private static void begin(String[][] a, String[][] b, String[][] c) {
		// TODO Auto-generated method stub
		a[0][0] = "人才部";
		a[0][1] = "b1";
		a[0][2] = "b123456";
		a[0][3] = "普通员工";

		a[1][0] = "市场部";
		a[1][1] = "b2";
		a[1][2] = "b123456";
		a[1][3] = "普通员工";

		a[2][0] = "调研部";
		a[2][1] = "b3";
		a[2][2] = "b123456";
		a[2][3] = "普通员工";

		a[3][0] = "软件部";
		a[3][1] = "b4";
		a[3][2] = "b123456";
		a[3][3] = "管理员";
//	、书籍信息（编码（ISBN）、书籍名称、价格、出版社、作者）、
		b[0][0] = "11003";
		b[0][1] = "活着";
		b[0][2] = "40";
		b[0][3] = "清华大学";
		b[0][4] = "余华";

		b[1][0] = "11004";
		b[1][1] = "许三观卖血记";
		b[1][2] = "45";
		b[1][3] = "清华大学";
		b[1][4] = "余华";

		b[2][0] = "11005";
		b[2][1] = "三毛日记";
		b[2][2] = "20";
		b[2][3] = "北京大学";
		b[2][4] = "三毛";

		b[3][0] = "11006";
		b[3][1] = "唐诗三百首";
		b[3][2] = "40";
		b[3][3] = "清华大学";
		b[3][4] = "李白";
//		出版社信息（出版社名称、地址、联系人）
		c[0][0] = "北京大学";
		c[0][1] = "北京学院路";
		c[0][2] = "00112233441";

		c[0][0] = "古田";
		c[0][1] = "古田学院路";
		c[0][2] = "00112233442";

		c[0][0] = "清华大学";
		c[0][1] = "清华学院路";
		c[0][2] = "00112233443";
	}

	public static void start(String[][] a, String[][] b) {
		System.out.println("欢迎来到闽大图书管理系统！");
		while (true) {
			System.out.println("1.登录	2.注册");
			int key = scan.nextInt();
			if (key == 1) {// 登录
				denglu(a,b);
			} else if (key == 2) {// 注册
				register(a);
			} else {
				System.out.println("输入错误！请重新输入！");
			}
		}
	}

	

	private static void denglu(String[][] a, String[][] b) {
		// TODO Auto-generated method stub
		while (true) {
			System.out.println("请输入用户名");
			String sun1 = scan.next();
			System.out.println("请输入密码：");
			String sun2 = scan.next();
			boolean falg = false;
			for (int i = 0; i < a.length; i++) {
				if (sun1.equals(a[i][1]) && sun2.equals(a[i][2])) {
					falg = true;
					break;
				}
			}

			if (falg ==true) {
				System.out.println("登陆成功!");
				homepage(sun1, a, b);

			} else {
				System.out.println("登陆失败");
			}
		}
	}

	private static void register(String[][] a) {
		// TODO Auto-generated method stub
		int index = -1;
		for (int i = 0; i < a.length; i++) {
			if (a[i][0] == null) {
				index = i;
				break;
			}
		}
		System.out.println("请输入部门");
		a[index][0] = scan.next();
		System.out.println("请输入用户名");
		a[index][1] = scan.next();
		System.out.println("请输入密码");
		a[index][2] = scan.next();
		System.out.println("请输入用户角色");
		a[index][3] = scan.next();

		System.out.println("注册成功！！！！");
	}
	public static void homepage(String sun1,String[][] a,String[][] b) {
		Scanner scan = new Scanner(System.in);
		System.out.println(sun1 + "， 欢迎您使用闽大书籍管理系统！！！");
		System.out.println("请输入数字进行选择：1 图书管理  2 出版社管理  3 退出登录  4 退出系统");
		int key = scan.nextInt();
		switch (key) {
		case 1://图书管理
			break;
		case 2://出版社管理
			break;
		case 3://退出登录
			start(a, b);
			break;
		case 4://退出系统
			System.exit(0);
			break;
		default:
			System.out.println("输入错误，请重新输入！");
			break;
		}
	}
}