import java.util.Scanner;

public class Demo01 {
	static Scanner sc = new Scanner(System.in);
	static String[][] users = new String[100][4];
	static String[][] books = new String[100][5];
	static String[][] press = new String[100][3];
	static String loginUser = "";
	private static void init() {

		users[0][0] = "行政部";
		users[0][1] = "123";
		users[0][2] = "wbd";
		users[0][3] = "普通用户";
		
		users[1][0] = "行政部";
		users[1][1] = "456";
		users[1][2] = "123";
		users[1][3] = "工具人";
		
		books[0][0] = "123456";
		books[0][1] = "从你的全世界路过";
		books[0][2] = "50";
		books[0][3] = "北京出版社";
		books[0][4] = "张嘉佳";
		
		press[0][0] = "北京出版社";
		press[0][1] = "北京";
		press[0][2] = "张三";
	}
	
	public static void main(String[] args) {

		init();

		while (true) {
			initialinterface();

		}

	}

	private static void initialinterface() {
		System.out.println("欢迎登陆图书管理系统");
		System.out.println("1.登录 2.注册");

		int shuru = sc.nextInt();
		if (shuru == 1) {
			loginModule();
		} else if (shuru == 2) {
			register();
		} else {
			System.out.println("输入错误 请重新输入");
		}

	}

	private static void register() {
		//注册
		int index = getFirstNullIndex(users);
		
		System.out.println("请输入所属部门：");
		users[index][0] = sc.next();
		System.out.println("请输入用户名：");
		users[index][1] = sc.next();
		System.out.println("请输入密码：：");
		users[index][2] = sc.next();
		System.out.println("请输入用户角色：");
		users[index][3] = sc.next();
		System.out.println("注册成功");

	}

	private static void loginModule() { 
		// 登录
		System.out.println("请输入用户名：");
		String userName = sc.next();
		System.out.println("请输入密码：");
		String password = sc.next();

		boolean flag = false;
		for (int i = 0; i < users.length; i++) {
			if (userName.equals(users[i][1]) && password.equals(users[i][2])) {
				flag = true;
			}
		}
		if (flag == true) {
			System.out.println("登陆成功！欢迎使用");
			loginUser = userName;
			homePage();
		} else {
			System.out.println("该用户不存在或者密码错误！请重新登录！");
		}

	}

	private static void homePage() {
		//主页
		System.out.println(loginUser + "欢迎您使用书籍管理系统！！！");
		System.out.println("请输入数字进行选择：1 图书管理  2 出版社管理  3 退出登录  4 退出系统");
		int number = sc.nextInt();
		switch (number) {
		case 1:
			BookManagement();

		case 2:// 出版社管理
			pressmanagement();

			break;
		case 3:// 退出登陆
			initialinterface();
			break;
		case 4:// 退出系统
			System.out.println("退出系统成功！");
			System.exit(0);
			break;

		default:
			break;
		}

	}

	private static void pressmanagement() {
		System.out.println("请输入：1.增加 2.删除 3.更新 4.根据出版社名称查询 5.查询所有出版社 6.返回上一级菜单");
		int key = sc.nextInt();
		switch (key) {
		case 1:
			// 增加
			
			int index = -1;
			index = getFirstNullIndex(press);
			
			System.out.println("请输入出版社名称：");
			press[index][0] = sc.next();
			System.out.println("请输入出版社地址：:");
			press[index][1] = sc.next();
			System.out.println("请输入出版社联系人：");
			press[index][2] = sc.next();
			System.out.println("添加成功！");
			pressmanagement();
			break;
		case 2:
			// 删除
			System.out.println("请输入要删除的出版社名称：");
			String Temporaryname = sc.next();
			int Temporaryindex = -1;
			for (int i = 0; i < press.length; i++) {
				if (Temporaryname.equals(press[i][1])) {
					if (press[i][1].equals(books[i][3])) {					
						System.out.println("不能删除！");
					}else {
						Temporaryindex = i;
						System.err.println("删除成功！");
					}
				}

			}

			if (Temporaryindex != -1) {
				press[Temporaryindex][0] = null;
				press[Temporaryindex][1] = null;
				press[Temporaryindex][2] = null;
				System.out.println("删除成功！");
			} else {
				System.out.println("删除失败！");
			}
				pressmanagement();
			break;
		case 3:
			// 更新
			System.out.println("请输入出版社名称");
			String pressupdate = sc.next();
			int pressindex = -1;
			for (int i = 0; i < press.length; i++) {
				if (pressupdate.equals(press[i][0])) {
					pressindex = i;
					break;
				}
			}
			if (pressindex != -1) {
				
				System.out.println("请输入更新出版社地址：");
				press[pressindex][1] = sc.next();
				System.out.println("请输入更新出版社联系人：");
				press[pressindex][2] = sc.next();
				System.out.println("更新成功！！！");
				break;
			} else {
				System.out.println("该出版社名称不存在！！！");
			}
			pressmanagement();
			break;
		case 4:
			// 根据出版社名称查询
			System.out.println("请输入出版社名称");
			String pressname = sc.next();
			for (int i = 0; i < press.length; i++) {
				if (press[i][0] != null && press[i][0].equals(pressname)) {
					System.out.println(press[i][0] + "\t" + press[i][1] + "\t" + press[i][2] );
				}
				break;
			}
			pressmanagement();
			break;
		case 5:
			// .查询所有出版社
			for (int i = 0; i < press.length; i++) {
				for (int j = 0; j < press[i].length; j++) {
					if (press[i][0]!=null) {
						System.out.println(press[i][j]+"\t");
					}
				}
				break;
			}
			pressmanagement();
			break;
		case 6:
			homePage();
			// 返回上一级菜单
			break;

		default:
			pressmanagement();
			break;
		}

	}

	private static int getFirstNullIndex(String[][] arr) {
		int index = -1;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i][0] == null) {
				index = i;
				break;
			}
		}
		return index;
	}

	private static void BookManagement() {
		//图书管理
		System.out.println("请输入：1.增加 2.删除 3.更新 4.查询 5.返回上一级菜单");
		int select = sc.nextInt();
		switch (select) {
		case 1:
			//增加	
			Morebooks();

			break;
		case 2:
			int Temporaryindex = -1;
			//删除
			System.out.println("请输入要删除的书本名称：");
			String Temporaryname = sc.next();
			for (int i = 0; i < books.length; i++) {
				if (Temporaryname.equals(books[i][1])) {
					Temporaryindex = i;
				}

			}

			if (Temporaryindex != -1) {
				books[Temporaryindex][0] = null;
				books[Temporaryindex][1] = null;
				books[Temporaryindex][2] = null;
				books[Temporaryindex][3] = null;
				books[Temporaryindex][4] = null;
				System.out.println("删除成功！");
			} else {
				System.out.println("删除失败！");
			}
		
			BookManagement();
			break;
		case 3:
			//更新
			System.out.println("请输入ISBN号：");
			String bookISBN = sc.next();
			int booksindex = -1;
			for (int i = 0; i < books.length; i++) {
				if (bookISBN.equals(books[i][0])) {
					booksindex = i;
					break;
				}
			}
			if (booksindex != -1) {
				System.out.println("请输入新的书名：");
				books[booksindex][1] = sc.next();
				System.out.println("请输入新的价格：");
				books[booksindex][2] = sc.next();
				System.out.println("请输入新的出版社：");
				books[booksindex][3] = sc.next();
				System.out.println("请输入新的作者：");
				books[booksindex][4] = sc.next();
				System.out.println("更新成功！！！");

			} else {
				System.out.println("该ISBN号不存在！！！");
			}
			BookManagement();
			break;
		case 4:
			//查询
			inquires();
		case 5:
			//返回上一级菜单
			homePage();
			break;
		default:
			break;
		}

	}

	private static void inquires() {
		System.out.println("请输入查询种类：");
		System.out.println("1.isbn 2.书名（模糊） 3.出版社 4. 作者 5. 价格范围 6.查询所有 7.返回上一级");
		int inquire = sc.nextInt();
		switch (inquire) {
		case 1:
			//isbn
			System.out.println("请输入ISBN号：");
			String tempisbn = sc.next();
			int isbnindex = -1;
			for (int i = 0; i < books.length; i++) {
				if (tempisbn.equals(books[i][0])) {
					isbnindex = i;
				}

			}
			if (isbnindex != -1) {
				for (int i = 0; i < 5; i++) {// 循环打印出来
					System.out.print(books[isbnindex][i] + "  ");

				}
				System.out.println();

			} else {
				System.out.println("输入错误");
			}
			inquires();
			break;
		case 2:
			//书名（模糊）
			System.out.println("请输入书名");
			String bookname = sc.next();
			for (int i = 0; i < books.length; i++) {
				if (books[i][1] != null) {
					if (books[i][1].indexOf(bookname) != -1) {
						System.out.println(books[i][1]);
					}
				} else {
					System.out.println("没有找到这个书名!");
				}
			}
			break;
		case 3:
			//出版社
			System.out.println("请输入出版社名字：");
			String pressname = sc.next();
			for (int i = 0; i < press.length; i++) {
				if (pressname.equals(books[i][0])) {
					System.out.println("ISBN\t书名\t价格\t出版社\t作者");
					for (int j = 0; j < press[i].length; j++) {
						System.out.print(press[i][j] + "\t");
					}
				} else {
					System.out.println("没有找到");
					break;
				}
			}

			break;

		case 4:
			//作者
			System.out.println("请输入作者名");
			String author = sc.next();
			for (int i = 0; i < books.length; i++) {
				if (books[i][4] != null && books[i][4].equals(author)) {
					System.out.println(books[i][0] + "\t" + books[i][1] + "\t" + books[i][2] + "\t" + books[i][3] + "\t"
							+ books[i][4] + "\t");
				}
			}
			inquires();
			break;
		case 5:
			//价格范围
			System.out.println("请输入最小值");
			int min = sc.nextInt();
			System.out.println("请输入最大值");
			int max = sc.nextInt();
			for (int j = 0; j < books.length; j++) {
				if (min <= Integer.parseInt(books[j][2]) && Integer.parseInt(books[j][2]) <= max) {
					System.out.println("ISBN\t书名\t价格\t出版社\t作者");
					for (int i = 0; i < books[j].length; i++) {
						System.out.print(books[j][i] + "\t");
						System.out.println();
					}
				}
				break;
			}

			break;
		case 6:
			//查询所有
			for (int i = 0; i < books.length; i++) {
				for (int j = 0; j < books[i].length; j++) {
					if (books[i][j] != null) {

						System.out.print(books[i][j] + "\t");
						System.out.println();
					}
				}
			}
			break;
		case 7:
			//返回上一级
			BookManagement();
			break;

		default:
			break;
		}

	}

	private static void Morebooks() {
		int index = getFirstNullIndex(books);
		
		System.out.println("请输入图书ISBN:");
		books[index][0] = sc.next();
		System.out.println("请输入书名:");
		books[index][1] = sc.next();
		System.out.println("请输入价格:");
		books[index][2] = sc.next();
		System.out.println("请输入出版社:");
		books[index][3] = sc.next();
		System.out.println("请输入作者:");
		books[index][4] = sc.next();
		System.out.println("添加成功！");

		BookManagement();
	}



}