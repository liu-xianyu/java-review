import java.util.Scanner;

public class Library {
	static Scanner sc = new Scanner(System.in);
	static String[][] users = new String[100][4];
	static String[][] books = new String[100][5];
	static String[][] pubs = new String[100][3];
	
	public static void main(String[] args) {
		inti();
		login();
		admin();
	}
	
	private static void login() {
		while (true) {
			System.out.println("欢迎来到图书馆");
			System.out.println("1.登录  2.注册");
			String key1 = sc.next();
			switch (key1) {
			case "1":
				System.out.println("请输入用户名");
				String username = sc.next();
				System.out.println("请输入密码");
				String password = sc.next();
				boolean flag = false;
				for (int i = 0; i < users.length; i++) {
					if(username.equals(users[i][1]) && password.equals(users[i][2])) {
						flag = true;
						break;
					}
				}
				if(flag==true) {
					System.err.println("登陆成功！");
					System.err.println(username+"， 欢迎您使用闽大书籍管理系统！！！");
					admin();
					
				}else {
					System.out.println("登录失败！");
				}
				
				break;
			case "2":
				int index=-1;
				for (int i = 0; i < users.length; i++) {
					if(users[i][1]==null) {
						index=i;
						break;
					}
				}
				System.out.println("请输入所属部门");
				users[index][0] = sc.next();
				System.out.println("请输入用户名");
				users[index][1] = sc.next();
				System.out.println("请输入密码");
				users[index][2] = sc.next();
				System.out.println("注册成功！");
				break;
			default:
				System.out.println("输入错误！");
				break;
			}
			
		}
		
	}


	//图书管理
	private static void admin() {
		System.err.println("请输入数字进行选择：1 图书管理  2 出版社管理  3 退出登录  4 退出系统");
		String key2=sc.next();
		switch (key2) {
		case "1":
			System.err.println("请输入：1.增加 2.删除 3.更新 4.查询 5.返回上一级菜单");
			String key3 = sc.next();
				switch (key3) {
				case "1":
					addbook();
					admin();
					break;
					
				case "2":
					deletebook();
					admin();
					break;
				case "3":
					update();
					admin();
					break;
				case "4":
					System.out.println("请输入查询种类：1.isbn 2.书名（模糊） 3.出版社 4. 作者 5. 价格范围 6.查询所有 7.返回上一级");
					String key4 = sc.next();
						switch (key4) {
						case "1":
							System.out.println("请输入ISBN号：");
							String ISBN1 = sc.next();
							for (int i = 0; i < books.length; i++) {
								if(ISBN1.equals(books[i][0])) {
									System.out.println(books[i][0]+"\t"+
													books[i][1]+"\t"+
													books[i][2]+"\t"+
													books[i][3]+"\t"+
													books[i][4]	);
								}
							}
							break;
						case "2":
							System.out.println("请输入书名：");
							String bookname2 = sc.next();
							for (int i = 0; i < books.length; i++) {
								if(bookname2.equals(books[i][1])) {
									System.out.println(books[i][0]+"\t"+
											books[i][1]+"\t"+
											books[i][2]+"\t"+
											books[i][3]+"\t"+
											books[i][4]	);
								}
							}
							break;
						case "3":
							System.out.println("请输入出版社：");
							String press1 = sc.next();
							for (int i = 0; i < books.length; i++) {
								if(press1.equals(books[i][2])) {
									System.out.println(books[i][0]+"\t"+
											books[i][1]+"\t"+
											books[i][2]+"\t"+
											books[i][3]+"\t"+
											books[i][4]	);
								}
							} 
							break;
						case "4":
							System.out.println("请输入作者：");
							String author1 = sc.next();
							for (int i = 0; i < books.length; i++) {
								if(author1.equals(books[i][2])) {
									System.out.println(books[i][0]+"\t"+
											books[i][1]+"\t"+
											books[i][2]+"\t"+
											books[i][3]+"\t"+
											books[i][4]	);
								}
							} 
							break;
						case "5":
							System.out.println("请输入价格范围：");
							System.out.println("请输入最小值：");
							int pricemin = sc.nextInt();
							System.out.println("请输入最大值：");
							int pricemax = sc.nextInt();
							for (int i = 0; i < books.length; i++) {
								if(Integer.parseInt(books[i][3])>pricemin && Integer.parseInt(books[i][3])<pricemax) {
									System.out.println(books[i][0]+"\t"+
											books[i][1]+"\t"+
											books[i][2]+"\t"+
											books[i][3]+"\t"+
											books[i][4]	);
								}
							} 
							break;
						case "6":
							for (int i = 0; i < books.length; i++) {
								if(books[i][0]!=null) {
									System.out.println(books[i][0]+"\t"+
											books[i][1]+"\t"+
											books[i][2]+"\t"+
											books[i][3]+"\t"+
											books[i][4]	);
								}
							} 
							break;
						case "7":
							admin();
							break;
	
						default:
							admin();
							break;
						}
				case "5":
					admin();
					break;

				default:
					System.out.println("输入错误！");
					admin();
					break;
				}
			break;
		case "2":
			pubadmin();
			break;
		case "3":
			System.out.println("退出登录！");
			login();
			break;
		case "4":
			System.err.println("退出程序！");
			System.exit(0);
			break;
			
		default:
			System.out.println("输入错误！");
			break;
		}
		
	}
	
	
	//更新
	private static void update() {
		System.out.println("请输入ISBN号：");
		books[index(users)][0] = sc.next();
		System.out.println("请输入新的书名：");
		books[index(users)][1] = sc.next();
		System.out.println("请输入新的价格：");
		books[index(users)][2] = sc.next();
		System.out.println("请输入新的出版社：");
		books[index(users)][3] = sc.next();
		System.out.println("请输入新的作者：");
		books[index(users)][4] = sc.next();
		System.out.println("更新完成！");
		
	}


	//删除
	private static void deletebook() {
		System.err.println("请输入要删除的书本名称：");
		String bookname = sc.next();
		for (int i = 0; i < pubs.length; i++) {
			if(bookname.equals(books[i][1])) {
				for (int j = 0; j < 5; j++) {
					books[i][j]=null;
				}
			}
		}
		
	}


	//添加
	private static void addbook() {
				System.err.println("请输入图书ISBN:");
				books[index(users)][0] = sc.next();
				System.out.println("请输入书名");
				books[index(users)][1] = sc.next();
				System.out.println("请输入价格");
				books[index(users)][2] = sc.next();
				System.out.println("请输入出版社");
				books[index(users)][3] = sc.next();
				System.out.println("请输入作者");
				books[index(users)][4] = sc.next();
				for (int i = 0; i < books.length; i++) {
					if(books[index(users)][0].equals(books[i][0])) {
						System.out.println("该书已存在");
						books[index(users)][0]=null;
						books[index(users)][1]=null;
						books[index(users)][2]=null;
						books[index(users)][3]=null;
						books[index(users)][4]=null;
						break;
					}else {
						System.out.println("添加成功！");
						break;
					}
				}
			}



	//出版社管理
	private static void pubadmin() {
		System.out.println("请输入：1.增加 2.删除 3.更新 4.根据出版社名称查询 5.查询所有出版社 6.返回上一级菜单");
		String key4 = sc.next();
		switch (key4) {
		case "1":
			addbook();
			admin();
			break;
		case "2":
			deletebook();
			admin();
			break;
		case "3":
			update();
			admin();
			break;
		case "4":
			System.out.println("请输入出版社名称：");
			String pubname2 = sc.next();
			for (int i = 0; i < books.length; i++) {
				if(books[i][3]!=null && books[i][3].indexOf(pubname2)!=-1) {
					System.out.println(books[i][0]+"\t"+
									books[i][1]+"\t"+
									books[i][2] );
				}
			}
			pubadmin();
			break;
		case "5":
			for (int i = 0; i < pubs.length; i++) {
				if(books[i][0]!=null) {
					System.out.println(books[i][0]+"\t"+
									books[i][1]+"\t"+
									books[i][2]+"\t"+
									books[i][3]+"\t"+
									books[i][4]	);
				}
			}
			pubadmin();
			break;
		case "6":
			admin();
			break;

		default:
			break;
		}
	}

	
	//查询书
	private static void query() {
		System.out.println("请输入查询种类：1.isbn 2.书名（模糊） 3.出版社 4. 作者 5. 价格范围 6.查询所有 7.返回上一级");
		String key4 = sc.next();
			switch (key4) {
			case "1":
				System.out.println("请输入ISBN号：");
				String ISBN1 = sc.next();
				for (int i = 0; i < pubs.length; i++) {
					if(ISBN1.equals(books[i][0])) {
						System.out.println(books[i][0]+"\t"+
										books[i][1]+"\t"+
										books[i][2]+"\t"+
										books[i][3]+"\t"+
										books[i][4]	);
					}
				}
				admin();
				break;
			case "2":
				System.out.println("请输入书名：");
				String bookname2 = sc.next();
				for (int i = 0; i < books.length; i++) {
					if(bookname2.equals(books[i][1])) {
						System.out.println(books[i][0]+"\t"+
								books[i][1]+"\t"+
								books[i][2]+"\t"+
								books[i][3]+"\t"+
								books[i][4]	);
					}
				}
				break;
			case "3":
				System.out.println("请输入出版社：");
				String press1 = sc.next();
				for (int i = 0; i < books.length; i++) {
					if(press1.equals(books[i][2])) {
						System.out.println(books[i][0]+"\t"+
								books[i][1]+"\t"+
								books[i][2]+"\t"+
								books[i][3]+"\t"+
								books[i][4]	);
					}
				}
				break;
			case "4":
				System.out.println("请输入作者：");
				String author1 = sc.next();
				for (int i = 0; i < books.length; i++) {
					if(author1.equals(books[i][2])) {
						System.out.println(books[i][0]+"\t"+
								books[i][1]+"\t"+
								books[i][2]+"\t"+
								books[i][3]+"\t"+
								books[i][4]	);
					}
				}
				break;
			case "5":
				System.out.println("请输入价格范围：");
				System.out.println("请输入最小值：");
				int pricemin = sc.nextInt();
				System.out.println("请输入最大值：");
				int pricemax = sc.nextInt();
				for (int i = 0; i < books.length; i++) {
					if(Integer.parseInt(books[i][3])>pricemin && Integer.parseInt(books[i][3])<pricemax) {
						System.out.println(books[i][0]+"\t"+
								books[i][1]+"\t"+
								books[i][2]+"\t"+
								books[i][3]+"\t"+
								books[i][4]	);
					}
				}
				break;
			case "6":
				for (int i = 0; i < books.length; i++) {
					if(books[i][0]!=null) {
						System.out.println(books[i][0]+"\t"+
								books[i][1]+"\t"+
								books[i][2]+"\t"+
								books[i][3]+"\t"+
								books[i][4]	);
					}
				}
				break;
			case "7":
				admin();
				break;

			default:
				break;
			}
		
	}


	//赋值
	private static void inti() {
		users[0][0]="软件部";
		users[0][1]="a";
		users[0][2]="123";
		users[0][3]="普通用户";
		
		users[1][0]="软件部";
		users[1][1]="a";
		users[1][2]="123";
		users[1][3]="普通用户";
		
		books[0][0]="1";
		books[0][1]="史记";
		books[0][2]="100";
		books[0][3]="北京出版社";
		books[0][4]="司马迁";
		
		pubs[0][0]="北京出版社";
		pubs[0][1]="北京";
		pubs[0][2]="123";
	}
	//找出空余位置
	public static int index(String[][] users) {
		int index=-1;
		for (int i = 0; i < users.length; i++) {
			if(users[i][1]==null) {
				index=i;
				break;
			}
		}
		return index;
	}
	
}






