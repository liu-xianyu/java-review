import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;

public class Demo1 {
	static String[][] asg = new String [100][3];
	static String[][] users = new String[100][4];
	static String[][] books = new String[100][5];
	static String[][] msg = new String[100][3];
	static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		init();
		选择();

	}

	private static void init() {
		users[0][0] = "软件部";
		users[0][1] = "admin";
		users[0][2] = "123";
		users[0][3] = "员工";

		users[1][0] = "学习部";
		users[1][1] = "123";
		users[1][2] = "123";
		users[1][3] = "普通员工";

		books[0][0] = "123456";
		books[0][1] = "lll";
		books[0][2] = "901";
		books[0][3] = "中华书局";
		books[0][4] = "l";

		books[1][0] = "123457";
		books[1][1] = "lin";
		books[1][2] = "1000";
		books[1][3] = "重庆出版社";
		books[1][4] = "lin";

		books[2][0] = "123458";
		books[2][1] = "lili";
		books[2][2] = "20";
		books[2][3] = "重庆出版社";
		books[2][4] = "li";

		books[3][0] = "666";
		books[3][1] = "ong";
		books[3][2] = "200";
		books[3][3] = "重庆出版社";
		books[3][4] = "ong";

		books[4][0] = "226";
		books[4][1] = "admin";
		books[4][2] = "226";
		books[4][3] = "重庆出版社";
		books[4][4] = "long";

		msg[0][0] = "12456";
		msg[0][1] = "777";
		msg[0][2] = "aaa";

		msg[1][0] = "23456";
		msg[1][1] = "888";
		msg[1][2] = "bbb";
	}

	private static void 选择() {
		System.out.println("欢迎来到闽大图书管理系统！");
		System.out.println("请选择您要执行的操作");
		System.out.println("1.登录\t2.注册");
		int key1 = scanner.nextInt();
		if (key1 == 1) {
			登录();
		} else if (key1 == 2) {
			注册();
		} else {
			System.out.println("输入错误！请重新输入");
			选择();
		}

	}

	private static void 登录() {
		System.out.println("请输入用户名：");
		String usersname = scanner.next();

		System.out.println("请输入密码：");
		String password = scanner.next();

		boolean flag = false;

		for (int i = 0; i < users.length; i++) {
			if (usersname.equals(users[i][1]) && password.equals(users[i][2])) {
				flag = true;
				break;
			}
		}
		if (flag == true) {
			System.out.println("登录成功！");
			选择1();
		} else {
			System.out.println("该用户不存在或者密码错误！请重新登录！");
			登录();
		}

	}

	private static void 注册() {
		int index1 = getFirstNullUserIndex();
		System.out.println("请输入所属部门：");
		users[index1][0] = scanner.next();
		System.out.println("请输入用户名：");
		users[index1][1] = scanner.next();
		System.out.println("请设置登录密码：");
		users[index1][2] = scanner.next();
		users[index1][3] = "普通用户";
		System.out.println("注册成功！");
		选择();

	}

	private static void 选择1() {
		System.out.println("欢迎您使用闽大书籍管理系统！！！");
		System.out.println("1 图书管理  2 出版社管理  3 退出登录  4 退出系统");
		int key2 = scanner.nextInt();
		if (key2 == 1) {
			System.out.println("请选择你要执行的操作");
			System.out.println("1.增加\t2.删除\t3.更新\t4.查询菜单\t5.返回上一级");
			int key3 = scanner.nextInt();
			if (key3 == 1) {
				增加();
			} else if (key3 == 2) {
				删除();
			} else if (key3 == 3) {
				更新();
			} else if (key3 == 4) {
				查询菜单();
			} else if (key3 == 5) {
				返回上一级();
			} else {
				System.out.println("输入错误，请重新输入");
				选择();
			}
		}
		if (key2 == 2) {
			System.out.println("请输入：1.增加 2.删除 3.更新 4.根据出版社名称查询 5.查询所有出版社 6.返回上一级菜单");
			int key4 = scanner.nextInt();
			switch (key4) {
			case 1:
				增加1();
				break;
			case 2:
				删除1();
				break;
			case 3:
				更新1();
				break;
			case 4:
				出版社();
				break;
			case 5:
				查询所有();
				break;
			case 6:
				选择1();
				break;

			default:
				System.out.println("输入错误，请重新输入");
				选择1();
			}
		}
		if(key2==3) {
			System.out.println("已退出该账号，请重新登录");
			登录();
		}
		if(key2==4) {
			System.out.println("退出系统");
		}
	}

	private static void 出版社() {
		System.out.println("请输入要查找的出版社名称：");
		String lin = scanner.next();
		System.out.println("出版社名称\t地址\t联系人");
		for (int i = 0; i < asg.length; i++) {
			if (lin.equals(asg[i][0])) {
				for (int j = 0; j < asg[i].length; j++) {
					if (asg[i][0] != null) {
						System.out.print(asg[i][j] + "\t");
					}
				}
				System.out.println();
			}
		}
		选择1();
		
	}

	private static void 查询所有() {
		// TODO Auto-generated method stub
		System.out.println("出版社名称\t地址\t联系人");
		for (int i = 0; i < asg.length; i++) {
			if (asg[i][0] != null) {
				for (int j = 0; j < asg[i].length; j++) {
					if (asg[i][0] != null) {
						System.out.print(asg[i][j] + "\t");
					}
				}
				System.out.println();
			}
		}
		选择1();
	}
	
	private static void 更新1() {
		// TODO Auto-generated method stub
		int i = getFirstNullUserIndex();
		boolean falg = false;
		System.out.println("输入要更改的出版社名称");
		String name =scanner.next();
		for (int j = 0; j < asg.length; j++) {
			if(name.equals(asg[j][0])) {
				falg = true;
				break;
			}
		}
		if(falg==true) {
			System.out.println("请输入新出版社名称");
			asg[i][0] = scanner.next();
			System.out.println("请输入新地址");
			asg[i][1] = scanner.next();
			System.out.println("请输入新联系人");
			asg[i][2] = scanner.next();
			System.out.println("已更新成功");
			选择1();
		}else {
			System.out.println("错误");
			选择1();
		}
				
	}

	private static void 删除1() {
		// TODO Auto-generated method stub
		System.out.println("请输入要删除出版社");
		String name = scanner.next();
		boolean result = false;
		for (int i = 0; i < asg.length; i++) {
			if (name.equals(asg[i][0])) {
				result = true;
				break;
			}
		}
		if (result == true) {
			for (int i = 0; i < asg.length; i++)
				if (name.equals(asg[i][0])) {
					asg[i][0] = null;
					asg[i][1] = null;
					asg[i][2] = null;
				}
			System.out.println("删除成功");
			选择1();
		} else {
			System.out.println("删除失败");
			选择1();

		}
	}
	private static void 增加1() {
		int i = getFirstNullUserIndex();
		System.out.println("请输入出版社名称");
		asg[i][0] = scanner.next();
		System.out.println("请输入出版社地址");
		asg[i][1] = scanner.next();
		System.out.println("请输入出版社联系人");
		asg[i][2] = scanner.next();
		System.out.println("添加成功");
		选择1();
	}

	private static void 返回上一级() {
		选择1();
	}

	private static void 查询菜单() {
		System.out.println("请输入查询种类：1.isbn 2.书名（模糊） 3.出版社 4. 作者 5. 价格范围 6.查询所有 7.返回上一级");
		int key = scanner.nextInt();
		switch (key) {
		case 1:
			System.out.println("请输入isbn编码");
			String li = scanner.next();
			boolean result = false;
			for (int i = 0; i < books.length; i++) {
				if (li.equals(books[i][0])) {
					result = true;
					break;
				}
			}
			if (result == true) {
				for (int i = 0; i < books.length; i++) {
					if (li.equals(books[i][0])) {
						System.out.println("ISBN\t书名\t 价格 \t出版社 \t 作者");
						for (int h = 0; h < books[i].length; h++) {
							System.out.print(books[i][h] + "\t");
						}
						System.out.println();
						查询菜单();
						break;
					}
				}
			} else {
				System.out.println("没有这本书,请重新选择谢谢");
				查询菜单();
			}

			break;
		case 2:
			System.out.println("请输入书名");
			String lin = scanner.next();
			boolean result1 = false;
			for (int i = 0; i < books.length; i++) {
				if (books[i][1] != null && books[i][1].indexOf(lin) != -1) {
					result1 = true;
					break;
				}
			}
			if (result1 == true) {
				for (int i = 0; i < books.length; i++) {
					if (books[i][1] != null && books[i][1].indexOf(lin) != -1) {
						System.out.println("ISBN\t书名\t价格\t出版社\t作者");
						for (int h = 0; h < books[i].length; h++) {
							System.out.print(books[i][h] + "\t");
						}
						System.out.println();
						查询菜单();
						
					}
				}
			} else {
				System.out.println("你的查询有错误，没有这本书");
				查询菜单();
			}

			
		case 3:
			System.out.println("请输入出版社文字进行选择:1.中华书局\t2.作家出版社\t3.重庆出版社\t4.南海出版公司");
			String ing = scanner.next();
			boolean result11 = false;
			for (int i = 0; i < books.length; i++) {
				if (ing.equals(books[i][3])) {
					result11 = true;
					break;
				}
			}
			if (result11 == true) {
				for (int i = 0; i < books.length; i++) {
					if (ing.equals(books[i][3])) {
						System.out.println("ISBN\t书名\t价格\t出版社\t作者");

						for (int h = 0; h < books[i].length; h++) {
							System.out.print(books[i][h] + "\t");
						}
						System.out.println();
						查询菜单();
						break;
					}

				}

			} else {

				System.out.println("你的查询有错误");
				查询菜单();
			}

			break;
		case 4:
			System.out.println("请输入你要查找的书籍作者的名字");
			String ong = scanner.next();
			boolean result2 = false;
			for (int i = 0; i < books.length; i++) {
				if (ong.equals(books[i][4])) {
					result2 = true;
					break;
				}
			}
			if (result2 == true) {
				for (int i = 0; i < books.length; i++) {
					if (ong.equals(books[i][4])) {
						System.out.println("ISBN\t书名\t价格\t出版社\t作者");
						for (int h = 0; h < books[i].length; h++) {
							System.out.print(books[i][h] + "\t");
						}
						System.out.println();
						查询菜单();
					}

				}
			}

			else {
				System.out.println("没有叫这个名字的作者，请重新输入。");
				查询菜单();
			}

			break;
		case 5:
			System.out.println("请输入要查找的价格范围：");
			System.out.println("最小价格");
			int min = scanner.nextInt();
			System.out.println("最大价格");
			int max = scanner.nextInt();
			System.out.println("ISBN\t\t书名\t价格\t出版社\t作者");
			for (int i = 0; i < books.length; i++) {
				if (min <= Integer.parseInt(books[i][2]) && Integer.parseInt(books[i][2]) <= max) {
					for (int j = 0; j < 5; j++) {
						System.out.print(books[i][j] + "\t");
					}
					System.out.println();
					
				}else {
					System.out.println("没有这个范围的书");

				}

			}
			break;
		case 6:
			System.out.println("ISBN\t书名\t价格\t出版社\t作者");
			for (int i = 0; i < books.length; i++) {
				if(books[i][0]!=null) {
				for (int j = 0; j < books[i].length; j++) {
					System.out.print(books[i][j] + "\t");
				}
				System.out.println();
				}
			}
			查询菜单();

			break;
		case 7:
			选择1();
			break;

		default:
			System.out.println("输入的数字不符合条件请重新输入");
			查询菜单();
			break;
		}

	}

	private static void 更新() {
		// TODO Auto-generated method stub
		System.out.println("请输入ISBN号");
		String ling = scanner.next();
		boolean flag = true;
		for (int i = 0; i < books.length; i++) {
			if (ling.equals(books[i][0])) {
				flag = false;
				break;
			}
		}
		if (flag == false) {
			for (int i = 0; i < asg.length; i++) {
				if (ling.equals(books[i][0])) {
					System.out.println("请输入新书名");
					books[i][1] = scanner.next();
					System.out.println("请输入价格");
					books[i][2] = scanner.next();
					System.out.println("请输入出版社");
					books[i][3] = scanner.next();
					System.out.println("请输入作者");
					books[i][4] = scanner.next();
					System.out.println("更新成功!!!");
					选择1();
				}
			}
		} else if(flag=true) {
			System.out.println("该ISBN号不存在");
			选择1();
		}
	}

	private static void 增加() {
		// 图书编码唯一，如果新增的图书信息编码已存在，提示“图书已存在”；不存在才进行新增书籍信息；
		int index = getFirstNullUserIndex();// 找出书的数组中，第一个null的位置

		System.out.println("请输入你要添加书籍的ISBN：");
		String ISBN = scanner.next();

		boolean result = isExistISBN(ISBN);// 判断ISBN号是否已存在。

		if (result) {
			// ISBN号重复的情况
			System.out.println("该书已存在,请重新输入一个ISBN编码");
		} else {
			// ISBN号没有重复的情况，没有重复，那就赋值到数组中

			books[index][0] = ISBN;
			// 接下来就继续做赋值的动作。
			System.out.println("书籍名称");
			books[index][1] = scanner.next();
			System.out.println("请输入出版社");
			books[index][2] = scanner.next();
			System.out.println("请输入价格");
			books[index][3] = scanner.next();
			System.out.println("请输入作者");
			books[index][4] = scanner.next();
			System.out.println("书籍添加成功!!!");
		}
		选择1();
	}

	private static void 删除() {
		// 根据ISBN编码删除图书；
		System.out.println("请输入要删除书籍的名称");
		String name = scanner.next();

		boolean result = false;
		for (int i1 = 0; i1 < books.length; i1++) {
			if (name.equals(books[i1][1])) {
				result = true;
				break;
			}
		}
		if (result == true) {
			for (int i = 0; i < books.length; i++)
				if (name.equals(books[i][1])) {
					books[i][0] = null;
					books[i][1] = null;
					books[i][2] = null;
					books[i][3] = null;
					books[i][4] = null;
				}
			System.out.println("删除成功");
			选择1();
		} else {
			System.out.println("没有这本书");
			选择1();

		}
	}

	private static int getFirstNullUserIndex() {// 寻找数值一个个为null的索引
		int index = -1;
		for (int i = 0; i < users.length; i++) {
			if (books[i][0] == null) {
				index = i;
				break;
			}
		}
		return index;
	}

	private static boolean isExistISBN(String iSBN) {
		boolean flag = false;
		for (int i = 0; i < books.length; i++) {
			if (iSBN.equals(books[i][0])) {
				flag = true;
				break;
			}
		}
		return flag;
	}
}