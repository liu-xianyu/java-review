import java.util.Iterator;
import java.util.Scanner;

public class Pinwei {

	public static void main(String[] args) {
/*		-   需求：在编程竞赛中，有6个评委为参赛的选手打分，分数为0-100的整数分。
        选手的最后得分为：去掉一个最高分和一个最低分后 的4个评委平均值 (不考虑小数部分)。

-   思路：
1.定义一个数组，用动态初始化完成数组元素的初始化，长度为6
2.键盘录入评委分数
3.由于是6个评委打分，所以，接收评委分数的操作，用循环
4.求出数组最大值
5.求出数组最小值
6.求出数组总和
7.按照计算规则进行计算得到平均分
8.输出平均分*/
		
		int avg=0;
		int sum=0;
		int[]zhi=new int [6];
		int max=zhi[0];
		int min=zhi[0];
		Scanner Score =new Scanner(System.in);
		for (int i = 0; i < zhi.length; i++) {
				System.out.println("请输入分数（在0~100之间）：");
				zhi[i]=Score.nextInt();
				sum=zhi[i]+sum;
			if (zhi[i]<=100&&zhi[i]>=0) {
				max=zhi[0];
				for (int j = 0; j < zhi.length; j++) {
					if (zhi[i]>max) {
						max=zhi[i];
					}
				}
				min=zhi[0];
				for (int j2 = 1; j2 < zhi.length; j2++) {
					if (zhi[i]<min) {
						min=zhi[i];
					}
					}
				
			}else {
				System.out.println("输入错误，无法识别！（请输入0~100之间的分数）");	
			}
		}sum=sum-min-max;
		 avg=sum/4;
		System.out.println("最后平均分为："+avg);
		System.out.println(max);
		System.out.println(min);
	}

}
