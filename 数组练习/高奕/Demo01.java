package lesson;

import java.util.Scanner;

public class Demo01 {
//    1.定义一个数组，用动态初始化完成数组元素的初始化，长度为6
//    2.键盘录入评委分数
//    3.由于是6个评委打分，所以，接收评委分数的操作，用循环
//    4.求出数组最大值
//    5.求出数组最小值
//    6.求出数组总和
//    7.按照计算规则进行计算得到平均分
//    8.输出平均分
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//-   需求：在编程竞赛中，有6个评委为参赛的选手打分，分数为0-100的整数分。
		// 选手的最后得分为：去掉一个最高分和一个最低分后 的4个评委平均值 (不考虑小数部分)。

		int [] num = new int[6];
		Scanner scan =new Scanner(System.in);
		
		for (int i = 0; i < num.length; i++) {
			System.out.println("请输入第"+(i+1)+"位评委的分数");
			num[i]=scan.nextInt();
		}
		int max=0;
		int min=5;
		for (int j = 0; j < num.length; j++) {
			if (num[j]>max) {
				max=num[j];
			}
			if (num[j]<min) {
				min=num[j];
			}
		}
		System.out.println(max);
		System.out.println(min);
		int sum=0;
		int j=0;
		for (int i = 0; i < num.length; i++) {
			sum=sum+num[i];
			j=j+1;
		
		}
		System.out.println("选手的最后的得分为"+(sum-max-min)/j);
		
	}

}
