import java.util.Scanner;

public class Demo01 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int ar[] = new int [6];
		
		for (int i = 0; i < ar.length; i++) {
			System.out.println("输入评分：");
			ar[i] = sc.nextInt();
			if (ar[i] >= 0 && ar[i] <= 100 ) {
				continue;
			}else {
				System.out.println("请重新输入！");
				i--;
			}
		}
		
		int max = ar[0];
		int min = ar[0];
		
		for (int i = 0; i < ar.length; i++) {
			if (max < ar[i]) {
				max = ar[i];
			}
		}
		
		for (int i = 0; i < ar.length; i++) {
			if (min > ar[i]) {
				min = ar[i];
			}
		}
		
		int s = 0;
		
		for (int i = 0; i < ar.length; i++) {
			s = s + ar[i];
		}
		
		int avg = 0 ;
		avg = (s - max - min) / ar.length-2;
		System.out.println("选手最后得分为：" + avg);
		
		System.out.println("end");
	}

}
