import java.util.Scanner;
//-   需求：在编程竞赛中，有6个评委为参赛的选手打分，分数为0-100的整数分。
//选手的最后得分为：去掉一个最高分和一个最低分后 的4个评委平均值 (不考虑小数部分)。

//-   思路：
//1.定义一个数组，用动态初始化完成数组元素的初始化，长度为6
//2.键盘录入评委分数
//3.由于是6个评委打分，所以，接收评委分数的操作，用循环
//4.求出数组最大值
//5.求出数组最小值
//6.求出数组总和
//7.按照计算规则进行计算得到平均分
//8.输出平均分

public class PWDF {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
	    int[] a =new int[6];
	    for(int i=0 ; i <6 ; i++) {
	    	System.out.println("请输入第"+(i+1)+"评委打分");
	    	a[i]=sc.nextInt();
	    	if(a[i]>0 && a[i]<100){
	    		continue;
	    	}else {
	    		System.out.println("请输入正确评分！");
	    		i--;
	    	}
	    }
	    int max = a[0];
	    int min = a[0];
	    int sum = 0;
	    
	    for (int i = 0; i < a.length; i++) {
			if(a[i]>max) {
				max=a[i];
			}
			if(a[i]<min) {
				min=a[i];
			}
			sum=sum+a[i];
		}
	    System.out.println("最大值为："+max);
	    System.out.println("最小值为："+min);
	    System.out.println("总和为："+sum);
	    System.out.println("平均值为："+(sum-max-min)/4);
	}

}