package Less;

import java.util.Scanner;

public class Demo01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//-   需求：在编程竞赛中，有6个评委为参赛的选手打分，分数为0-100的整数分。
        
		//选手的最后得分为：去掉一个最高分和一个最低分后 的4个评委平均值 (不考虑小数部分)。

		//-   思路：
		        //1.定义一个数组，用动态初始化完成数组元素的初始化，长度为6
		        //2.键盘录入评委分数
		        //3.由于是6个评委打分，所以，接收评委分数的操作，用循环
		        //4.求出数组最大值
		        //5.求出数组最小值
		        //6.求出数组总和
		        //7.按照计算规则进行计算得到平均分
		        //8.输出平均分
		Scanner scanner = new Scanner(System.in);
		
		int [] num = new int [6];
		System.out.println("请输入评委分数");
		num[0] = scanner.nextInt();
		System.out.println("请输入评委分数");
		num[1] = scanner.nextInt();
		System.out.println("请输入评委分数");
		num[2] = scanner.nextInt();
		System.out.println("请输入评委分数");
		num[3] = scanner.nextInt();
		System.out.println("请输入评委分数");
		num[4] = scanner.nextInt();
		System.out.println("请输入评委分数");
		num[5] = scanner.nextInt();
		int max=0;
		int min=100;
		for (int i = 0; i < num.length; i++) {
			if (num[i]>max) {
				max=num[i];
				
			}
		}
		System.out.println("最大值："+max);
		//
		for (int i = 0; i < num.length; i++) {
			if (num[i]<min) {
				min=num[i];
				
			}
		}
		System.out.println("最小值："+min);
		
		System.out.println(" 平均分为"+（num[0]+num[1]+num[2]+num[3]+num[4]+num[5]-max-min）/4);
		
		
		
		
	}

}