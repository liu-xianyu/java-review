import java.util.Scanner;

public class DEMO3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//- 需求：设计一个方法，该方法能够同时获取数组的最大值，和最小值
		//- 注意: return语句, 只能带回一个结果.
		//- 提示:返回一个数组，数组里第一个元素是最大值，第二个元素是最小值
		Scanner scan = new Scanner(System.in);
		
		int arr[] = new int[5];
		
		for (int i = 0; i < arr.length; i++) {
			System.out.println("请输入第"+(i+1)+"个数：");
			arr[i] = scan.nextInt();
		}
		
		bubble(arr);

		int max = arr[arr.length - 1] ;
		System.out.println("最大值为："+ max);
		
		int min  = arr[0] ;
		System.out.println("最小值为："+ min);
		
	}
	
	public static int[] bubble(int[] arr) {
		int temp ;
		
		for (int i = 0; i < arr.length; i++) {
			for (int j = arr.length - 1; j > i; j--) {
				if (arr[j] < arr[j - 1]) {
					temp = arr[j - 1] ;
					arr[j - 1] = arr[j] ;
					arr[j] = temp;
				}
			}
		}
		return arr ;
	}

}
