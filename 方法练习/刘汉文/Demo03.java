
public class Demo03 {

	public static void main(String[] args) {
		// 无参数，无返回值
//		sayHello();
		
		// 有参数，无返回值
//		bigOrSmall(50,100);
		
		// 无参数，有返回值
//		int result = getMax();
//		System.out.println(getMax());
		
		//有参数，有返回值。
		int r = getMax2(100,99);
		System.out.println(r);
		
		
	}
	
	// 无参数，无返回值
	//实现的功能：专门用来给人打招呼的
	public static void sayHello() {
		System.out.println("你好，世界！");
	}
	
	//有参数，无返回值
	//比大小
	public static void bigOrSmall(int num1,int num2) {
		if(num1>num2) {
			System.out.println("更大的数是："+num1);
		}else {
			System.out.println("更大的数是："+num2);
		}
	}
	
	//无参数，有返回值
	//求5*6的和
	public static int getMax() {
		int a = 5;
		int b = 6;
		int sum = a + b;
		return sum;
	}
	
	//有参数，有返回值
	//比大小，返回更大的值
	public static int getMax2(int num1,int num2) {
		int result;
		
		if(num1>num2) {
			result=num1;
		}else {
			result=num2;
		}
		
		return result;
	}

}
