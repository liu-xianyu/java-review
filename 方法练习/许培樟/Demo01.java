
public class Demo01 {

	public static void main(String[] args) {
		int a [] = {1,10,20,30,40};
		max(a);
	}
	
	
    public static void max(int[] a ) {
    	int max=a[0];
    	for (int i = 0; i < a.length; i++) {
			if (a[i]>max) {
				max=a[i];
			}
		}
    	System.out.println(max );
    }
}
