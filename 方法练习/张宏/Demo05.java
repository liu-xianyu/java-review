package md.lesson02.com;

public class Demo05 {

	public static void main(String[] args) {
		// 需求：设计一个方法用于获取数组中元素的最大值 

		//* 思路：

		// * ①定义一个数组，用静态初始化完成数组元素初始化
		//* ②定义一个方法，用来获取数组中的最大值，最值的认知和讲解我们在数组中已经讲解过了
		//* ③调用获取最大值方法，用变量接收返回结果
		//* ④把结果输出在控制台
			int [] arr = {11,22,33,44};
			getmax(arr);
	}
	
	public static int getmax(int [] num1) {
		int max = num1[0];
		for (int i = 0; i < num1.length; i++) {
			if (max<num1[i]) {
				max=num1[i];
			}
		}
		
		System.out.println("max="+max);
		return max;
			
		
	
      }
	


}