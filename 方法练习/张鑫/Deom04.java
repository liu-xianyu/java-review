
public class Deom04 {

	public static void main(String[] args) {
		int [] arr = {20,30,40,50,60};
		int[] maxandmin = getmaxandmin(arr);
		System.out.println(maxandmin[0]);
		System.out.println(maxandmin[1]);
	}
	public static int [] getmaxandmin(int[] arr) {
		int max = arr[0];
		for (int i = 1; i < arr.length; i++) {
			if (max < arr[i]) {
				max = arr[i];
			}
		}
		int min = arr[0];
		for (int i = 1; i < arr.length; i++) {
			if (min > arr[i]) {
				min = arr[i];
			}
		}
		int [] maxandmin = {max,min};
		return maxandmin;
	}
}
