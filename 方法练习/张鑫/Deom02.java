
public class Deom02 {

	public static void main(String[] args) {
		int[] arr = {2,3,4,5,6};
		int number = max(arr);
		System.out.println(number);
	}
	public static int max(int[] arr) {
		int max = arr[0];
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] > max) {
				max = arr [i];
			}
		}
		return max;
	}
}
