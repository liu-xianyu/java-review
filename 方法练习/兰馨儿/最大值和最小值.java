import java.awt.geom.Area;

//- 需求：设计一个方法，该方法能够同时获取数组的最大值，和最小值
//
//- 注意: return语句, 只能带回一个结果.
//
//- 提示:返回一个数组，数组里第一个元素是最大值，第二个元素是最小值

public class 最大值和最小值 {
	public static void main(String[] args) {
		int []arr = {78,90,10};
		int []result = getMax(arr);
		System.out.println("最大值是：" + arr[0] +"\t" +"最小值是:" + arr[1]);
	}

	public static int[] getMax(int[]arr){
		int max = arr[0];
		for (int i = 0; i < arr.length; i++) {
			if (arr[i]>max) {
				max = arr[i];
		}
		 }
		int min = arr[0];
		for (int i = 0; i < arr.length; i++) {
			if (arr[i]<min) {
			    min = arr[i];
		 }
		}
		
		int [] arry = new int[2];
		arr[0] = max;
		arr[1] = min;
		return arr;
}}
