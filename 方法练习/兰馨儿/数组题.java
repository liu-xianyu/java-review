
//如果一个数组保存元素是有序的（从大到小），向这个数组中插入一个数，使得插入后的数组元素仍然保持有序。

public class 数组题 {

	public static void main(String[] args) {
		int []arr = {90,70,60,50,10};
		for (int i = 0; i < arr.length-1; i++) {
			int removeNum = arr[i+1];//从数组中抽出的数字
			int j = 0;
			for (j = i; j >=0; j--) {
				if (removeNum<arr[j]) {
					arr[j+1] = arr[j]; 
				}else {
					break;
				}
			}
			arr[j+1] = removeNum;
		}
		for (int i = 0; i < arr.length; i++) {
		System.out.print(arr[i]+ "\t");
	}
}}