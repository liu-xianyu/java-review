
//* 需求：设计一个方法用于获取数组中元素的最大值 
//
//* 思路：
//
//  * ①定义一个数组，用静态初始化完成数组元素初始化
//  * ②定义一个方法，用来获取数组中的最大值，最值的认知和讲解我们在数组中已经讲解过了
//  * ③调用获取最大值方法，用变量接收返回结果
//  * ④把结果输出在控制台

public class 最大值 {
	public static void main(String[]args) {
			int []arr = {56,87,80,56,90};
			int [] brr = getMax(arr);
			System.out.println("最大值是:" + arr[0]);
		}
		public static int[] getMax(int[]arr) {
			int max = arr[0];
			for (int i = 0; i < arr.length; i++) {
				if (arr[i]>max) {
					max = arr[i];
				}	
			}
			arr[0] = max ;
			return arr;
		}}
	

