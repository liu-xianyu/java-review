
public class Demo01 {

	public static void main(String[] args) {
//		- 需求：设计一个方法，该方法能够同时获取数组的最大值，和最小值
//
//		- 注意: return语句, 只能带回一个结果.
//
//		- 提示:返回一个数组，数组里第一个元素是最大值，第二个元素是最小值
int[] arr= {11,44,56};
int []result=getMax(arr);
System.out.println("数组中最大值是"+result[0]+";"+"最小值是："+result[1]);
	}
	public static int[] getMax(int[] arr){
		int max =arr[0];
		
		int min =arr[0];
		for (int i = 0; i < arr.length; i++) {
		if(max<arr[i]){
			max=arr[i];
		}
		}
		for (int i = 0; i< arr.length; i++) {
			if(min>arr[i]) {
				min=arr[i];
			}
			
		}
		int[] result=new int[2];
		result[0]=max;
		result[1]=min;
		return result;
			
		}
	}	
	