package fang;

public class Domo03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		- 需求：设计一个方法，该方法能够同时获取数组的最大值，和最小值
//
//		- 注意: return语句, 只能带回一个结果.
//
//		- 提示:返回一个数组，数组里第一个元素是最大值，第二个元素是最小值
		int[] array = {80,20,30};
		int [] acc=fan(array);
		for (int i = 0; i < acc.length; i++) {
			System.out.println(acc[i]);	
		}
		
	}
	public static int[] fan(int[] a){
		int [] fanhui = new int[2];
		int max=a[0];
		int min=a[0];
		for (int i = 0; i < a.length; i++) {
			if (max<a[i]) {
				max=a[i];
			}
			if (min>a[i]) {
				min=a[i];
			}
		}
		fanhui[0]=max;
		fanhui[1]=min;
		return fanhui;
	}
}
