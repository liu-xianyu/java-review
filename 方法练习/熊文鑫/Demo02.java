import java.util.Scanner;

public class Demo02 {

	public static void main(String[] args) {
		// 设计一个方法，该方法能够同时获取数组的最大值，和最小值
		Scanner scan = new Scanner(System.in);
		
		int arr[] = new int[5];
		
		for (int i = 0; i < arr.length; i++) {
			System.out.println("请输入第"+(i+1)+"个数：");
			arr[i] = scan.nextInt();
		}
		
		int MaxOrMin[] = new int[2] ;
		MaxOrMin = bubble(arr) ;
		
		int max = MaxOrMin[0] ;
		int min = MaxOrMin[1] ;
		
		System.out.println("该数组的最大值为：" + max);
		System.out.println("该数组的最小值为：" + min);
		
	}
	
	public static int[] bubble(int[] arr) {
		int max = arr[0] ; 
		int min = arr[1] ;
		
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] > max) {
				max = arr[i] ;
			}
			if (min > arr[i]) {
				min = arr[i] ;
			}
		}
		
		int array[] = new int[2];
		
		array[0] = max ;
		array[1] = min ;
		
		return array ;
	}

}
