import java.util.Scanner;

public class Demo01 {
		public static void main(String[] args) {
			//找出最大值
			Scanner scan = new Scanner(System.in) ;
			System.out.println("请输入第一个数字：");
			int num1 = scan.nextInt() ;
			System.out.println("请输入第二个数字：");
			int num2 = scan.nextInt() ;
			
			int max = Getmax(num1 , num2) ;
			
			System.out.println("最大值为："+ max);
		}
		
		public static int Getmax(int num1 , int num2) {
			int max = 0 ;
			
			if (num1 > num2) {
				max = num1 ;
			}else {
				max = num2 ;
			}
			return max ;
		}
}
