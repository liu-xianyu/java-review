import java.util.Scanner;

public class Dome01 {
    public static void main(String[]args) {
//
//    	  * ①定义一个数组，用静态初始化完成数组元素初始化
//    	  * ②定义一个方法，用来获取数组中的最大值，最值的认知和讲解我们在数组中已经讲解过了
//    	  * ③调用获取最大值方法，用变量接收返回结果
//    	  * ④把结果输出在控制台
    	int [] arr = {20,50,63,95};
    	int max = fan(arr);
    	System.out.println(max);
    }
    public static int fan(int[] a) {
    	int max=0;
    	for (int i = 0; i < a.length; i++) {
			if(a[i]>max) {
				max=a[i];
			}
		}
    	return max;
    	
    }
}
