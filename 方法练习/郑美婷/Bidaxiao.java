
public class Bidaxiao {

	public static void main(String[] args) {
//		- 需求：设计一个方法，该方法能够同时获取数组的最大值，和最小值
//
//		- 注意: return语句, 只能带回一个结果.
//
//		- 提示:返回一个数组，数组里第一个元素是最大值，第二个元素是最小值
		int[] arryy= {11,12,13,14,18,19,20};
		int[] result=maxormin(arryy);
		System.out.println("数组最大值为："+result[0]+",数组最小值为："+result[1]);
	}
	public static int[] maxormin(int[]arry) {
		int max=arry[0];
		int min=arry[1];
		for (int i = 0; i < arry.length; i++) {
			if(arry[i]>max) {
				max=arry[i];
			}
		}
		for (int i = 0; i < arry.length; i++) {
			if(arry[i]<min) {
				min=arry[i];
			} 
		}
		int arr[]=new int[2];
		arr[0]=max;
		arr[1]=min;
		return arr;
	}	
}
