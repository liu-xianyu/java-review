com.array.test;

import java.util.Arrays;
import java.util.Scanner;

//实现：在有序数组中插入一个元素
// 对新数组排序---直接使用Arrays.sort()方法
public class ArrayInsertTwo {
    public static void main(String[] args) {
        int[] arr=new int[]{5,8,19,20,23};
        System.out.println("原数组为：arr="+ Arrays.toString(arr));
        Scanner sc=new Scanner(System.in);
        System.out.println("请输入插入的数据");
        arr[arr.length-1]=sc.nextInt();   //把要插入的数据放到数组的最后一个
        Arrays.sort(arr);
        System.out.println("新数为：arr="+ Arrays.toString(arr));
    }
}    