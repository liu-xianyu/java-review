create database ClassicDb

GO



use ClassicDb

GO



create table StudentInfo

(

    Id int PRIMARY key not null IDENTITY,

    StudentCode nvarchar(80),

    StudentName nvarchar(80),

    Birthday date not null,

    Sex nvarchar(2),

    ClassId int not null

)



GO



-- select * from StudentInfo



insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('01' , '赵雷' , '1990-01-01' , 'm',1)

insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('02' , '钱电' , '1990-12-21' , 'm',1)

insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('03' , '孙风' , '1990-12-20' , 'm',1)

insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('04' , '李云' , '1990-12-06' , 'm',1)

insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('05' , '周梅' , '1991-12-01' , 'f',1)

insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('06' , '吴兰' , '1992-01-01' , 'f',1)

insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('07' , '郑竹' , '1989-01-01' , 'f',1)

insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('09' , '张三' , '2017-12-20' , 'f',1)

insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('10' , '李四' , '2017-12-25' , 'f',1)

insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('11' , '李四' , '2012-06-06' , 'f',1)

insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('12' , '赵六' , '2013-06-13' , 'f',1)

insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('13' , '孙七' , '2014-06-01' , 'f',1)





GO





CREATE TABLE Teachers

(

    Id int PRIMARY key not null IDENTITY,

    TeacherName nvarchar(80)

)



go

-- select * from Teachers



insert into Teachers (TeacherName) values('张三')

insert into Teachers (TeacherName) values('李四')

insert into Teachers (TeacherName) values('王五')



GO



create table CourseInfo

(

    Id int PRIMARY key not null IDENTITY,

    CourseName NVARCHAR(80) not null,

    TeacherId int not null

)



go

-- select * from CourseInfo



insert into CourseInfo (CourseName,TeacherId) values( '语文' , 2)

insert into CourseInfo (CourseName,TeacherId) values( '数学' , 1)

insert into CourseInfo (CourseName,TeacherId) values( '英语' , 3)



GO



create table StudentCourseScore

(

    Id int PRIMARY key not null IDENTITY,

    StudentId int not null,

    CourseId int not null,

    Score int not null

)

go

-- select * from StudentCourseScore



insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='01') , 1 , 80)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='01') , 2 , 90)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='01') , 3 , 99)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='02') , 1 , 70)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='02') , 2 , 60)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='02') , 3 , 80)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='03') , 1 , 80)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='03') , 2 , 80)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='03') , 3 , 80)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='04') , 1 , 50)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='04') , 2 , 30)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='04') , 3 , 20)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='05') , 1 , 76)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='05') , 2 , 87)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='06') , 1 , 31)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='06') , 3 , 34)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='07') , 2 , 89)

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='07') , 3 , 98)



go
-- 练习题目：

select *from CourseInfo
select *from StudentCourseScore
select *from StudentInfo
select *from Teachers


-- 1.查询"数学 "课程比" 语文 "课程成绩高的学生的信息及课程分数

select * from 
	(select * from StudentCourseScore where courseId = 1) as A
		inner join StudentInfo s on s.Id = A.Id,
	(select * from StudentCourseScore where CourseId = 2) as B
			where A.Score > B.Score and A.StudentId = B.StudentId


-- 1.1 查询同时存在" 数学 "课程和" 语文 "课程的情况

select StudentId from StudentCourseScore
	where CourseId = 1 or CourseId = 2
	 group by StudentId
		HAVING COUNT(distinct CourseId) = 2
		

-- 1.2 查询存在" 数学 "课程但可能不存在" 语文 "课程的情况(不存在时显示为 null )

select * from
	(select * from StudentCourseScore where CourseId = 2) A
	left join (select * from StudentCourseScore where CourseId = 1) B on A.StudentId = B.StudentId

-- 1.3 查询不存在" 数学 "课程但存在" 语文 "课程的情况

select * from 
	(select * from StudentCourseScore where CourseId = 1) A
	left join (select * from StudentCourseScore where CourseId = 2) B on A.StudentId = B.StudentId
		where B.CourseId is null

-- 2.查询平均成绩大于等于 60 分的同学的学生编号和学生姓名和平均成绩

select Studentcode 学生编号,StudentName 学生姓名,AVG(Score) 平均成绩 from StudentCourseScore
	inner join StudentInfo on StudentCourseScore.StudentId = StudentInfo.StudentCode
		group by Studentcode,StudentName
			HAVING AVG(Score) >= 60

-- 3.查询在 成绩 表存在成绩的学生信息

select * from StudentCourseScore 
	left join StudentInfo on StudentCourseScore.StudentId = StudentInfo.StudentCode

-- 4.查询所有同学的学生编号、学生姓名、选课总数、所有课程的总成绩(没成绩的显示为 null )

select StudentCode ,StudentName ,COUNT(CourseId) ,SUM(Score) from StudentCourseScore scs
	right join StudentInfo si on scs.StudentId = si.StudentCode
		group by StudentCode,StudentName

-- 4.1 查有成绩的学生信息

select * from StudentCourseScore scs
	inner join StudentInfo si on scs.StudentId = si.StudentCode

-- 5.查询「李」姓老师的数量

select COUNT(TeacherName) from Teachers
	where TeacherName like '李%'

-- 6.查询学过 「张三」老师授课 的 同学的信息

select * from StudentCourseScore A
	inner join CourseInfo B on A.CourseId = B.Id
	inner join StudentInfo C on A.StudentId = C.StudentCode
	inner join Teachers D on B.TeacherId = D.Id
		where TeacherName = '张三'

-- 7.查询没有学全所有课程的同学的信息
select * from CourseInfo
select * from StudentCourseScore
select * from StudentInfo
select * from Teachers

select StudentCode ,StudentName from StudentCourseScore A
	inner join StudentInfo B on A.StudentId = B.StudentCode 
		group by StudentCode,StudentName
			HAVING COUNT(CourseId) < (select COUNT(CourseName) from CourseInfo)

-- 8.查询至少有一门课与学号为" 01 "的同学所学相同的同学的信息

select StudentCode ,StudentName from StudentCourseScore A
	inner join StudentInfo B on A.StudentId = B.StudentCode
		group by  StudentCode ,StudentName
			having count(CourseId) > 0

-- 9.查询和" 01 "号的同学学习的课程 完全相同的其他同学的信息

select StudentCode ,StudentName from StudentCourseScore A
	inner join StudentInfo B on A.StudentId = B.StudentCode
		group by  StudentCode ,StudentName
			having count(CourseId)=(select COUNT(CourseName) from CourseInfo)

-- 10.查询没学过"张三"老师讲授的任一门课程的学生姓名

select distinct studentName from StudentCourseScore A
	inner join CourseInfo B on A.CourseId = B.Id
	inner join StudentInfo C on A.StudentId = C.StudentCode
	inner join Teachers D on B.TeacherId = D.Id
		where TeacherName != '张三'


-- 11.查询两门及其以上不及格课程的同学的学号，姓名及其平均成绩



-- 12.检索" 数学 "课程分数小于 60，按分数降序排列的学生信息



-- 13.按平均成绩从高到低显示所有学生的所有课程的成绩以及平均成绩



-- 14.查询各科成绩最高分、最低分和平均分：



-- 15.以如下形式显示：课程 ID，课程 name，最高分，最低分，平均分，及格率，中等率，优良率，优秀率

/*

	及格为>=60，中等为：70-80，优良为：80-90，优秀为：>=90



	要求输出课程号和选修人数，查询结果按人数降序排列，若人数相同，按课程号升序排列



	按各科成绩进行排序，并显示排名， Score 重复时保留名次空缺

*/	

	

-- 15.1 按各科成绩进行排序，并显示排名， Score 重复时合并名次



-- 16.查询学生的总成绩，并进行排名，总分重复时保留名次空缺



-- 16.1 查询学生的总成绩，并进行排名，总分重复时不保留名次空缺



-- 17.统计各科成绩各分数段人数：课程编号，课程名称，[100-85]，[85-70]，[70-60]，[60-0] 及所占百分比



-- 18.查询各科成绩前三名的记录



-- 19.查询每门课程被选修的学生数



-- 20.查询出只选修两门课程的学生学号和姓名



-- 21.查询男生、女生人数



-- 22.查询名字中含有「风」字的学生信息



-- 23.查询同名同性学生名单，并统计同名人数



-- 24.查询 1990 年出生的学生名单



-- 25.查询每门课程的平均成绩，结果按平均成绩降序排列，平均成绩相同时，按课程编号升序排列



-- 26.查询平均成绩大于等于 85 的所有学生的学号、姓名和平均成绩



-- 27.查询课程名称为「数学」，且分数低于 60 的学生姓名和分数



-- 28.查询所有学生的课程及分数情况（存在学生没成绩，没选课的情况）



-- 29.查询任何一门课程成绩在 70 分以上的姓名、课程名称和分数



-- 30.查询不及格的课程



-- 31.查询课程编号为 01 且课程成绩在 80 分以上的学生的学号和姓名



-- 32.求每门课程的学生人数



-- 33.成绩不重复，查询选修「张三」老师所授课程的学生中，成绩最高的学生信息及其成绩



--34.成绩有重复的情况下，查询选修「张三」老师所授课程的学生中，成绩最高的学生信息及其成绩



-- 35.查询不同课程成绩相同的学生的学生编号、课程编号、学生成绩



-- 36.查询每门功成绩最好的前两名



-- 37.统计每门课程的学生选修人数（超过 5 人的课程才统计）。



-- 38.检索至少选修两门课程的学生学号



-- 39.查询选修了全部课程的学生信息



-- 40.查询各学生的年龄，只按年份来算



-- 41.按照出生日期来算，当前月日 < 出生年月的月日则，年龄减一



-- 42.查询本周过生日的学生



-- 43.查询下周过生日的学生



-- 44.查询本月过生日的学生



-- 45.查询下月过生日的学生
