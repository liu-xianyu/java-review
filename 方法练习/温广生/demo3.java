package less;

public class demo3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		- 需求：设计一个方法，该方法能够同时获取数组的最大值，和最小值
//
//		- 注意: return语句, 只能带回一个结果.
//
//		- 提示:返回一个数组，数组里第一个元素是最大值，第二个元素是最小值
		int[] p = fangfa();
		System.out.println("最大值"+p[0]);
		System.out.println("最小值"+p[1]);
	}

	public static int[] fangfa() {
		int[] d = new int[] { 99, 100, 65, 75, 80, 94, 92 };
		int max;
		max = d[0];
		for (int i = 0; i < d.length; i++) {
			if (max < d[i]) {
				max = d[i];
			}
		}
		int min;
		min = d[0];
		for (int i = 0; i < d.length; i++) {
			if (min > d[i]) {
				min = d[i];
			}
		}
		int e[] = new int[2];
		e[0] = max;
		e[1] = min;
		return e;

	}

}
