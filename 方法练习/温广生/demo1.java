package less;

import java.util.Scanner;

public class demo1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//* 需求：设计一个方法用于获取数组中元素的最大值 
		//
		//* 思路：
		
		//* ①定义一个数组，用静态初始化完成数组元素初始化
		//* ②定义一个方法，用来获取数组中的最大值，最值的认知和讲解我们在数组中已经讲解过了
		//* ③调用获取最大值方法，用变量接收返回结果
		//* ④把结果输出在控制台
		//////////////
		Scanner scan = new Scanner(System.in);
		
		int c=fangfa();
		System.out.println(c);
	}

	public static int fangfa() {
		int [] d=new int [] {99,100,65,75,80,94,92};
		int max;
		max=d[0];
		for (int i = 0; i < d.length; i++) {
			if (max<d[i]) {
				max=d[i];
			}
		}
		return max;
	}
}
