package lession2;

public class Demo3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//- 需求：设计一个方法，该方法能够同时获取数组的最大值，和最小值

		//- 注意: return语句, 只能带回一个结果.

		//- 提示:返回一个数组，数组里第一个元素是最大值，第二个元素是最小值
		int [] arr = {10,20,40,69,91};
        int  [] arr1= num(arr);
        System.out.println("最大值"+arr1[0]+"最小值"+arr1[1]);
	}

	private static int[] num(int[] arr) {
		// TODO Auto-generated method stub
		int arr1 []=new int [2];
		int max=arr[0];
		int min=arr[4];
		for (int i = 0; i < arr.length; i++) {
			if (arr[i]>max) {
				max=arr[i];
			}
			if (arr[i]<min) {
				min=arr[i];
			}
			
		}
		arr1[0]=max;
		arr1[1]=min;
		return arr1;
	} 

}
