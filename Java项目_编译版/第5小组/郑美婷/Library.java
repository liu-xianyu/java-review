import java.lang.invoke.SwitchPoint;

import java.util.Iterator;

import java.util.Scanner;



public class Library {

	static Scanner xx = new Scanner(System.in);

	static String users[][] = new String[100][4];

	static String books[][] = new String[100][5];

	static String pubs[][] = new String[100][3];

	static String loginUser = "";



//主方法

	public static void main(String[] args) {

		inner();

		Homepage();

	}



//图书馆存储信息

	private static void inner() {

		users[0][0] = "设计部";

		users[0][1] = "coco";

		users[0][2] = "123";

		users[0][3] = "管理员";

		users[1][0] = "项目部";

		users[1][1] = "一点点";

		users[1][2] = "321";

		users[1][3] = "普通员工";



		books[0][0] = "a1";

		books[0][1] = "童年";

		books[0][2] = "36";

		books[0][3] = "童年出版社";

		books[0][4] = "高尔基";

		books[1][0] = "a2";

		books[1][1] = "水浒传";

		books[1][2] = "108";

		books[1][3] = "好汉出版社";

		books[1][4] = "施耐庵";

		books[2][0] = "a3";

		books[2][1] = "骆驼祥子";

		books[2][2] = "45";

		books[2][3] = "老酒出版社";

		books[2][4] = "老舍";

		books[3][0] = "a4";

		books[3][1] = "祥瑞";

		books[3][2] = "20";

		books[3][3] = "瑞兽出版社";

		books[3][4] = "老舍";



		pubs[0][0] = "童年出版社";

		pubs[0][1] = "北京";

		pubs[0][2] = "乔一";

		pubs[1][0] = "好汉出版社";

		pubs[1][1] = "上海";

		pubs[1][2] = "陈二";

		pubs[2][0] = "老酒出版社";

		pubs[2][1] = "吉林";

		pubs[2][2] = "吕三";

	}



//图书馆选择登录系统

	private static void Homepage() {

		System.out.println("欢迎来到闽大系统图书馆");

		while (true) {

			System.out.println("请选择输入：1.注册  2.登录");

			int key = xx.nextInt();

			switch (key) {

			case 1:

				writer();

				break;

			case 2:

				login();

				break;

			default:

				System.out.println("系统退出！");

				System.exit(0);

				break;

			}

		}

	}



//图书馆登录界面

	private static void login() {

		System.out.println("欢迎来到登录界面~");

		System.out.println("请输入用户名：");

		String username = xx.next();

		System.out.println("请输入密码：");

		String password = xx.next();

		boolean flat = false;

		for (int i = 0; i < users.length; i++) {

			if (username.equals(users[i][1]) && password.equals(users[i][2])) {

				flat = true;

				loginUser = username;

				break;

			}

		}

		if (flat) {

			System.out.println(loginUser + ",恭喜你登陆成功！");

			System.out.println("*****登录界面结束*****");

			librarygl();

		} else {

			System.out.println("该用户不存在或者密码错误！请重新登录！");

		}

	}



//图书馆注册系统

	private static void writer() {

		System.out.println("欢迎来到注册界面~");

		int wrindex = -1;

		for (int i = 0; i < users.length; i++) {

			if (users[i][0] == null) {

				wrindex = i;

				break;

			}

		}

		System.out.println("请输入部门：");

		users[wrindex][0] = xx.next();

		System.out.println("请输入用户名：");

		users[wrindex][1] = xx.next();

		System.out.println("请输入密码：");

		users[wrindex][2] = xx.next();

		users[wrindex][3] = "普通员工";

		System.out.println("注册成功！");

		System.out.println("*****注册界面结束*****");

		Homepage();

	}



//图书馆管理系统

	private static void librarygl() {

		System.out.println(loginUser+",欢迎您使用闽大书籍管理系统！！！");
		
		System.out.println("请输入数字进行选择:1.图书管理  2.出版社管理  3.退出登录  4.退出系统");

		int order = xx.nextInt();

		switch (order) {

		case 1:

			// 管理
			System.out.println("******欢迎来到图书馆管理菜单******");
			gl();

			break;

		case 2:

			// 出版社管理

			pubgl();

			break;

		case 3:

			// 退出登录

			loginUser = " ";

			System.out.println("退出登录成功！");

			Homepage();

			break;

		case 4:

			// 退出系统

			System.out.println("退出系统成功");

			System.exit(0);

			break;

		default:

			Homepage();

			break;

		}

	}



// 图书馆管理菜单

	private static void gl() {


		System.out.println("请选择输入管理项目：1.增加  2.删除  3.更新  4.查询 5.返回上一级菜单");

		int glxz = xx.nextInt();

		switch (glxz) {

		case 1:

			gladd();

			break;

		case 2:

			gldelect();

			break;

		case 3:

			glnew();

			break;

		case 4:

			gllookfor();

			break;
		case 5:

			librarygl();

			break;

		default:

			librarygl();

			break;

		}

	}



// 图书出版社管理系统

	private static void pubgl() {

//		5.出版社管理菜单：
		System.out.println("请输入数字进行选择：1.增加 2.删除 3.更新 4.根据出版社名称查询 5.查询所有出版社 6.返回上一级菜单");
		int pubxz = xx.nextInt();

		switch (pubxz) {

		case 1:

			pubadd();

			break;

		case 2:

			pubdelect();

			break;

		case 3:

			pubnew();

			break;

		case 4:

			pubname();

			break;

		case 5:

			puball();

			break;

		case 6:

//			6)返回上一级菜单

			librarygl();

			break;

		default:

			librarygl();

			break;

		}

	}



	private static void puball() {

		System.out.println("出版社名称\t\t地址\t\t联系人");

		for (int i = 0; i < pubs[i].length; i++) {

			for (int j = 0; j < pubs[j].length; j++) {

				if (pubs[i][0] != null) {

					System.out.print(pubs[i][j] + "\t\t");

				}

			}

			System.out.println();



		}

		pubgl();

	}



//	4)根据出版社名称查询

	private static void pubname() {

		System.out.println("请输入出版社名称：");

		String pubname = xx.next();

		

		for (int i = 0; i < pubs.length; i++) {

			if (pubs[i][0] != null && pubs[i][1].indexOf(pubname) != -1) {
				System.out.println("出版社名称\t地址\t联系人");
				System.out.println(pubs[i][0] + "\t" + pubs[i][1] + "\t" + pubs[i][2]);

				break;

			}

			System.out.println("出版社名称错误，出版社信息不存在");

			break;



		}

		System.out.println();

		pubgl();

	}



//	3)更新 

	private static void pubnew() {

		int pubc = -1;

		boolean pubcc = false;

		System.out.println("请输入出版社名称：");

		String pubccc = xx.next();

		for (int i = 0; i < books.length; i++) {

			if (pubccc.equals(books[i][0])) {

				pubc = i;

				pubcc = true;

				break;

			}

			System.out.println("该出版社不存在，更新失败");

			break;

		}

		if (pubcc) {

			books[pubc][0] = pubccc;

			System.out.println("请输入出版社名称");

			books[pubc][1] = xx.next();

			System.out.println("请输入地址：");

			books[pubc][2] = xx.next();

			System.out.println("请输入联系人：");

			books[pubc][3] = xx.next();

			System.out.println("出版社信息更新成功！");

		}

		librarygl();

	}



//	2)删除：出版社有关联的图书不能删除；

	private static void pubdelect() {

		int pubb = -1;

		boolean pubbb = false;

		System.out.println("请输入出版社名称：");

		String pubbbb = xx.next();

		for (int i = 0; i < pubs.length; i++) {

			if (pubs[i][0].equals(pubbbb)) {

				pubbb = true;

				pubb = i;

				break;

			}

			System.out.println("删除失败");
			gl();
			break;

		}

		for (int j = 0; j < pubs.length; j++) {

			if (pubbb) {

				books[pubb][j] = null;

				System.out.println("删除成功");

				break;

			}

			librarygl();

		}



	}



//	1)增加

	private static void pubadd() {

		int puba = -1;

		boolean pubaa = false;

		System.out.println("请输入出版社名称：");

		String pubaaa = xx.next();

		for (int i = 0; i < pubs.length; i++) {

			if (pubaaa.equals(pubs[i][0])) {

				System.out.println("出版社已存在");

				break;

			} else {

				pubaa = true;

				puba = i;

				break;

			}

		}

		if (pubaa) {

			books[puba][0] = pubaaa;

			System.out.println("请输入出版社名称：");

			books[puba][1] = xx.next();

			System.out.println("请输入地址：");

			books[puba][2] = xx.next();

			System.out.println("请输入联系人：");

			books[puba][3] = xx.next();

			System.out.println("新增出版社信息成功！");

		}

		librarygl();

	}



//	1)增加：图书编码唯一，如果新增的图书信息编码已存在，提示“图书已存在”；不存在才进行新增书籍信息；

	private static void gladd() {

		int glh = -1;

		boolean a = false;

		System.out.println("请输入书名：");

		String hao = xx.next();

		for (int i = 0; i < books.length; i++) {

			if (hao.equals(books[i][1])) {

				System.out.println("图书已存在");

				break;

			} else {

				a = true;

				glh = i;

				break;

			}

		}

		if (a) {

			books[glh][0] = hao;

			System.out.println("请输入书名：");

			books[glh][1] = xx.next();

			System.out.println("请输入价格：");

			books[glh][2] = xx.next();

			System.out.println("请输入出版社：");

			books[glh][3] = xx.next();

			System.out.println("请输入作者：");

			books[glh][4] = xx.next();

			System.out.println("新增书籍信息成功！");

		}

		gl();

	}



//	2)删除：根据ISBN编码删除图书；

	private static void gldelect() {

		int de = -1;

		boolean b = false;

		System.out.println("请输入书籍编号：");

		String hao1 = xx.next();

		for (int i = 0; i < books.length; i++) {

			if (books[i][0].equals(hao1)) {

				b = true;

				de = i;

				break;

			}
			System.out.println("没有找到该书！");
			System.out.println("删除失败");
			gl();
			break;

		}

		for (int j = 0; j < books.length; j++) {

			if (b) {

				books[de][j] = null;

				System.out.println("删除成功");
				gl();
				break;

			}

		}

	}



//	3)更新：ISBN唯一不能更改，根据ISBN更新相应的书籍信息；

	private static void glnew() {

		int ne = -1;

		boolean c = false;

		System.out.println("请输入书籍编号：");

		String hao2 = xx.next();

		for (int i = 0; i < books.length; i++) {

			if (hao2.equals(books[i][0])) {

				ne = i;

				c = true;

				break;

			}

			System.out.println("该书籍编号不存在，更新失败");
			gl();
			break;

		}

		if (c) {

			books[ne][0] = hao2;

			System.out.println("请输入书名");

			books[ne][1] = xx.next();

			System.out.println("请输入价格：");

			books[ne][2] = xx.next();

			System.out.println("请输入出版社：");

			books[ne][3] = xx.next();

			System.out.println("请输入作者：");

			books[ne][4] = xx.next();

			System.out.println("书籍信息更新成功！");

		}

		gl();

	}



//	4)查询菜单：

	private static void gllookfor() {

		System.out.println("请选择输入查询方式：1.ISBN查询 2.书名查询（模糊）3.出版社查询 4.作者查询 5.价格范围查询 6.所有书籍信息 7.返回上一级菜单");

		int lk = xx.nextInt();

		switch (lk) {

		case 1:

			System.out.println("请输入ISBN：");

			String bkisbn = xx.next();

			lkisbn(bkisbn);

			break;

		case 2:

			System.out.println("请输入书名关键字：");

			String bkname = xx.next();

			lkname(bkname);

			break;

		case 3:

			System.out.println("请输入出版社：");

			String bkpub = xx.next();

			lkpub(bkpub);

			break;

		case 4:

			System.out.println("请输入作者：");

			String bkwriter = xx.next();

			lkwriter(bkwriter);

			break;

		case 5:

			System.out.println("请输入价格范围：");

			System.out.println("最高价：");

			Double bkpricemax = xx.nextDouble();

			System.out.println("最低价：");

			Double bkpricemin = xx.nextDouble();

			lkprice(bkpricemax, bkpricemin);

			break;

		case 6:

			lkallbooks();

			break;

		case 7:

//			g)返回上一级菜单

			System.out.println("返回成功");

			gl();

			break;



		default:

			System.out.println("识别错误");

			break;

		}

	}



//	a)根据ISBN查询

	private static void lkisbn(String isbn) {

		for (int i = 0; i < books.length; i++) {

			if (isbn.equals(books[i][0])) {

				System.out.println("ISBN\t书名\t价格\t出版社\t作者");

				System.out.println(books[i][0] + "\t" + books[i][1] + "\t" + books[i][2] + "\t" + books[i][3] + "\t"

						+ books[i][4]);

				break;

			}

			System.out.println("编码错误，书籍信息不存在");

			break;

		}

		System.out.println();

		gllookfor();



	}



//	b)根据书名查询（模糊）

	private static void lkname(String name) {

		System.out.println("ISBN\t书名\t价格\t出版社\t作者");

		for (int i = 0; i < books.length; i++) {

			if (books[i][0] != null && books[i][1].indexOf(name) != -1) {

				System.out.println(books[i][0] + "\t" + books[i][1] + "\t" + books[i][2] + "\t" + books[i][3] + "\t"

						+ books[i][4]);

				break;

			}

			System.out.println("书名关键字错误，书籍信息不存在");

			break;



		}

		System.out.println();

		gllookfor();

	}



//		c)根据出版社查询

	private static void lkpub(String pub) {

		int c = -1;

		boolean cc = false;

		for (int i = 0; i < books.length; i++) {

			if (books[i][0] != null && books[i][3].equals(pub)) {

				cc = true;

				c = i;

			}

		}

		if (cc) {

			System.out.println("ISBN\t书名\t价格\t出版社\t作者");

			System.out.println(

					books[c][0] + "\t" + books[c][1] + "\t" + books[c][2] + "\t" + books[c][3] + "\t" + books[c][4]);

		} else {

			System.out.println("出版社错误，书籍信息不存在");

		}

		gllookfor();

	}



//	d)根据作者查询

	private static void lkwriter(String wr) {

		boolean d = false;

		int d1 = -1;

		for (int i = 0; i < books.length; i++) {

			if (books[i][0] != null && books[i][4].equals(wr)) {

				d1 = i;

				d = true;

			}



		}

		for (int i = 0; i < books.length; i++) {

			if (d) {

				System.out.println("ISBN\t书名\t价格\t出版社\t作者");

				System.out.print(books[d1][0] + "\t" + books[d1][1] + "\t" + (books[d1][2] + "") + "\t" + books[d1][3]

						+ "\t" + books[d1][4]);

				System.out.println();
				break;
			} else {

				System.out.println("作者错误，书籍信息不存在");

				break;

			}

		}

		gllookfor();

	}



//	e)根据价格范围查询

	private static void lkprice(Double prx, Double prn) {

		System.out.println("ISBN\t书名\t价格\t出版社\t作者");

		for (int i = 0; i < books.length - i; i++) {

			if (books[i][0] != null

					&& (prx >= Double.parseDouble(books[i][2]) && Double.parseDouble(books[i][2]) >= prn)) {

				System.out.print(books[i][0] + "\t" + books[i][1] + "\t" + (books[i][2] + "") + "\t" + books[i][3]

						+ "\t" + books[i][4]);

			

			System.out.println();

		}
		else {
			
			break;
		}
		}System.out.println("没有找到书籍信息");
		gllookfor();

	}



//	f)查询所有书籍信息

	private static void lkallbooks() {

		System.out.println("ISBN\t\t书名\t\t价格\t\t出版社\t\t作者");

		for (int i = 0; i < books[i].length; i++) {

			for (int j = 0; j < books[j].length; j++) {

				if (books[i][0] != null) {

					System.out.print(books[i][j] + "\t\t");

				}

			}

			System.out.println();



		}

		gllookfor();

	}

}

