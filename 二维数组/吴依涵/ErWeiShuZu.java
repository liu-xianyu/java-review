import java.util.Scanner;
public class ErWeiShuZu {
//	接收并输出某公司本月的考勤和扣款信息，
//	假设公司有5个员工， 每个员工的考勤项有上下班忘打卡、 迟到、 早退、 旷工， 
//	其中上下班忘打卡扣款10元/次， 迟到和早退扣款为20元/次， 旷工100元/天， 
//	参考图如下(参考图中是以3个员工为例)： 
	public static void main(String[] args) {
		int [][] f = new int [5][6];
    Scanner sc = new Scanner(System.in);
	
	for (int i = 0; i < f.length; i++) {
		System.out.println("请输入第"+(i+1)+"个员工的编号：");
		f[i][0] = sc.nextInt();
	
        System.out.println("请输入第"+(i+1)+"个员工的忘打卡次数：");
        f[i][1] = sc.nextInt();
        
        System.out.println("请输入第"+(i+1)+"个员工的迟到次数：");
        f[i][2] = sc.nextInt();

        System.out.println("请输入第"+(i+1)+"个员工的早退次数：");
        f[i][3] = sc.nextInt();
        
        System.out.println("请输入第"+(i+1)+"个员工的旷工次数：");
        f[i][4] = sc.nextInt();
        
        f[i][5] = f[i][1]*10 + f[i][2]*20 + f[i][3]*20 + f[i][4]*100;       
	}
	    System.out.println("***************本月考勤信息**********************");
	    System.out.println("员工编号\t忘打卡\t迟到次数\t早退次数\t旷工次数\t总罚款(单位元)");
	for (int i = 0; i < f.length; i++) {
		for (int j = 0; j < f[i].length; j++) {
			System.out.print(f[i][j]+"\t");
		}
		System.out.println();
	}
}
}