import java.util.Scanner;
public class ErWeiShuZu2 {
//1.一家贸易公司有四位销售员，每位销售员负责销售四件商品，编写一个程序，接收每名销售员销售的各类产品的数量。
//  打印产品销售明细表，明细表包括每类产品的销售总数，以及每位销售员销售的产品数量占总销售的百分比。
//	使用以下公式：
//	销售员N销售的产品A的百分比=（销售员N售出的产品A的销售量/总销售量）*100
//	总销售量指各类产品销售量的总和（使用二维数组）
	public static void main(String[] args) {
		int [][] f = new int [5][6];
    Scanner sc = new Scanner(System.in);
	
	for (int i = 0; i < f.length; i++) {
		System.out.println("请输入第"+(i+1)+"个员工的编号：");
		f[i][0] = sc.nextInt();
	
        System.out.println("请输入第"+(i+1)+"个员工的忘打卡次数：");
        f[i][1] = sc.nextInt();
        
        System.out.println("请输入第"+(i+1)+"个员工的迟到次数：");
        f[i][2] = sc.nextInt();

        System.out.println("请输入第"+(i+1)+"个员工的早退次数：");
        f[i][3] = sc.nextInt();
        
        System.out.println("请输入第"+(i+1)+"个员工的旷工次数：");
        f[i][4] = sc.nextInt();
        
        f[i][5] = f[i][1]*10 + f[i][2]*20 + f[i][3]*20 + f[i][4]*100;       
	}
	    System.out.println("***************本月考勤信息**********************");
	    System.out.println("员工编号\t忘打卡\t迟到次数\t早退次数\t旷工次数\t总罚款(单位元)");
	for (int i = 0; i < f.length; i++) {
		for (int j = 0; j < f[i].length; j++) {
			System.out.print(f[i][j]+"\t");
		}
		System.out.println();
	}
}
}
