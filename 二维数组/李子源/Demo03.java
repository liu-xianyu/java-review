import java.util.Scanner;

public class Demo03 {

	public static void main(String[] args) {
		Scanner abc = new Scanner(System.in);

		int[][] arr = new int[5][5];
//		int [] asd = new int [5];

		for (int i = 0; i < arr.length; i++) {
			int s = i+1;
			for (int j = 0; j < 1; j++) {
				System.out.println("请输入第" + s + "个员工的编号：");
				int a = abc.nextInt();
				arr[i][0] = a;
				System.out.println("请输入第" + s + "个员工的忘打卡次数：");
				int b = abc.nextInt();
				arr[i][1] = b;
				System.out.println("请输入第" + s + "个员工的迟到次数：");
				int c = abc.nextInt();
				arr[i][2] = c;
				System.out.println("请输入第" + s + "个员工的早退次数：");
				int d = abc.nextInt();
				arr[i][3] = d;
				System.out.println("请输入第" + s + "个员工的旷工次数：");
				int e = abc.nextInt();
				arr[i][4] = e;
			}
		}


		System.out.println("***********本月考勤信息**********");
		System.out.print("员工编号" + "     " + "忘打卡" + "     " + "迟到" + "     " + "早退" + "       " + "旷工" + "     " + "总罚款(单位元)");
		System.out.println();
		for (int i = 0; i < arr.length; i++) {
			int sum = arr[i][1] * 10 + arr[i][2] * 20 + arr[i][3] * 20 + arr[i][4] * 100;
			System.out.print("  "+arr[i][0] + "     " 
			+ arr[i][1] + "    " + arr[i][2] + "    " 
			+ arr[i][3] + "    "+ arr[i][4] + "    "+sum);
			System.out.println();
		}
	}

}
