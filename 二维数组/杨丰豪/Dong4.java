import java.util.Iterator;
import java.util.Scanner;

public class Dong4 {

	public static void main(String[] args) {
//		一家贸易公司有四位销售员，每位销售员负责销售四件商品，编写一个程序，
//		接收每名销售员销售的各类产品的数量。
//		打印产品销售明细表，明细表包括每类产品的销售总数，以及每位销售员销售的产品数量占总销售的百分比。
//		使用以下公式：
//		销售员N销售的产品A的百分比=（销售员N售出的产品A的销售量/总销售量）*100
//		总销售量指各类产品销售量的总和（使用二维数组）
		Scanner sc = new Scanner(System.in);
		int a[][] = new int[4][5];
		double sum=0;
		for (int i = 0; i < a.length; i++) {
        System.out.println("请输入第" + (i+1) + "个销售员的编号：");
        a[i][0] = sc.nextInt();
        System.out.println("请输入第" + (i+1) + "个销售员的A种类产品的销售量：");
        a[i][1] = sc.nextInt();
        System.out.println("请输入第" + (i+1) + "个销售员的B种类产品的销售量：");
        a[i][2] = sc.nextInt();
        System.out.println("请输入第" + (i+1) + "个销售员的C种类产品的销售量：");
        a[i][3] = sc.nextInt();
        System.out.println("请输入第" + (i+1) + "个销售员的D种类产品的销售量：");
        a[i][4] = sc.nextInt();
   
		}
		
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				sum=sum+a[i][j];
			}
		}
		System.out.println("***********本月考勤信息***********");
		System.out.print("销售员编号" + "  " + "A种类产品的百分比" + "  " + "B种类产品的百分比" + "  " + "C种类产品的百分比" + "  " + "D种类产品的百分比" + "  " + "销售总量");
		System.out.println();
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i][0]);
			System.out.print("  ");
			
			System.out.print(a[i][1]/sum);
			System.out.print("  ");
			
			System.out.print(a[i][2]/sum);
			System.out.print("  ");
			
			System.out.print(a[i][3]/sum);
			System.out.print("  ");
			
			System.out.print(a[i][4]/sum);
			System.out.print("  ");
			
			System.out.println(sum);
		}
		
		
	}
}
