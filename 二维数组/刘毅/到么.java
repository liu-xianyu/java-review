package 练习;

import java.util.Scanner;

public class woke{

	public static void main(String[] args) {
		

		

			
				// 1.	一家贸易公司有四位销售员，每位销售员负责销售四件商品，
//				编写一个程序，接收每名销售员销售的各类产品的数量。打印产品销售明细表，
//				明细表包括每类产品的销售总数，以及每位销售员销售的产品数量占总销售的百分比。
//				使用以下公式：
//				销售员N销售的产品A的百分比=（销售员N售出的产品A的销售量/总销售量）*100
//				总销售量指各类产品销售量的总和（使用二维数组）
				Scanner scan = new Scanner(System.in);
				
				double [][] arr = new double [4][5];
				
				for (int i = 0; i < arr.length; i++) {
					System.out.println("请输入第"+(i + 1)+"位员工的编号：");
					arr[i][0] = scan.nextInt();
					
					System.out.println("请输入第"+(i + 1)+"位员工A商品的销售额 ：");
					arr[i][1] = scan.nextInt();
					
					System.out.println("请输入第"+(i + 1)+"位员工B商品的销售额 ：");
					arr[i][2] = scan.nextInt();
					
					System.out.println("请输入第"+(i + 1)+"位员工C商品的销售额 ：");
					arr[i][3] = scan.nextInt();
					
					System.out.println("请输入第"+(i + 1)+"位员工D商品的销售额 ：");
					arr[i][4] = scan.nextInt();
				}
				
				double Asum = 0;
				double  Bsum = 0;
				double  Csum = 0;
				double  Dsum = 0;
				
				for (int i = 0; i < arr.length; i++) {
					Asum = arr[i][1] + Asum;
					Bsum = arr[i][2] + Bsum;
					Csum = arr[i][3] + Csum;
					Dsum = arr[i][4] + Dsum;
				}
				
				System.out.println("****************产品销售明细表***************");
				System.out.println("员工编号"+"\t"
									+"商品A销售额"+"\t"
									+"商品B销售额"+"\t"
									+ "商品C销售额"+"\t"
									+"商品D销售额"+"\t"
									+"商品A总销售额"+"\t"
									+"商品A占总销售额的百分比"+"\t"
									+"商品B总销售额"+"	"
									+"商品B占总销售额的百分比"+"\t"
									+"商品C总销售额"+"	"
									+"商品C占总销售额的百分比"+"\t"
									+"商品D总销售额"+"	"
									+"商品D占总销售额的百分比"+"\t");
				System.out.println();
				
				for (int i = 0; i < arr.length; i++) {
					System.out.print(arr[i][0]);
					System.out.print("\t");
					
					System.out.print(arr[i][1]);
					System.out.print("\t");
					
					System.out.print(arr[i][2]);
					System.out.print("\t");
					
					System.out.print(arr[i][3]);
					System.out.print("\t");
					
					System.out.print(arr[i][4]);
					System.out.print("\t");
					
					System.out.print(Asum);
					System.out.print("\t");
					
					System.out.print((arr[i][1]/Asum)*100);
					System.out.print("\t");
					
					System.out.print(Bsum);
					System.out.print("\t");
					
					System.out.print((arr[i][2]/Bsum)*100);
					System.out.print("\t");
					
					System.out.print(Csum);
					System.out.print("\t");
					
					System.out.print((arr[i][3]/Csum)*100);
					System.out.print("\t");
					
					System.out.print(Dsum);
					System.out.print("\t");
					
					System.out.print((arr[i][4]/Dsum)*100);
					System.out.print("\t");
					
					System.out.println();
				
		
	

	}

}}