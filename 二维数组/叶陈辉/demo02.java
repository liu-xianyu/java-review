package demo;

import java.util.Scanner;

public class demo02 {

	public static void main(String[] args) {
		// 考勤情况
		int[][]arr= new int[5][6];
		Scanner scan=new Scanner(System.in);
		for (int i = 0; i < arr.length; i++) {
			System.out.println("请输入第"+(i+1)+"个员工的编号");
				arr[i][0]=scan.nextInt();
			System.out.println("请输入第"+(i+1)+"个员工的忘打卡次数");
				arr[i][1]=scan.nextInt();
			System.out.println("请输入第"+(i+1)+"个员工的迟到次数");
				arr[i][2]=scan.nextInt();
			System.out.println("请输入第"+(i+1)+"个员工的早退次数");
				arr[i][3]=scan.nextInt();
			System.out.println("请输入第"+(i+1)+"个员工的旷工次数");
				arr[i][4]=scan.nextInt();
			
			arr[i][5]=arr[i][1]*10+arr[i][2]*20+arr[i][3]*20+arr[i][4]*100;
		}
		System.out.println("*******本月考勤信息*******");
		System.out.println("员工编号\t忘打卡\t迟到次数\t早退次数\t旷工次数\t总罚款");
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j]+"\t");
			}
			System.out.println();
		}
			
	}

}
