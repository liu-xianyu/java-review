import java.util.Scanner;

public class KaoQin {

	public static void main(String[] args) {
		// 接收并输出某公司本月的考勤和扣款信息， 假设公司有5个员工，
		//每个员工的考勤项有上下班忘打卡、 迟到、 早退、 旷工，
		//其中上下班忘打卡扣款10元/次， 迟到和早退扣款为20元/次， 旷工100元/天
		Scanner sc=new Scanner(System.in);
		int [][]arr=new int [5][5];
		int[] sum=new int[5];
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length;j++) {
				System.out.println("请输入第"+(i+1)+"个员工编号");
				arr[i][j]=sc.nextInt();
				j++;
				System.out.println("请输入第"+(i+1)+"个员工忘打卡数");
				arr[i][j]=sc.nextInt();
				j++;
				System.out.println("请输入第"+(i+1)+"个员工迟到数");
				arr[i][j]=sc.nextInt();
				j++;
				System.out.println("请输入第"+(i+1)+"个员工早退数");
				arr[i][j]=sc.nextInt();
				j++;
				System.out.println("请输入第"+(i+1)+"个员工旷工数");
				arr[i][j]=sc.nextInt();
				j++;
				sum[i]=(arr[i][1]*10)+((arr[i][2]+arr[i][3])*20)+(arr[i][4]*100);
			}
		}
		System.out.println("员工编号"+"\t"+"忘打卡"+"\t"+"迟到"+"\t"+"早退"+"\t"+"旷工"+"\t"+"总金额");
		for (int i = 0; i < sum.length; i++) {
			for (int j = 0; j < sum.length; j++) {
				System.out.print(arr[i][j]+"\t");
			}
			System.out.println(sum[i]);
		}	
	}

}
