import java.util.Scanner;

//一家贸易公司有四位销售员，每位销售员负责销售四件商品，编写一个程序，
//接收每名销售员销售的各类产品的数量。打印产品销售明细表，
//明细表包括每类产品的销售总数，以及每位销售员销售的产品数量占总销售的百分比。
//使用以下公式：
//销售员N销售的产品A的百分比=（销售员N售出的产品A的销售量/总销售量）*100
//总销售量指各类产品销售量的总和（使用二维数组）

public class 二维数组题 {
	public static void main(String[] args) {
		double arr[][] = new double[5][6];
		double sum = 0;
		Scanner input = new Scanner(System.in);
		for (int i = 0; i < arr.length; i++) {
			System.out.println("请输入第" + (i+1)+"位售货员编号：");
			arr[i][0] = input.nextInt();
			System.out.println("请输入第"+(i+1)+"位售货员的产品A：");
			arr[i][1] = input.nextInt();
			System.out.println("请输入第"+(i+1)+"位售货员的产品B：");
			arr[i][2] = input.nextInt();
			System.out.println("请输入第"+(i+1)+"位售货员的产品C：");
			arr[i][3] = input.nextInt();
			System.out.println("请输入第"+(i+1)+"位售货员的产品D：");
			arr[i][4] = input.nextDouble() ;
			sum = arr[i][i+1] + sum;
		}
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				sum = arr[i][j] + sum;
			}
		}
		System.out.println("************************"+"本月考勤信息"+"************************");
		System.out.println("销售原编号"+" "
				+"第一个商品"+" "+"总销售的百分比"+"  "
				+"第二个商品"+" "+"    总销售的百分比"+"  "
				+"第三个商品"+" "+"    总销售的百分比"+"  "
				+"第四个商品"+" "+"    总销售的百分比"+"  "
				+"总销售量");
				for (int i = 0; i < arr.length; i++) {
				System.out.print(arr[i][0]+"    ");
				System.out.print(arr[i][1]+"    ");
				System.out.print(arr[i][1]/sum+"    ");
				System.out.print(arr[i][2]+"    ");
				System.out.print(arr[i][2]/sum+"    ");
				System.out.print(arr[i][3]+"    ");
				System.out.print(arr[i][3]/sum+"    ");
				System.out.print(arr[i][4]+"    ");
				System.out.print(arr[i][4]/sum+"    ");
				System.out.print(sum);
				System.out.println();
}

	}
}
