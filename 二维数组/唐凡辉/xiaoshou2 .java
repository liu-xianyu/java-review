import java.util.Scanner;

public class xiaoshou2 {

	public static void main(String[] args) {
		//一家贸易公司有四位销售员，每位销售员负责销售四件商品，编写一个程序，接收每名销售员销售的各类产品的数量。
		//打印产品销售明细表，明细表包括每类产品的销售总数，以及每位销售员销售的产品数量占总销售的百分比。
		//使用以下公式：
		//销售员N销售的产品A的百分比=（销售员N售出的产品A的销售量/总销售量）*100
		//总销售量指各类产品销售量的总和（使用二维数组）

		Scanner abc = new Scanner(System.in);

		int[][] arr = new int[4][5];
		Double sum = (double) 0;

		for (int i = 0; i < arr.length; i++) {
			int a = i+1;
			for (int j = 0; j < 1; j++) {
				System.out.println("请输入第" + a + "个销售员的编号：");
				int m = abc.nextInt();
				arr[i][0] = m;
				System.out.println("请输入第" + m + "个销售员的第一种产品的销售量：");
				int b = abc.nextInt();
				arr[i][1] = b;
				System.out.println("请输入第" + m + "个销售员的第二种产品的销售量：");
				int c = abc.nextInt();
				arr[i][2] = c;
				System.out.println("请输入第" + m + "个销售员的第三种产品的销售量：");
				int d = abc.nextInt();
				arr[i][3] = d;
				System.out.println("请输入第" + m + "个销售员的第四种产品的销售量：");
				int e = abc.nextInt();
				arr[i][4] = e;
				sum=arr[i][i+1]+sum;
			}
		}
		System.out.println("***********本月考勤信息***********");
		System.out.print("销售员编号" + "     " + "第一种产品的销售量" + "     " + "第二种产品的销售量" + "     " + "第三种产品的销售量" + "       " + "第四种产品的销售量" + "     " + "销售的产品数量占总销售的百分比");
		System.out.println();
		for (int i = 0; i < arr.length; i++) {
			
			System.out.print("   "+arr[i][0] + "          " 
			+ arr[i][1] + "              " + arr[i][2] + "            " 
			+ arr[i][3] + "            " + arr[i][4] + "              "
			+((arr[i][1]+arr[i][2]+arr[i][3]+arr[i][4])/sum));
			System.out.println();

		}
	}
}
