package lession;

import java.util.Scanner;

public class Demo3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int arr1[] = new int[5];// 员工编号
		int arr2[] = new int[5];// 忘打卡
		int arr3[] = new int[5];// 迟到
		int arr4[] = new int[5];// 早退
		int arr5[] = new int[5];// 旷工
		int sum[] = new int[5];// 罚款
		Scanner scan = new Scanner(System.in);
		for (int i = 0; i < 5; i++) {
			System.out.println("请输入第" + (i + 1) + "个员工编号：");
			arr1[i] = scan.nextInt();
			System.out.println("请输入第" + (i + 1) + "个员工忘打卡次数：");
			arr2[i] = scan.nextInt();
			System.out.println("请输入第" + (i + 1) + "个员工迟到次数：");
			arr3[i] = scan.nextInt();
			System.out.println("请输入第" + (i + 1) + "个员工早退次数：");
			arr4[i] = scan.nextInt();
			System.out.println("请输入第" + (i + 1) + "个员工旷工次数：");
			arr5[i] = scan.nextInt();
			sum[i] = arr2[i] * 10 + (arr3[i] + arr4[i]) * 20 + arr5[i] * 100;
		}
		System.out.println("本月考勤信息");
		System.out.println("员工编号\t  忘打卡\t   迟到\t  早退\t  旷工\t  总罚款（单位元）");
		for (int i = 0; i < arr5.length; i++) {
			System.out.println(
			arr1[i] + "\t" + arr2[i] + "\t" + arr3[i] + "\t" + arr4[i] + "\t" + arr5[i] + "\t" + sum[i]);

		}

	}
}
