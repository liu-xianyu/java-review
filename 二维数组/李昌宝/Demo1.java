import java.util.Random;
import java.util.Scanner;

public class Demo1 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int arr[][] = new int[5][6];

		for (int i = 0; i < arr.length; i++) {

			System.out.println("请输入第" + (i + 1) + "个员工的编号");
			arr[i][0] = scanner.nextInt();

			System.out.println("请输入第" + (i + 1) + "个员工忘打卡数");
			arr[i][1] = scanner.nextInt();

			System.out.println("请输入第" + (i + 1) + "个员工迟到数");
			arr[i][2] = scanner.nextInt();

			System.out.println("请输入第" + (i + 1) + "个员工早退数");
			arr[i][3] = scanner.nextInt();

			System.out.println("请输入第" + (i + 1) + "个员工矿工数");
			arr[i][4] = scanner.nextInt();
			
			arr[i][5] = (arr[i][1] * 10 + arr[i][2] * 20 + arr[i][3] * 20 + arr[i][4] * 100);
		}
		System.out.println("*******************本月考情信息**********************");
		System.out.println("员工编号\t忘打卡\t迟到\t早退\t旷工\t总罚款(单位元)");
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr.length; j++) {
				System.out.print(arr[i][j] + "\t");
			}
			System.out.println(arr[i][5]);
			System.out.println();
		}
	}
}
