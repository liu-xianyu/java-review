package shuzu.com;
import java.util.Scanner;

public class Demoo4 {
		//一家贸易公司有四位销售员，每位销售员负责销售四件商品，编写一个程序
	public static void main(String[] args) {
		//明细表包括每类产品的销售总数，以及每位销售员销售的产品数量占总销售的百分比。
		//销售员N销售的产品A的百分比=（销售员N售出的产品A的销售量/总销售量）*100
		//总销售量指各类产品销售量的总和（使用二维数组）
		Scanner scanner = new Scanner(System.in);
		double [][] arr= new double[4][7]; 
		for (int i = 0; i < arr.length; i++) {
			System.out.println("输入第"+(i+1)+"位员工编号");
			arr[i][0]=scanner.nextInt();
			System.out.println("输入第"+(i+1)+"位员工A产品的销售数量");
			arr[i][1]=scanner.nextInt();
			System.out.println("输入第"+(i+1)+"位员工B产品的销售数量");
			arr[i][2]=scanner.nextInt();
			System.out.println("输入第"+(i+1)+"位员工C产品的销售数量");
			arr[i][3]=scanner.nextInt();
			System.out.println("输入第"+(i+1)+"位员工D产品的销售数量");
			arr[i][4]=scanner.nextInt();
			
			arr[i][5]=arr[i][1]+arr[i][2]+arr[i][3]+arr[i][4];
			arr[i][6]=arr[i][1]/(arr[i][1]+arr[i][2]+arr[i][3]+arr[i][4])*100;
		}
		System.out.println("*******************商品明细**********************");
		System.out.println("员工编号\t A产品数量\tB产品数量\tC产品数量\tD产品数量\t销售总数\tA物品百分比");
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j]+"\t"+"\t");		
			}
			System.out.println();
		}

	}

}
