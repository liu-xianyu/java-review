import java.util.Scanner;

public class Demo01 {

	public static void main(String[] args) {
//		1.	一家贸易公司有四位销售员，每位销售员负责销售四件商品，
//		编写一个程序，接收每名销售员销售的各类产品的数量。
//		打印产品销售明细表，明细表包括每类产品的销售总数，
//		以及每位销售员销售的产品数量占总销售的百分比。
//		使用以下公式：
//		销售员N销售的产品A的百分比=（销售员N售出的产品A的销售量/总销售量）*100
//		总销售量指各类产品销售量的总和（使用二维数组）
		
		int [] [] arr= new int [4] [6]; 
		int sum=0;
		Scanner sc=new Scanner(System.in);
		
		for (int i = 0; i < arr.length; i++) {
			System.out.println("请输入第"+(i+1)+"位的员工编号");
			arr[i][0]=sc.nextInt();
			System.out.println("请输入第1件商品的销售额");
			arr[i][1]=sc.nextInt();
			System.out.println("请输入第2件商品的销售额");
			arr[i][2]=sc.nextInt();
			System.out.println("请输入第3件商品的销售额");
			arr[i][3]=sc.nextInt();
			System.out.println("请输入第4件商品的销售额");
			arr[i][4]=sc.nextInt();
			
			arr[i][5]=arr[i][1]+arr[i][2]+arr[i][3]+arr[i][4];
			sum=sum+arr[i][5];
			
			
		}
		System.out.println();
		System.out.println("****************销售明细表*****************");
		System.out.println("销售员编号\t销售总数\t百分比");
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j]+"\t");
				
			}System.out.print((double)arr[i][5]/sum*100+"%");
			System.out.println();
			
		}
		
		
		
	}

}
