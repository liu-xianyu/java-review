
public class Demo01 {

	public static void main(String[] args) {
		// ​	已知一个二维数组 arr = {{11, 22, 33}, {33, 44, 55}};
		//​	遍历该数组，取出所有元素并打印
		int[][] arr = {{11,22,33},{33,44,55}};
		
		for (int i = 0; i < arr.length; i++) {
			System.out.print("[");
			for (int j = 0; j < arr[i].length; j++) {
				if (j == arr[i].length - 1) {
					System.out.print(arr[i][j]);
				}else {
					System.out.print(arr[i][j]);
					System.out.print(",");
				}
			}
			System.out.print("]");
		}
		
		
	}

}
