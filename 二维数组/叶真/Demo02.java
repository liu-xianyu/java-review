
public class Demo02 {

	public static void main(String[] args) {
		//  某公司季度和月份统计的数据如下：单位(万元)
		//	第一季度：22,66,44
		//	第二季度：77,33,88
		//	第三季度：25,45,65
		//	第四季度：11,66,99
		
		int[][] arr = {{22,66,44},{77,33,88},{25,45,65},{11,66,99}};
		int sum = 0;
		for (int i = 0; i < arr.length; i++) {
			System.out.print("第"+(i + 1)+"季度：");
			for (int j = 0; j < arr[i].length; j++) {
				sum = arr[i][j] + sum;
				if (j == arr[i].length - 1) {
					System.out.println(arr[i][j]);
				}else {
					System.out.print(arr[i][j]);
					System.out.print(",");
				}
			}
		}
		System.out.println("总数据为："+sum);
	}

}
