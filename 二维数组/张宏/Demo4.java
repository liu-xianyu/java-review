package md.lesson01.com;

import java.util.Scanner;

public class Demo4 {

	public static void main(String[] args) {
		//1.一家贸易公司有四位销售员，每位销售员负责销售四件商品，编写一个程序，
		//接收每名销售员销售的各类产品的数量。
		//打印产品销售明细表，明细表包括每类产品的销售总数，
		//以及每位销售员销售的产品数量占总销售的百分比。
		//使用以下公式：
		//销售员N销售的产品A的百分比=（销售员N售出的产品A的销售量/总销售量）*100
		//总销售量指各类产品销售量的总和（使用二维数组）
			int [][] arr = new int [5][4];
			Scanner scanner = new Scanner(System.in);
			for (int i = 1; i < arr.length; i++) {
				for (int j = 1; j < arr[i].length; j++) {
					System.out.println("请输入第" +i + "位销售员的编号：");
					arr[i][1] = scanner.nextInt();
					System.out.println("请输入第" + j + "个商品数量");
					arr[i][2] = scanner.nextInt();
				}
				
					//arr[i][5] = (arr[i][j]/arr[i][6])*100;
					//arr[i][6] = arr[i][1]+arr[i][2]+arr[i][3]+arr[i][4];
				
				
			}
			System.out.println("*********销售表***********");
			System.out.println("销售员的销售商品数量/t商品的销售比/t销售总数");
			for (int i = 0; i < arr.length; i++) {
				for (int j = 0; j < arr[i].length; j++) {
					System.out.print(arr[i][j]+"/t");
				}
				System.out.println();
			}
		}				
	}					

		