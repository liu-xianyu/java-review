package dvfd;

import java.util.Scanner;

public class hgjk {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		接收并输出某公司本月的考勤和扣款信息， 假设公司有5个员工，
//		每个员工的考勤项有上下班忘打卡、 迟到、 早退、 旷工，
//		其中上下班忘打卡扣款10元/次， 迟到和早退扣款为20元/次，
//		旷工100元/天， 参考图如下(参考图中是以3个员工为例)： 
		Scanner sc = new Scanner(System.in);

		int[][] a = new int[5][6];

		for (int i = 0; i < a.length; i++) {
			
			System.out.println("第"+(i+1)+"员工编号");
			a[i][0] = sc.nextInt();
			System.out.println("第"+(i+1)+"忘打卡");
			a[i][1] = sc.nextInt();
			System.out.println("第"+(i+1)+"迟到");
			a[i][2] = sc.nextInt();
			System.out.println("第"+(i+1)+"早退");
			a[i][3] = sc.nextInt();
			System.out.println("第"+(i+1)+"旷工");
			a[i][4] = sc.nextInt();
			
			a[i][5] = a[i][1] * 10 + a[i][2] * 10 + a[i][3] * 20 + a[i][4] *100;
		}

		System.out.println("**************某公司本月的考勤和扣款信息*********************");
		System.out.println("员工编号\t忘打卡\t 迟到\t早退\t 旷工\t总罚款");
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				System.out.print(a[i][j] + "\t");
			}
			System.out.println();
		}

	}

}
