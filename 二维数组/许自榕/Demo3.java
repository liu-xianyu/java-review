package md.com;

import java.util.Scanner;

public class Demo3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int[][] arr = new int[5][5];
		for (int i = 0; i < arr.length; i++) {
			int b1 = i + 1;
			System.out.println("请输入第" + b1 + "个员工的编号:");
			int a = sc.nextInt();
			arr[i][0] = a;
			System.out.println("请输入第" + b1 + "个员工的忘打卡次数：");
			int b = sc.nextInt();
			arr[i][1] = b;
			System.out.println("请输入第" + b1 + "个员工的迟到次数：");
			int c = sc.nextInt();
			arr[i][2] = c;
			System.out.println("请输入第" + b1 + "个员工的早到次数：");
			int d = sc.nextInt();
			arr[i][3] = d;
			System.out.println("请输入第" + b1 + "个员工的旷工次数：");
			int e = sc.nextInt();
			arr[i][4] = e;

		}
		System.out.println("***********本月考勤信息**********");
		System.out.print("员工编号\t忘打卡\t迟到\t早到\t旷工\t总罚款(单位元)");
		System.out.println();
		for (int i = 0; i < arr.length; i++) {
			int sum = arr[i][1] * 10 + arr[i][2] * 20 + arr[i][3] * 20 + arr[i][4] * 100;
			System.out.print(arr[i][0] + "    " + arr[i][1] + "    " + arr[i][2] + "    " + arr[i][3] + "    "+ arr[i][4] + "    "+sum);
			System.out.println();
		}
	}

}
