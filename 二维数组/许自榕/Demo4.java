package md.com;

import java.util.Scanner;

public class Demo4 {
	//1.一家贸易公司有四位销售员，每位销售员负责销售四件商品，编写一个程序，接收每名销售员销售的各类产品的数量。打印产品销售明细表，明细表包括每类产品的销售总数，以及每位销售员销售的产品数量占总销售的百分比。
	//使用以下公式：
	//销售员N销售的产品A的百分比=（销售员N售出的产品A的销售量/总销售量）*100
	//总销售量指各类产品销售量的总和（使用二维数组）
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int [][]arr = new int [4][5];
		Double sum = (double) 0;
		for (int i = 0; i < arr.length; i++) {
			int b1  = i+1;
			System.out.println("请输入第" + b1 + "名销售员的第一件产品销售量");
			int a =sc.nextInt();
			arr[i][0] = a;
			System.out.println("请输入第" + b1 + "名销售员的第二件产品销售量");
			int b =sc.nextInt();
			arr[i][1] = b;
			System.out.println("请输入第" + b1 + "名销售员的第三件产品销售量");
			int c =sc.nextInt();
			arr[i][2] = c;
			System.out.println("请输入第" + b1 + "名销售员的第四件产品销售量");
			int d =sc.nextInt();
			arr[i][3] = d;
			sum=arr[i][i+1]+sum;
		}
		System.out.println("***********各类产品销售总和**********");
		System.out.print("第一件\t第二件\t第三件\t第四件\t总和");
		System.out.println();
		for (int i = 0; i < arr.length; i++) {
		System.out.print("   "+arr[i][0] + "          " 
			+ arr[i][1] + "              " + arr[i][2] + "            " 
			+ arr[i][3] + "            " 
			+((arr[i][1]+arr[i][2]+arr[i][3]+)/sum));");
			System.out.println();
		}
		
	}

}
