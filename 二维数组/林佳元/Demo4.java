import java.util.Scanner;

public class Demo4 {

	public static void main(String[] args) {
//		接收并输出某公司本月的考勤和扣款信息， 假设公司有5个员工，
//		每个员工的考勤项有上下班忘打卡、 迟到、 早退、 旷工， 其中上下班忘打卡扣款10元/次， 迟到和早退扣款为20元/次， 旷工100元/天， 
//		参考图如下(参考图中是以3个员工为例)：
		int [][] arr = new int[5][6];
		Scanner scanner = new Scanner(System.in);
		for (int i = 0; i < arr.length; i++) {
			System.out.println("请输入第"+(i+1)+"个员工的编号：");
			arr[i][0] = scanner.nextInt();
			System.out.println("请输入第"+(i+1)+"个员工的忘打卡次数：");
			arr[i][1] = scanner.nextInt();
			System.out.println("请输入第"+(i+1)+"个员工的迟到的次数：");
			arr[i][2] = scanner.nextInt();
			System.out.println("请输入第"+(i+1)+"个员工的早退的次数：");
			arr[i][3] = scanner.nextInt();
			System.out.println("请输入第"+(i+1)+"个员工的旷工的次数：");
			arr[i][4] = scanner.nextInt();
			
			arr[i][5] = arr[i][1]*10 + arr[i][2]*20 + arr[i][3]*20 + arr[i][4]*100;
		}
		System.out.println("***************本月考勤信息***************");
		System.out.println("员工编号\t忘打卡\t迟到\t早退\t旷工\t总罚款（单位元）");
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j]+"\t");
			}
			System.out.println();
		}

	}

}
