package less;

import java.util.Scanner;

public class demo4 {
	public static void main(String[] args) {
//		1.	一家贸易公司有四位销售员，每位销售员负责销售四件商品，
		//编写一个程序，接收每名销售员销售的各类产品的数量。
		//打印产品销售明细表，明细表包括每类产品的销售总数，
		//以及每位销售员销售的产品数量占总销售的百分比。
//		使用以下公式：
//		销售员N销售的产品A的百分比=（销售员N售出的产品A的销售量/总销售量）*100
//		总销售量指各类产品销售量的总和（使用二维数组）
		int [][]arr=new int [4][10];
		Scanner scan=new Scanner(System.in);
		for (int i = 0; i < arr.length; i++) {
			System.out.println("请输入第"+(i+1)+"个销售员的编号：");
			arr[i][0]=scan.nextInt();
			System.out.println("请输入a件销售商品数量");
			arr[i][1]=scan.nextInt();
			System.out.println("请输入b件销售商品数量");
			arr[i][2]=scan.nextInt();
			System.out.println("请输入c件销售商品数量");
			arr[i][3]=scan.nextInt();
			System.out.println("请输入d件销售商品数量");
			arr[i][4]=scan.nextInt();
			arr[i][5]=arr[i][1]+arr[i][2]+arr[i][3]+arr[i][4];
			arr[i][6]=(int)((double)arr[i][1]/arr[i][5])*100;//  1/5
			arr[i][7]=(int)((double)arr[i][2]/arr[i][5])*100;
			arr[i][8]=(int)((double)arr[i][3]/arr[i][5])*100;
			arr[i][9]=(int)((double)arr[i][4]/arr[i][5])*100;
			
		}
		System.out.println("**********************产品销售明细表**********************");
		System.out.println("销售员编号\t第1件\t第2件\t第3件\t第4件\t销售总数\t产品a百分比\t产品b百分比\t产品c百分比\t产品d百分比\t");
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j]+"\t");
			}
			System.out.println();
		}
	}
}
