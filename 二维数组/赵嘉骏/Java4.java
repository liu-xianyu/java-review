package xxx;

import java.util.Random;

public class Java4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	//	一家贸易公司有    四位销售员，
		//每位销售员负责销售    四件商品，
		//编写一个程序，
		//接收     每名销售员销售的各类产品的数量。
		//打印产品销售明细表，
		//明细表包括     每类产品的销售总数，
		//以及每位销售员销售的       产品数量占总销售的百分比。
		//使用以下公式：
		//销售员N销售的产品A的百分比=
		//（销售员N售出的产品A的销售量/总销售量）*100
		//总销售量指各类产品销售量的总和（使用二维数组）
		int [][] arr= new int[4][5];
		Random sc = new Random();
		for (int i = 0; i < arr.length; i++) {
			System.out.println("请输入第"+(i+1)+"位销售员的编号");
			arr[i][0] = sc.nextInt();
			System.out.println("请输入第"+(i+1)+"位销售员销售的A类产品的数量。");
			arr[i][1] = sc.nextInt();
			System.out.println("请输入第"+(i+1)+"位销售员销售的B类产品的数量。");
			arr[i][2] = sc.nextInt();
			System.out.println("请输入第"+(i+1)+"位销售员销售的C类产品的数量。");
			arr[i][3] = sc.nextInt();
			System.out.println("请输入第"+(i+1)+"位销售员销售的D类产品的数量。");
			arr[i][4] = sc.nextInt();
			arr[i][5] = arr[i][i]%(arr[i][1] + arr[i][2] + arr[i][3] + arr[i][4])*100;
		}
			System.out.println("****************产品销售明细表*******************");
			System.out.println("员工编号\t第一种产品的销售数量\t第二种产品的销售数量\t第三种产品的销售数量\t第四种产品的销售数量\t百分比");
			for (int i = 0; i < arr.length; i++) {
				for (int j = 0; j < arr.length; j++) {
					System.out.print(arr[i][j]+"\t");
				}
				System.out.println();
			}
		}

	}

}
