import java.util.Scanner;

public class Demo04 {

	public static void main(String[] args) {

//		1.	一家贸易公司有四位销售员，每位销售员负责销售四件商品，
//		编写一个程序，接收每名销售员销售的各类产品的数量。
//		打印产品销售明细表，明细表包括每类产品的销售总数，
//		以及每位销售员销售的产品数量占总销售的百分比。
//		使用以下公式：
//		销售员N销售的产品A的百分比=（销售员N售出的产品A的销售量/总销售量）*100
//		总销售量指各类产品销售量的总和（使用二维数组）

		
		Scanner abc = new Scanner (System.in);
		
		int [][] arr = new int [4][2];
		
		for (int i = 0; i < arr.length; i++) {
			System.out.println("请输入第" + (i+1) + "个销售员的编号：");
			int a = abc.nextInt();
			arr[i][0] = a;
			
			System.out.println("请输入第" + (i+1) + "个销售员的第1个商品的销售个数");
			int b = abc.nextInt();
			System.out.println("请输入第" + (i+1) + "个销售员的第2个商品的销售个数");
			int c = abc.nextInt();			
			System.out.println("请输入第" + (i+1) + "个销售员的第3个商品的销售个数");
			int d = abc.nextInt();						
			System.out.println("请输入第" + (i+1) + "个销售员的第4个商品的销售个数");
			int e = abc.nextInt();		
			
			int sum = b + c + d + e;
			arr[i][1] = sum;
		}
		
		System.out.println("****************销售明细表*****************");
		System.out.println("销售员编号\t销售总数\t百分比");
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j]+"\t");
			}
			System.out.print((double)(arr[i][0]/arr[i][1])*100+"%");
			System.out.println();
		}
		
	}

}
