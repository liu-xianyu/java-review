import java.util.Scanner;

public class Demo03 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int[][] arr=new int [5][6];
		for (int i = 0; i < arr.length; i++) {
			System.out.println("请输入第"+(i+1)+"个员工编号");
			arr[i][0]=sc.nextInt();
			System.out.println("请输入第"+(i+1)+"个员工忘打卡次数");
			arr[i][1]=sc.nextInt();
			System.out.println("请输入第"+(i+1)+"个员工迟到次数");
			arr[i][2]=sc.nextInt();
			System.out.println("请输入第"+(i+1)+"个员工早退次数");
			arr[i][3]=sc.nextInt();
			System.out.println("请输入第"+(i+1)+"个员工旷工次数");
			arr[i][4]=sc.nextInt();
			
			arr[i][5]=arr[i][1]*10+arr[i][2]*20+arr[i][3]*20+arr[i][4]*100;
		}
		System.out.println("*********************本月考勤信息**********************");
		System.out.println("员工编号\t忘打卡\t迟到\t早退\t旷工\t总罚款（单位元）");
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j]+"\t");
			}
			System.out.println();
		}
	}

}
