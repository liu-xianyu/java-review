import java.util.Scanner;

public class Demo02 {

	public static void main(String[] args) {
		//一家贸易公司有四位销售员，每位销售员负责销售四件商品，编写一个程序，
		//接收每名销售员销售的各类产品的数量。打印产品销售明细表，明细表包括每类产品的销售总数，
		//以及每位销售员销售的产品数量占总销售的百分比。
		//使用以下公式：
		//销售员N销售的产品A的百分比=（销售员N售出的产品A的销售量/总销售量）*100
		//总销售量指各类产品销售量的总和（使用二维数组）
		int[][] arr=new int[4][6];
		Scanner sc=new Scanner(System.in);

		
		
		for (int i = 0; i < arr.length; i++) {
			System.out.println("请输入第个"+(i+1)+"销售员销售第一件产品数量");
			arr[i][0]=sc.nextInt();
			System.out.println("请输入第个"+(i+1)+"销售员销售第二件产品数量");
			arr[i][1]=sc.nextInt();
			System.out.println("请输入第个"+(i+1)+"销售员销售第三件产品数量");
			arr[i][2]=sc.nextInt();
			System.out.println("请输入第个"+(i+1)+"销售员销售第四件产品数量");
			arr[i][3]=sc.nextInt();
			

		}
		double sum=0;
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				sum=sum+arr[i][j];
			}
		}
		System.out.println("*********************本月销售考勤信息**********************");
		System.out.println("销售数量\tA百分比\tB百分比\tC百分比\tD百分比\t销售量总和");
			for (int i = 0; i < arr.length; i++) {
				System.out.print(arr[i][0]);
				System.out.print("\t");
				
				System.out.print(arr[i][1]/sum);
				System.out.print("\t");
				
				System.out.print(arr[i][2]/sum);
				System.out.print("\t");
				
				System.out.print(arr[i][3]/sum);
				System.out.print("\t");
				
				
				System.out.println("\t"+sum);
		}
	}
}
