package 小胖子;

import java.util.Scanner;

public class Demo04 {

	public static void main(String[] args) {
		/*1.一家贸易公司有四位销售员，每位销售员负责销售四件商品，编写一个程序，
		接收每名销售员销售的各类产品的数量。
		打印产品销售明细表，明细表包括每类产品的销售总数，以及每位销售员销售的产品数量占总销售的百分比。
		使用以下公式：
		销售员N销售的产品A的百分比=（销售员N售出的产品A的销售量/总销售量）*100
		总销售量指各类产品销售量的总和（使用二维数组）*/

		int arr[][]=new int [4][4];
		Scanner scan=new Scanner(System.in);
		
		

		for (int i = 0; i < arr.length; i++) {
			System.out.println("请输入第" + (i+1) + "个销售员的编号：");
			int a = scan.nextInt();
			arr[i][0] = a;
			
			System.out.println("请输入第" + (i+1) + "个销售员的第1个商品的销售个数");
			int b = scan.nextInt();
			System.out.println("请输入第" + (i+1) + "个销售员的第2个商品的销售个数");
			int c = scan.nextInt();			
			System.out.println("请输入第" + (i+1) + "个销售员的第3个商品的销售个数");
			int d = scan.nextInt();						
			System.out.println("请输入第" + (i+1) + "个销售员的第4个商品的销售个数");
			int e = scan.nextInt();		
			
			int sum = b + c + d + e;
			arr[i][1] = sum;
		}
		
		double sum= 0;
		int avg= 0;
		
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				sum=arr[i][j]+sum;
			}
		}
		System.out.println(sum);
		System.out.println(arr[0][0]/sum);
		
		int b[][]=new int [4][5];
		for (int i = 0; i < b.length; i++) {
			for (int j = 0; j < b[i].length; j++) {
				if (j<=3) {
					b[i][j]=arr[i][j];
				}
				if (j==4){
					b[i][j]=(int) sum;
				}
			
			}
		}	
		System.out.println("***********产品销售明细表***********");
		System.out.println("第1个产品\t第2个产品\t第3个产品\t第4个产品\t总销售");
		for (int i = 0; i < b.length; i++) {
			System.out.println("第"+(i+1)+"个销售员");
			for (int j = 0; j < b[i].length; j++) {
				if (j<4) {
					System.out.print(b[i][j]+" "+(double)(b[i][j]/sum)*100+"%");
				}
				
			}System.out.println();
		}
		
	
	}

}
